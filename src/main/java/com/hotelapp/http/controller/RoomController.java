package com.hotelapp.http.controller;

import com.hotelapp.database.dto.AvailabilityDto;
import com.hotelapp.database.dto.PayDto;
import com.hotelapp.database.dto.booking.BookingCreateEditDto;
import com.hotelapp.database.dto.booking.BookingReadDto;
import com.hotelapp.database.dto.booking.ConveniencyReadDto;
import com.hotelapp.database.dto.booking.RoomTypeReadDto;
import com.hotelapp.database.dto.user.UserReadDto;
import com.hotelapp.database.entity.BookingStatus;
import com.hotelapp.database.entity.Conveniency;
import com.hotelapp.database.entity.Room;
import com.hotelapp.database.entity.RoomType;
import com.hotelapp.database.repository.RoomRepository;
import com.hotelapp.database.repository.RoomTypeRepository;
import com.hotelapp.database.repository.TypeConveniencesRepository;
import com.hotelapp.service.ChatService;
import com.hotelapp.service.RoomService;
import com.hotelapp.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.annotation.CurrentSecurityContext;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;
import java.util.Optional;


@Controller
@RequiredArgsConstructor
public class RoomController {

    private final RoomService roomService;
    private final UserService userService;
    private final ChatService chatService;


    @GetMapping("/rooms")
    public String getAllRooms(Model model,
                              @AuthenticationPrincipal UserDetails userDetails) {
        model.addAttribute("rooms", roomService.findAllRoomTypeOrderedByPrice());
        UserReadDto currentUser = userService.findByUsername(userDetails.getUsername()).get();
        model.addAttribute("countNotReadMessages", chatService.findCountMessagesInAllChats(currentUser.getId(), currentUser.getRole().getAuthority()));
        model.addAttribute("currentUser", currentUser);
        model.addAttribute("url", "rooms");
        return "app/_rooms";
    }


    @GetMapping("/rooms/{id}")
    public String findById(@PathVariable("id") Integer id,
                           Model model,
                           @AuthenticationPrincipal UserDetails userDetails) {
        return Optional.of(roomService.findRoomTypeById(id))
                .map(room -> {
                    model.addAttribute("room", room);
                    UserReadDto currentUser = userService.findByUsername(userDetails.getUsername()).get();
                    model.addAttribute("countNotReadMessages", chatService.findCountMessagesInAllChats(currentUser.getId(), currentUser.getRole().getAuthority()));
                    model.addAttribute("currentUser", currentUser);
                    model.addAttribute("conveniencies", roomService.findAllConvienciesByType(room));
                    model.addAttribute("url", "rooms");
                    return "app/_room";
                })
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    @GetMapping("/rooms/availableroom/{id}")
    public String checkAvailabilityOfRoomTypePage(@PathVariable("id") Integer id,
            Model model) {
        return Optional.of(roomService.findRoomTypeById(id))
                .map(room -> {
                    model.addAttribute("room", room);
                    return "app/_booking";
                })
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    @PostMapping("/rooms/availableroom_process/{id}")
    public String checkAvailabilityOfRoomType(@ModelAttribute @Validated AvailabilityDto availability,
                                              BindingResult bindingResult,
                                              RedirectAttributes redirectAttributes,
                                              @PathVariable("id") Integer roomTypeId,
                                              @AuthenticationPrincipal UserDetails userDetails) {
        if (bindingResult.hasErrors()) {
            redirectAttributes.addFlashAttribute("errors", "error");
            return "redirect:/rooms/availableroom/" + roomTypeId;
        }

        RoomTypeReadDto roomTypeReadDto = roomService.findRoomTypeById(roomTypeId);

        try {
            roomService.createBooking(
                    new BookingCreateEditDto(BookingStatus.RESERVED,
                            0L,
                            roomTypeId,
                            0,
                            0,
                            availability.getCheckIn(),
                            availability.getCheckOut()),
                    userDetails);
        } catch (IllegalArgumentException e) {
            redirectAttributes.addFlashAttribute("errors", "error");
            return "redirect:/rooms/availableroom/" + roomTypeId;
        }
        redirectAttributes.addFlashAttribute("bookedRoom", roomTypeReadDto);
        return "redirect:/bookings";
    }

    @GetMapping("/rooms/pay/{id}")
    public String payBookingPage(@PathVariable("id") Long id,
                                                  Model model,
                                 @AuthenticationPrincipal UserDetails userDetails) {
        UserReadDto currentUser = userService.findByUsername(userDetails.getUsername()).get();

        BookingReadDto bookingReadDto = roomService.findBookingById(id);

        if (currentUser.getId() != bookingReadDto.getUserReadDto().getId()) {
            return "redirect:/bookings";
        }

        return Optional.of(bookingReadDto)
                .map(booking -> {
                    model.addAttribute("booking", booking);
                    model.addAttribute("room", roomService.findRoomTypeById(booking.getRoomTypeReadDto().getId()));
                    return "app/_payroom";
                })
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    @PostMapping("/rooms/payprocess/{id}")
    public String payBooking(@ModelAttribute @Validated PayDto payDto,
                                              BindingResult bindingResult,
                                              RedirectAttributes redirectAttributes,
                                              @PathVariable("id") Long bookingId,
                             @AuthenticationPrincipal UserDetails userDetails) {
        UserReadDto currentUser = userService.findByUsername(userDetails.getUsername()).get();

        BookingReadDto booking = roomService.findBookingById(bookingId);

        if (currentUser.getId() != booking.getUserReadDto().getId()) {
            return "redirect:/bookings";
        }

        if (bindingResult.hasErrors()) {
            redirectAttributes.addFlashAttribute("errors", "error");
            return "redirect:/rooms/pay/" + bookingId;
        }

        BookingReadDto bookingReadDto = roomService.payBooking(bookingId);

        userService.addCost(bookingReadDto.getTotalPrice(), currentUser.getId());

        redirectAttributes.addFlashAttribute("paidRoom", bookingReadDto.getRoomTypeReadDto());

        return "redirect:/bookings";
    }

    @PostMapping("/rooms/cancel/{id}")
    public String cancelBooking(@PathVariable("id") Long bookingId,
                                RedirectAttributes redirectAttributes,
                                @AuthenticationPrincipal UserDetails userDetails) {
        UserReadDto currentUser = userService.findByUsername(userDetails.getUsername()).get();

        BookingReadDto booking = roomService.findBookingById(bookingId);

        if (currentUser.getId() != booking.getUserReadDto().getId()) {
            return "redirect:/bookings";
        }


        BookingReadDto bookingReadDto = roomService.cancelBooking(bookingId);

        redirectAttributes.addFlashAttribute("cancelRoom", bookingReadDto.getRoomTypeReadDto());

        return "redirect:/bookings";
    }

    @GetMapping("/bookings")
    public String getAllBookingsOfUser(Model model,
                              @AuthenticationPrincipal UserDetails userDetails) {
        UserReadDto currentUser = userService.findByUsername(userDetails.getUsername()).get();
        model.addAttribute("countNotReadMessages", chatService.findCountMessagesInAllChats(currentUser.getId(), currentUser.getRole().getAuthority()));
        model.addAttribute("currentUser", currentUser);
        model.addAttribute("bookings", roomService.findAllBookingByUserId(userDetails));
        model.addAttribute("url", "bookings");
        return "app/_bookings";
    }




}