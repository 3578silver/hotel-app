package com.hotelapp.database.dto.services;

import com.hotelapp.database.dto.food.FoodCartReadDto;
import com.hotelapp.database.dto.food.FoodReadDto;
import com.hotelapp.database.entity.Service;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Setter;
import lombok.Value;
import lombok.experimental.NonFinal;

@Value
public class ServiceCartCreateEditDto {
    @NotNull
    Integer serviceId;
    @NotNull
    Long cartId;
}