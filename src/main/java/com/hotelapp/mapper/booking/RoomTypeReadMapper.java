package com.hotelapp.mapper.booking;

import com.hotelapp.database.dto.booking.RoomTypeReadDto;
import com.hotelapp.database.entity.RoomType;
import com.hotelapp.mapper.Mapper;
import com.hotelapp.mapper.booking.locales.RoomTypeDescriptionReadMapper;
import com.hotelapp.mapper.booking.locales.RoomTypeNameReadMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@RequiredArgsConstructor
public class RoomTypeReadMapper implements Mapper<RoomType, RoomTypeReadDto> {

    private final RoomTypeNameReadMapper roomTypeNameReadMapper;
    private final RoomTypeDescriptionReadMapper roomTypeDescriptionReadMapper;
    @Override
    public RoomTypeReadDto map(RoomType roomType) {
        return new RoomTypeReadDto(
                roomType.getId(),
                Optional.of(roomType.getRoomTypeName()).map(roomTypeNameReadMapper::map).orElse(null),
                Optional.of(roomType.getRoomTypeDescription()).map(roomTypeDescriptionReadMapper::map).orElse(null),
                roomType.getImagePath(),
                roomType.getPrice()
        );
    }
}
