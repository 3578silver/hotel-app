package com.hotelapp.mapper.news.locales;

import com.hotelapp.database.dto.news.locales.NewsDescriptionDto;
import com.hotelapp.database.dto.news.locales.NewsNameDto;
import com.hotelapp.database.entity.NewsDescription;
import com.hotelapp.database.entity.NewsName;
import com.hotelapp.mapper.Mapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class NewsDescriptionCreateEditMapper implements Mapper<NewsDescriptionDto, NewsDescription> {
    @Override
    public NewsDescription map(NewsDescriptionDto newsDescriptionDto, NewsDescription newsDescription) {
        copy(newsDescriptionDto, newsDescription);
        return newsDescription;
    }

    @Override
    public NewsDescription map(NewsDescriptionDto newsDescriptionDto) {
        NewsDescription newsDescription = new NewsDescription();
        copy(newsDescriptionDto, newsDescription);
        return newsDescription;
    }

    private void copy(NewsDescriptionDto newsDescriptionDto, NewsDescription newsDescription) {
        newsDescription.setRuName(newsDescriptionDto.getRuName());
        newsDescription.setEnName(newsDescriptionDto.getEnName());
    }
}
