package com.hotelapp.http.controller;

import com.hotelapp.database.dto.PageResponse;
import com.hotelapp.database.dto.admin.*;
import com.hotelapp.database.dto.booking.*;
import com.hotelapp.database.dto.booking.locales.ConveniencyDescriptionDto;
import com.hotelapp.database.dto.booking.locales.RoomTypeDescriptionDto;
import com.hotelapp.database.dto.booking.locales.RoomTypeNameDto;
import com.hotelapp.database.dto.food.FoodCreateEditDto;
import com.hotelapp.database.dto.food.FoodReadDto;
import com.hotelapp.database.dto.food.locales.FoodDescriptionDto;
import com.hotelapp.database.dto.food.locales.FoodNameDto;
import com.hotelapp.database.dto.food.locales.FoodTypeDto;
import com.hotelapp.database.dto.news.NewsCreateEditDto;
import com.hotelapp.database.dto.news.NewsReadDto;
import com.hotelapp.database.dto.news.locales.NewsDescriptionDto;
import com.hotelapp.database.dto.news.locales.NewsNameDto;
import com.hotelapp.database.dto.services.ServiceCreateEditDto;
import com.hotelapp.database.dto.services.ServiceReadDto;
import com.hotelapp.database.dto.services.locales.ServiceDescriptionDto;
import com.hotelapp.database.dto.services.locales.ServiceNameDto;
import com.hotelapp.database.dto.services.locales.ServiceTypeDto;
import com.hotelapp.database.dto.user.ChatCreateEditDto;
import com.hotelapp.database.dto.user.ChatReadDto;
import com.hotelapp.database.dto.user.UserCreateEditDto;
import com.hotelapp.database.dto.user.UserReadDto;
import com.hotelapp.database.dto.user.locales.ChatNameDto;
import com.hotelapp.database.entity.ChatName;
import com.hotelapp.database.entity.Role;
import com.hotelapp.database.repository.RoomRepository;
import com.hotelapp.http.rest.RoomRestController;
import com.hotelapp.service.*;
import jakarta.persistence.EntityManager;
import jakarta.servlet.http.HttpSession;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequiredArgsConstructor
public class AdminController {
    private final UserService userService;
    private final RoomService roomService;
    private final RoomRepository roomRepository;
    private final EntityManager entityManager;
    private final RoomRestController roomRestController;
    private final FoodService foodService;
    private final ServicesService servicesService;
    private final NewsService newsService;
    private final ChatService chatService;

    @GetMapping("/adminpanel")
    public String adminPanel(Model model, @AuthenticationPrincipal UserDetails userDetails){
        UserReadDto currentUser = userService.findByUsername(userDetails.getUsername()).get();
        model.addAttribute("countNotReadMessages", chatService.findCountMessagesInAllChats(currentUser.getId(), currentUser.getRole().getAuthority()));
        model.addAttribute("currentUser", currentUser);
        return "app/_adminpanel";
    }

    @GetMapping("/adminrooms")
    public String adminRooms(Model model, @AuthenticationPrincipal UserDetails userDetails){
        UserReadDto currentUser = userService.findByUsername(userDetails.getUsername()).get();
        model.addAttribute("countNotReadMessages", chatService.findCountMessagesInAllChats(currentUser.getId(), currentUser.getRole().getAuthority()));
        model.addAttribute("currentUser", currentUser);
        model.addAttribute("roomTypes", roomService.findAllRoomTypeOrderedByPrice());
        return "app/_admin_rooms";
    }

    @PostMapping("/adminroomsprocess")
    public String adminRoomsProcess(@ModelAttribute AdminRoomsCreateDto adminRoomsCreateDto,
                                    RedirectAttributes redirectAttributes,
                           @AuthenticationPrincipal UserDetails userDetails) {
        Integer roomTypeId = roomService.findRoomTypeByRuName(adminRoomsCreateDto.getRoomType());
        roomRestController.createRoomByCountAndType(roomTypeId,
                adminRoomsCreateDto.getRoomCount());
        redirectAttributes.addFlashAttribute("createdRooms", adminRoomsCreateDto);
        return "redirect:/adminroomslist";
    }

    @GetMapping("/adminroomslist")
    public String adminRoomsList(Model model, @AuthenticationPrincipal UserDetails userDetails){
        UserReadDto currentUser = userService.findByUsername(userDetails.getUsername()).get();
        model.addAttribute("countNotReadMessages", chatService.findCountMessagesInAllChats(currentUser.getId(), currentUser.getRole().getAuthority()));
        model.addAttribute("currentUser", currentUser);
        model.addAttribute("roomTypes", roomService.findAllRoomTypeOrderedByPrice());
        model.addAttribute("roomCount", roomRepository.findCountRoomsWithRoomTypeId(entityManager));
        return "app/_admin_rooms_list";
    }

    @GetMapping("/adminuserslist/{id}")
    public String adminUsersList(Model model, @PathVariable("id") Integer id, @AuthenticationPrincipal UserDetails userDetails){
        UserReadDto currentUser = userService.findByUsername(userDetails.getUsername()).get();
        Page<UserReadDto> page = userService.findAll(Pageable.ofSize(8).withPage(id - 1));

        model.addAttribute("countNotReadMessages", chatService.findCountMessagesInAllChats(currentUser.getId(), currentUser.getRole().getAuthority()));
        model.addAttribute("currentUser", currentUser);
        model.addAttribute("roles", List.of(Role.values()));
        model.addAttribute("users", PageResponse.of(page));

        return "app/_adminuserslist";
    }


    @PostMapping("/adminusers/delete/{id}")
    public String adminUserDelete(@PathVariable("id") Long id,
                                  RedirectAttributes redirectAttributes,
                                  @AuthenticationPrincipal UserDetails userDetails){
        userService.delete(id);
        redirectAttributes.addFlashAttribute("removeUser", id);
        return "redirect:/adminuserslist/1";

    }

    @PostMapping("/adminchangerole/{id}")
    public String adminChangeRole(@ModelAttribute UserCreateEditDto user,
                                  @PathVariable("id") Long id,
                                  RedirectAttributes redirectAttributes,
                                  @AuthenticationPrincipal UserDetails userDetails) {
        UserReadDto userReadDto = userService.updateUserRole(id, user);
        redirectAttributes.addFlashAttribute("updatedRole", userReadDto);

        return "redirect:/adminuserslist/1";
    }

    @GetMapping("/adminroomtypes")
    public String adminRoomType(Model model, @AuthenticationPrincipal UserDetails userDetails){
        UserReadDto currentUser = userService.findByUsername(userDetails.getUsername()).get();
        model.addAttribute("countNotReadMessages", chatService.findCountMessagesInAllChats(currentUser.getId(), currentUser.getRole().getAuthority()));
        model.addAttribute("currentUser", currentUser);
        model.addAttribute("options", roomService.findAllConveniency());
        return "app/_admin_roomtypes";

    }

    @PostMapping("/adminroomtypesprocess")
    public String adminRoomTypesProcess(@ModelAttribute @Validated AdminRoomTypeDto adminRoomTypeDto,
                                        BindingResult bindingResult,
                                        @RequestParam("image") MultipartFile image,
                                        @RequestParam("images") MultipartFile[] images,
                                        @ModelAttribute ConvienciesFormDto convienciesFormDto,
                                    RedirectAttributes redirectAttributes,
                                    @AuthenticationPrincipal UserDetails userDetails) throws IOException {

        if (bindingResult.hasErrors()) {
            redirectAttributes.addFlashAttribute("errors", "errors");
            return "redirect:/adminroomtypes";
        }

        String fileName = adminRoomTypeDto.getRoomTypeEnName().replaceAll(" ", "_");
        String fullFileName = fileName + ".png";

        RoomTypeReadDto roomType = roomService.createRoomType(new RoomTypeCreateEditDto(
                new RoomTypeNameDto(adminRoomTypeDto.getRoomTypeRuName(),
                        adminRoomTypeDto.getRoomTypeEnName()),
                new RoomTypeDescriptionDto(adminRoomTypeDto.getRoomTypeRuDescription(),
                        adminRoomTypeDto.getRoomTypeEnDescription()),
                fullFileName,
                adminRoomTypeDto.getPrice()
        ));

        for (Integer convId : convienciesFormDto.getOptions()){
            if (convId != null) {
                roomRestController.addConveniencyToRoomType(convId, roomType.getId());
            }
        }


        Path dir1 = Paths.get("src", "main", "resources", "templates" , "images", "roomTypes");
        if (!Files.isDirectory(dir1)) {
            Files.createDirectory(dir1);

        }

        Path path = Paths.get("src", "main", "resources", "templates" , "images", "roomTypes", fullFileName);

        Files.write(path, image.getBytes());

        for (int i = 0; i < images.length; i++) {
            String fileName1 = i + 1 + fullFileName;
            Path path1 = Paths.get("src", "main", "resources", "templates" , "images", "roomTypes", fileName1);
            Files.write(path1, images[i].getBytes());
        }
        redirectAttributes.addFlashAttribute("createdType", adminRoomTypeDto);


        return "redirect:/adminroomslist";
    }

    @PostMapping("/adminroomtype/delete/{id}")
    public String adminRoomTypeDelete(@PathVariable("id") Integer id,
                                      RedirectAttributes redirectAttributes,
                                  @AuthenticationPrincipal UserDetails userDetails) throws IOException {
        RoomTypeReadDto roomType = roomService.findRoomTypeById(id);
        roomService.deleteRoomTypeById(id);
        Path path = Paths.get("src", "main", "resources", "templates" , "images", "roomTypes", roomType.getImagePath());
        Path path1 = Paths.get("src", "main", "resources", "templates" , "images", "roomTypes", "1" + roomType.getImagePath());
        Path path2 = Paths.get("src", "main", "resources", "templates" , "images", "roomTypes", "2" + roomType.getImagePath());
        Path path3 = Paths.get("src", "main", "resources", "templates" , "images", "roomTypes", "3" + roomType.getImagePath());
        Files.deleteIfExists(path);
        Files.deleteIfExists(path1);
        Files.deleteIfExists(path2);
        Files.deleteIfExists(path3);
        redirectAttributes.addFlashAttribute("removeRoomType", roomType);

        return "redirect:/adminroomslist";

    }

    @GetMapping("/adminconvlist")
    public String adminConvList(Model model, @AuthenticationPrincipal UserDetails userDetails){
        UserReadDto currentUser = userService.findByUsername(userDetails.getUsername()).get();
        model.addAttribute("countNotReadMessages", chatService.findCountMessagesInAllChats(currentUser.getId(), currentUser.getRole().getAuthority()));
        model.addAttribute("currentUser", currentUser);
        model.addAttribute("convs", roomService.findAllConveniency());
        return "app/_admin_conv_list";
    }



    @PostMapping("/adminconv/delete/{id}")
    public String adminConvDelete(@PathVariable("id") Integer id,
                                  RedirectAttributes redirectAttributes,
                                  @AuthenticationPrincipal UserDetails userDetails) throws IOException {
        ConveniencyReadDto conveniencyReadDto = roomService.findConveniencyById(id);
        roomService.deleteConveniency(id);
        Path path = Paths.get("src", "main", "resources", "templates" , "images", "conveniencies", conveniencyReadDto.getSvg_link() + ".svg");
        Files.deleteIfExists(path);

        redirectAttributes.addFlashAttribute("removeConv", conveniencyReadDto);


        return "redirect:/adminconvlist";

    }


    @GetMapping("/adminconvnew")
    public String adminConvNew(Model model, @AuthenticationPrincipal UserDetails userDetails){
        UserReadDto currentUser = userService.findByUsername(userDetails.getUsername()).get();
        model.addAttribute("countNotReadMessages", chatService.findCountMessagesInAllChats(currentUser.getId(), currentUser.getRole().getAuthority()));
        model.addAttribute("currentUser", currentUser);
        return "app/_admin_conv_new";

    }


    @PostMapping("/adminconvnewprocess")
    public String adminConvNewProcess(@ModelAttribute @Validated ConveniencyDescriptionDto conveniencyDescriptionDto,
                                      BindingResult bindingResult,
                                      @RequestParam("image") MultipartFile image,
                                    RedirectAttributes redirectAttributes,
                                    @AuthenticationPrincipal UserDetails userDetails) throws IOException {

        if (bindingResult.hasErrors()) {
            redirectAttributes.addFlashAttribute("errors", "errors");
            return "redirect:/adminconvnew";
        }

        String fileName = conveniencyDescriptionDto.getEnName().replaceAll(" ", "_");
        String fullFileName = fileName + ".svg";
        Path path = Paths.get("src", "main", "resources", "templates" , "images", "conveniencies", fullFileName);

        if (Files.exists(path)) {
            for (int i = 0; i < Integer.MAX_VALUE; i ++) {
                fullFileName = fileName + i + ".svg";
                if (!Files.exists(Paths.get("src/main/resources/templates/images/conveniencies/" + fullFileName))) {
                    Files.write(Paths.get("src/main/resources/templates/images/conveniencies/" + fullFileName), image.getBytes());
                    break;
                }
            }
        } else {
            Files.write(path, image.getBytes());
        }

        roomService.createConveniency(new ConveniencyCreateEditDto(
                fileName,
                conveniencyDescriptionDto
        ));

        redirectAttributes.addFlashAttribute("createdConv", conveniencyDescriptionDto);

        return "redirect:/adminconvlist";
    }


    @GetMapping("/adminconvexist")
    public String adminConvExist(Model model, @AuthenticationPrincipal UserDetails userDetails) throws IOException {
        UserReadDto currentUser = userService.findByUsername(userDetails.getUsername()).get();
        model.addAttribute("countNotReadMessages", chatService.findCountMessagesInAllChats(currentUser.getId(), currentUser.getRole().getAuthority()));
        model.addAttribute("currentUser", currentUser);
        List<String> svgNames = Files.list(Paths.get("src", "main", "resources", "templates", "images", "conveniencies", "rezerv")).map(o -> o.getFileName()
                .toString().substring(0, o.getFileName().toString().lastIndexOf("."))).toList();



        model.addAttribute("svgs",
                svgNames);
        return "app/_admin_conv_exist";

    }


    @PostMapping("/adminconvexistprocess")
    public String adminConvExistProcess(@ModelAttribute ConveniencyDescriptionDto conveniencyDescriptionDto,
                                        @RequestParam("svg") String svg,
                                   RedirectAttributes redirectAttributes,
                                   @AuthenticationPrincipal UserDetails userDetails) throws IOException {

        String fileName = conveniencyDescriptionDto.getEnName().replaceAll(" ", "_");
        String fullFileName = fileName + ".svg";

        byte[] defSvg = Files.readAllBytes(Paths.get("src", "main", "resources", "templates", "images", "conveniencies", "rezerv", svg + ".svg"));
        Files.write(Paths.get("src", "main", "resources", "templates", "images", "conveniencies", fullFileName), defSvg);


        roomService.createConveniency(new ConveniencyCreateEditDto(
                fileName,
                conveniencyDescriptionDto
        ));

        redirectAttributes.addFlashAttribute("createdConv", conveniencyDescriptionDto);

        return "redirect:/adminconvlist";
    }

    @GetMapping("/adminroomtypes/updatepage/{id}")
    public String adminRoomTypeUpdate(Model model, @AuthenticationPrincipal UserDetails userDetails,
                                @PathVariable("id") Integer id){
        UserReadDto currentUser = userService.findByUsername(userDetails.getUsername()).get();
        model.addAttribute("countNotReadMessages", chatService.findCountMessagesInAllChats(currentUser.getId(), currentUser.getRole().getAuthority()));
        model.addAttribute("currentUser", currentUser);        model.addAttribute("options", roomService.findAllConveniency());
        model.addAttribute("roomType", roomService.findRoomTypeById(id));
        model.addAttribute("optionsOfRoomType", roomService.findAllConveniencesByRoomType(id).stream().map(o -> o.getConveniencyDescriptionDto().getEnName()).toList());
        return "app/_admin_roomtypes_update";

    }

    @PostMapping("/adminroomtypes/update/{id}")
    public String adminRoomTypesUpdateProcess(@PathVariable("id") Integer id,
                                        @ModelAttribute @Validated AdminRoomTypeDto adminRoomTypeDto,
                                        BindingResult bindingResult,
                                        @ModelAttribute ConvienciesFormDto convienciesFormDto,
                                        RedirectAttributes redirectAttributes,
                                        @AuthenticationPrincipal UserDetails userDetails) throws IOException {

        if (bindingResult.hasErrors()) {
            System.out.println(bindingResult);
            redirectAttributes.addFlashAttribute("errors", "errors");
            return "redirect:/adminroomtypes/updatepage/" + id;
        }

        // можно добавить изменение  фоток

        RoomTypeReadDto roomType = roomService.findRoomTypeById(id);
        // обновление сущности типа номера
        roomService.updatePriceInRoomType(roomType.getId(), adminRoomTypeDto.getPrice());
        roomService.updateRoomTypeDescription(
                roomService.findRoomTypeDescriptionByEnName(roomType.getRoomTypeDescriptionDto().getEnName()),
                adminRoomTypeDto.getRoomTypeRuDescription(),
                adminRoomTypeDto.getRoomTypeEnDescription());
        roomService.updateRoomTypeName(
                roomService.findRoomTypeNameByEnName(roomType.getRoomTypeNameDto().getEnName()),
                adminRoomTypeDto.getRoomTypeRuName(),
                adminRoomTypeDto.getRoomTypeEnName());


        List<ConveniencyReadDto> conveniencyReadDtos = roomService.findAllConveniencesByRoomType(id);//
        for (ConveniencyReadDto conveniencyReadDto : conveniencyReadDtos) {
            if (!convienciesFormDto.getOptions().contains(conveniencyReadDto.getId())) {
                roomService.deleteTypeConvenience(id, conveniencyReadDto.getId());
            }
        }
        for (Integer convId : convienciesFormDto.getOptions()) {
            if (convId != null && !conveniencyReadDtos.stream().map(o -> o.getId()).toList().contains(convId)) {
                roomService.addConveniencyToRoomType(new TypeConveniencesCreateEditDto(roomType.getId(), convId));
            }
        }

        redirectAttributes.addFlashAttribute("updateRoomType", adminRoomTypeDto);


        return "redirect:/adminroomslist";
    }

    @GetMapping("/adminusercreate")
    public String adminUserCreate(Model model, UserCreateEditDto user,
                                  @AuthenticationPrincipal UserDetails userDetails){
        UserReadDto currentUser = userService.findByUsername(userDetails.getUsername()).get();
        model.addAttribute("countNotReadMessages", chatService.findCountMessagesInAllChats(currentUser.getId(), currentUser.getRole().getAuthority()));
        model.addAttribute("currentUser", currentUser);
        if (!model.containsAttribute("user")) {
            model.addAttribute("user", user);
        }
        if (!model.containsAttribute("errors")) {
            model.addAttribute("errors", new HashMap<>());
        }
        return "app/_admin_user_create";
    }

    @PostMapping("/adminusercreateprocess")
    public String adminUserCreateProcess(@ModelAttribute @Validated UserCreateEditDto user,
                           BindingResult bindingResult,
                           RedirectAttributes redirectAttributes,
                           HttpSession httpSession){
        redirectAttributes.addFlashAttribute("user", user);
        Map<String, String> errors = new HashMap<>(){{
            put("username", "correct");
            put("firstname", "correct");
            put("password", "correct");
            put("lastname", "correct");
            put("phoneNumber", "correct");
            put("birthDate", "correct");
        }
        };

        if (bindingResult.hasErrors()) {
            bindingResult.getFieldErrors().stream().map(FieldError::getField).forEach(field -> errors.put(field, "incorrect"));
            redirectAttributes.addFlashAttribute("errors", errors);
            return "redirect:/adminusercreate";
        }
        try {
            userService.create(user);
        } catch (IllegalArgumentException e) {
            errors.put("username", "incorrect");
            redirectAttributes.addFlashAttribute("errors", errors);
            return "redirect:/adminusercreate";
        }

        redirectAttributes.addFlashAttribute("createdUser", user);

        return "redirect:/adminuserslist/1";
    }

    @GetMapping("/adminfoodlist")
    public String adminFoodList(Model model, @AuthenticationPrincipal UserDetails userDetails){
        UserReadDto currentUser = userService.findByUsername(userDetails.getUsername()).get();
        model.addAttribute("countNotReadMessages", chatService.findCountMessagesInAllChats(currentUser.getId(), currentUser.getRole().getAuthority()));
        model.addAttribute("currentUser", currentUser);
        model.addAttribute("foods", foodService.findAllFoodDesc());
        return "app/_admin_food_list";
    }

    @PostMapping("/adminfood/delete/{id}")
    public String adminFoodDelete(@PathVariable("id") Integer id,
                                  RedirectAttributes redirectAttributes,
                                  @AuthenticationPrincipal UserDetails userDetails) throws IOException {

        FoodReadDto foodReadDto = foodService.findFoodById(id).orElseThrow();

        foodService.deleteFoodById(id);

        Path path = Paths.get("src/main/resources/templates/images/food/" + foodReadDto.getImageLink());

        Files.deleteIfExists(path);


        redirectAttributes.addFlashAttribute("removeFood", foodReadDto);

        return "redirect:/adminfoodlist";

    }

    @GetMapping("/adminfoodcreate")
    public String adminFoodCreate(Model model, @AuthenticationPrincipal UserDetails userDetails){
        UserReadDto currentUser = userService.findByUsername(userDetails.getUsername()).get();
        model.addAttribute("countNotReadMessages", chatService.findCountMessagesInAllChats(currentUser.getId(), currentUser.getRole().getAuthority()));
        model.addAttribute("currentUser", currentUser);
        model.addAttribute("foodTypes", foodService.findAllFoodTypes());
        return "app/_admin_food_create";

    }

    @PostMapping("/adminfoodcreateprocess")
    public String adminFoodCreateProcess(@ModelAttribute @Validated AdminFoodDto adminFoodDto,
                                        BindingResult bindingResult,
                                        @RequestParam("image") MultipartFile image,
                                        RedirectAttributes redirectAttributes,
                                        @AuthenticationPrincipal UserDetails userDetails) throws IOException {

        if (bindingResult.hasErrors()) {
            redirectAttributes.addFlashAttribute("errors", "errors");
            return "redirect:/adminfoodcreate";
        }

        FoodTypeDto foodTypeDto = foodService.findFoodTypeByRuName(adminFoodDto.getFoodType());

        String fileExtension = "." + image.getContentType().substring(image.getContentType().indexOf("/") + 1);
        String fileName = foodTypeDto.getEnName().toLowerCase() + "/" + adminFoodDto.getEnName().toLowerCase().replaceAll(" ", "_").toLowerCase();
        String fullFileName = fileName + fileExtension;



        Path dir1 = Paths.get("src", "main", "resources", "templates" , "images", "food");
        if (!Files.isDirectory(dir1)) {
            Files.createDirectory(dir1);

        }

        Path path = Paths.get("src/main/resources/templates/images/food/" + fullFileName);

        if (Files.exists(path)) {
            for (int i = 0; i < Integer.MAX_VALUE; i ++) {
                fullFileName = fileName + i + fileExtension;
                if (!Files.exists(Paths.get("src/main/resources/templates/images/food/" + fullFileName))) {
                    Files.write(Paths.get("src/main/resources/templates/images/food/" + fullFileName), image.getBytes());
                    break;
                }
            }
        } else {
            Files.write(path, image.getBytes());
        }

        FoodReadDto foodReadDto = foodService.createFood(new FoodCreateEditDto(
                new FoodNameDto(adminFoodDto.getRuName(),
                        adminFoodDto.getEnName()),
                adminFoodDto.getCost(),
                new FoodDescriptionDto(adminFoodDto.getRuDescription(),
                        adminFoodDto.getEnDescription()),
                foodTypeDto,
                fullFileName
        ));


        redirectAttributes.addFlashAttribute("createdFood", foodReadDto);


        return "redirect:/adminfoodlist";
    }

    @GetMapping("/adminfood/updatepage/{id}")
    public String adminFoodUpdate(Model model, @AuthenticationPrincipal UserDetails userDetails,
                                      @PathVariable("id") Integer id){
        UserReadDto currentUser = userService.findByUsername(userDetails.getUsername()).get();
        model.addAttribute("countNotReadMessages", chatService.findCountMessagesInAllChats(currentUser.getId(), currentUser.getRole().getAuthority()));
        model.addAttribute("currentUser", currentUser);
        model.addAttribute("food", foodService.findFoodById(id).orElseThrow());
        model.addAttribute("foodTypes", foodService.findAllFoodTypes());
        return "app/_admin_food_update";

    }

    @PostMapping("/adminfood/update/{id}")
    public String adminFoodUpdateProcess(@PathVariable("id") Integer id,
                                              @ModelAttribute @Validated AdminFoodDto adminFoodDto,
                                              BindingResult bindingResult,
                                              RedirectAttributes redirectAttributes,
                                              @AuthenticationPrincipal UserDetails userDetails) throws IOException {

        if (bindingResult.hasErrors()) {
            redirectAttributes.addFlashAttribute("errors", "errors");
            return "redirect:/adminfood/updatepage/" + id;
        }

        // можно добавить изменение  фоток

        FoodReadDto foodReadDto = foodService.findFoodById(id).orElseThrow();


        foodService.updateCost(foodReadDto.getId(), adminFoodDto.getCost());
        foodService.updateFoodName(
                foodService.findFoodNameIdByRuName(foodReadDto.getFoodName().getRuName()),
                adminFoodDto.getRuName(),
                adminFoodDto.getEnName()
        );
        foodService.updateFoodDescription(
                foodService.findFoodDescriptionIdByRuName(foodReadDto.getFoodDescription().getRuName()),
                adminFoodDto.getRuDescription(),
                adminFoodDto.getEnDescription()
        );
        foodService.updateFoodTypeInFood(foodReadDto.getId(),
                foodService.findFoodTypeIdByRuName(adminFoodDto.getFoodType()));


        redirectAttributes.addFlashAttribute("updateFood", adminFoodDto);


        return "redirect:/adminfoodlist";
    }


    @GetMapping("/adminserviceslist")
    public String adminServicesList(Model model, @AuthenticationPrincipal UserDetails userDetails){
        UserReadDto currentUser = userService.findByUsername(userDetails.getUsername()).get();
        model.addAttribute("countNotReadMessages", chatService.findCountMessagesInAllChats(currentUser.getId(), currentUser.getRole().getAuthority()));
        model.addAttribute("currentUser", currentUser);
        model.addAttribute("services", servicesService.findAllServicesDesc());
        return "app/_admin_services_list";
    }

    @PostMapping("/adminservice/delete/{id}")
    public String adminServiceDelete(@PathVariable("id") Integer id,
                                  RedirectAttributes redirectAttributes,
                                  @AuthenticationPrincipal UserDetails userDetails) throws IOException {

        ServiceReadDto serviceReadDto = servicesService.findServiceById(id).orElseThrow();

        servicesService.deleteServiceById(id);

        Path path = Paths.get("src/main/resources/templates/images/services/" + serviceReadDto.getImageLink());

        Files.deleteIfExists(path);


        redirectAttributes.addFlashAttribute("removeService", serviceReadDto);

        return "redirect:/adminserviceslist";

    }

    @GetMapping("/adminservicecreate")
    public String adminServiceCreate(Model model, @AuthenticationPrincipal UserDetails userDetails){
        UserReadDto currentUser = userService.findByUsername(userDetails.getUsername()).get();
        model.addAttribute("countNotReadMessages", chatService.findCountMessagesInAllChats(currentUser.getId(), currentUser.getRole().getAuthority()));
        model.addAttribute("currentUser", currentUser);
        model.addAttribute("serviceTypes", servicesService.findAllServiceTypes());
        return "app/_admin_service_create";

    }

    @PostMapping("/adminservicecreateprocess")
    public String adminServiceCreateProcess(@ModelAttribute @Validated AdminServiceDto adminServiceDto,
                                         BindingResult bindingResult,
                                         @RequestParam("image") MultipartFile image,
                                         RedirectAttributes redirectAttributes,
                                         @AuthenticationPrincipal UserDetails userDetails) throws IOException {

        if (bindingResult.hasErrors()) {
            redirectAttributes.addFlashAttribute("errors", "errors");
            return "redirect:/adminservicecreate";
        }

        ServiceTypeDto serviceTypeDto = servicesService.findServiceTypeByRuName(adminServiceDto.getServiceType());

        String fileExtension = "." + image.getContentType().substring(image.getContentType().indexOf("/") + 1);
        String fileName = serviceTypeDto.getEnName().toLowerCase() + "/" + adminServiceDto.getEnName().toLowerCase().replaceAll(" ", "_").toLowerCase();
        String fullFileName = fileName + fileExtension;


        Path dir1 = Paths.get("src", "main", "resources", "templates" , "images", "services");
        if (!Files.isDirectory(dir1)) {
            Files.createDirectory(dir1);

        }

        Path path = Paths.get("src/main/resources/templates/images/services/" + fullFileName);

        if (Files.exists(path)) {
            for (int i = 0; i < Integer.MAX_VALUE; i ++) {
                fullFileName = fileName + i + fileExtension;
                if (!Files.exists(Paths.get("src/main/resources/templates/images/services/" + fullFileName))) {
                    Files.write(Paths.get("src/main/resources/templates/images/services/" + fullFileName), image.getBytes());
                    break;
                }
            }
        } else {
            Files.write(path, image.getBytes());
        }

        ServiceReadDto serviceReadDto = servicesService.createService(new ServiceCreateEditDto(
                new ServiceNameDto(adminServiceDto.getRuName(),
                        adminServiceDto.getEnName()),
                new ServiceDescriptionDto(adminServiceDto.getRuDescription(),
                        adminServiceDto.getEnDescription()),
                serviceTypeDto,
                fullFileName,
                adminServiceDto.getPrice()
        ));



        redirectAttributes.addFlashAttribute("createdService", serviceReadDto);


        return "redirect:/adminserviceslist";
    }

    @GetMapping("/adminservice/updatepage/{id}")
    public String adminServiceUpdate(Model model, @AuthenticationPrincipal UserDetails userDetails,
                                  @PathVariable("id") Integer id){
        UserReadDto currentUser = userService.findByUsername(userDetails.getUsername()).get();
        model.addAttribute("countNotReadMessages", chatService.findCountMessagesInAllChats(currentUser.getId(), currentUser.getRole().getAuthority()));
        model.addAttribute("currentUser", currentUser);
        model.addAttribute("service", servicesService.findServiceById(id).orElseThrow());
        model.addAttribute("serviceTypes", servicesService.findAllServiceTypes());
        return "app/_admin_service_update";

    }

    @PostMapping("/adminservice/update/{id}")
    public String adminServiceUpdateProcess(@PathVariable("id") Integer id,
                                         @ModelAttribute @Validated AdminServiceDto adminServiceDto,
                                         BindingResult bindingResult,
                                         RedirectAttributes redirectAttributes,
                                         @AuthenticationPrincipal UserDetails userDetails) throws IOException {

        if (bindingResult.hasErrors()) {
            redirectAttributes.addFlashAttribute("errors", "errors");
            return "redirect:/adminservice/updatepage/" + id;
        }

        // можно добавить изменение  фоток

        ServiceReadDto serviceReadDto = servicesService.findServiceById(id).orElseThrow();


        servicesService.updatePrice(serviceReadDto.getId(), adminServiceDto.getPrice());
        servicesService.updateServiceName(
                servicesService.findServiceNameIdByRuName(serviceReadDto.getServiceName().getRuName()),
                adminServiceDto.getRuName(),
                adminServiceDto.getEnName()
        );
        servicesService.updateServiceDescription(
                servicesService.findServiceDescriptionIdByRuName(serviceReadDto.getServiceDescription().getRuName()),
                adminServiceDto.getRuDescription(),
                adminServiceDto.getEnDescription()
        );
        servicesService.updateServiceTypeInService(serviceReadDto.getId(),
                servicesService.findServiceTypeIdByRuName(adminServiceDto.getServiceType()));


        redirectAttributes.addFlashAttribute("updateService", adminServiceDto);


        return "redirect:/adminserviceslist";
    }


    @GetMapping("/adminnewslist")
    public String adminNewsList(Model model, @AuthenticationPrincipal UserDetails userDetails){
        UserReadDto currentUser = userService.findByUsername(userDetails.getUsername()).get();
        model.addAttribute("countNotReadMessages", chatService.findCountMessagesInAllChats(currentUser.getId(), currentUser.getRole().getAuthority()));
        model.addAttribute("currentUser", currentUser);
        model.addAttribute("news", newsService.findAllNewsByOrderByIdDesc());
        return "app/_admin_news_list";
    }

    @PostMapping("/adminnews/delete/{id}")
    public String adminNewsDelete(@PathVariable("id") Long id,
                                     RedirectAttributes redirectAttributes,
                                     @AuthenticationPrincipal UserDetails userDetails) throws IOException {

        NewsReadDto newsReadDto = newsService.findNewsById(id);

        newsService.deleteNewsById(id);

        Path path = Paths.get("src/main/resources/templates/images/news/" + newsReadDto.getImageLink());

        Files.deleteIfExists(path);


        redirectAttributes.addFlashAttribute("removeNews", newsReadDto);

        return "redirect:/adminnewslist";

    }

    @GetMapping("/adminnewscreate")
    public String adminNewsCreate(Model model, @AuthenticationPrincipal UserDetails userDetails){
        UserReadDto currentUser = userService.findByUsername(userDetails.getUsername()).get();
        model.addAttribute("countNotReadMessages", chatService.findCountMessagesInAllChats(currentUser.getId(), currentUser.getRole().getAuthority()));
        model.addAttribute("currentUser", currentUser);
        return "app/_admin_news_create";

    }

    @PostMapping("/adminnewscreateprocess")
    public String adminNewsCreateProcess(@ModelAttribute @Validated AdminNewsDto adminNewsDto,
                                            BindingResult bindingResult,
                                            @RequestParam("image") MultipartFile image,
                                            RedirectAttributes redirectAttributes,
                                            @AuthenticationPrincipal UserDetails userDetails) throws IOException {

        if (bindingResult.hasErrors()) {
            redirectAttributes.addFlashAttribute("errors", "errors");
            return "redirect:/adminnewscreate";
        }

        String fileExtension = "." + image.getContentType().substring(image.getContentType().indexOf("/") + 1);
        String fileName = adminNewsDto.getEnName().toLowerCase().replaceAll(" ", "_").toLowerCase();
        String fullFileName = fileName + fileExtension;


        Path dir1 = Paths.get("src", "main", "resources", "templates" , "images", "news");
        if (!Files.isDirectory(dir1)) {
            Files.createDirectory(dir1);

        }

        Path path = Paths.get("src/main/resources/templates/images/news/" + fullFileName);

        if (Files.exists(path)) {
            for (int i = 0; i < Integer.MAX_VALUE; i ++) {
                fullFileName = fileName + i + fileExtension;
                if (!Files.exists(Paths.get("src/main/resources/templates/images/news/" + fullFileName))) {
                    Files.write(Paths.get("src/main/resources/templates/images/news/" + fullFileName), image.getBytes());
                    break;
                }
            }
        } else {
            Files.write(path, image.getBytes());
        }

        NewsReadDto newsReadDto = newsService.createNews(new NewsCreateEditDto(
                new NewsNameDto(adminNewsDto.getRuName(),
                        adminNewsDto.getEnName()),
                new NewsDescriptionDto(adminNewsDto.getRuDescription(),
                        adminNewsDto.getEnDescription()),
                fullFileName
        ));


        redirectAttributes.addFlashAttribute("createdNews", newsReadDto);


        return "redirect:/adminnewslist";
    }

    @GetMapping("/adminnews/updatepage/{id}")
    public String adminNewsUpdate(Model model, @AuthenticationPrincipal UserDetails userDetails,
                                     @PathVariable("id") Long id){
        UserReadDto currentUser = userService.findByUsername(userDetails.getUsername()).get();
        model.addAttribute("countNotReadMessages", chatService.findCountMessagesInAllChats(currentUser.getId(), currentUser.getRole().getAuthority()));
        model.addAttribute("currentUser", currentUser);
        model.addAttribute("news", newsService.findNewsById(id));
        return "app/_admin_news_update";

    }

    @PostMapping("/adminnews/update/{id}")
    public String adminNewsUpdateProcess(@PathVariable("id") Long id,
                                            @ModelAttribute @Validated AdminNewsDto adminNewsDto,
                                            BindingResult bindingResult,
                                            RedirectAttributes redirectAttributes,
                                            @AuthenticationPrincipal UserDetails userDetails) throws IOException {

        if (bindingResult.hasErrors()) {
            redirectAttributes.addFlashAttribute("errors", "errors");
            return "redirect:/adminnews/updatepage/" + id;
        }

        // можно добавить изменение  фоток

        NewsReadDto newsReadDto = newsService.findNewsById(id);

        newsService.updateNewsName(
                newsService.findNewsNameIdByRuName(newsReadDto.getNewsName().getRuName()),
                adminNewsDto.getRuName(),
                adminNewsDto.getEnName()
        );

        newsService.updateNewsDescription(
                newsService.findNewsDescriptionIdByRuName(newsReadDto.getNewsDescription().getRuName()),
                adminNewsDto.getRuDescription(),
                adminNewsDto.getEnDescription()
        );


        redirectAttributes.addFlashAttribute("updateNews", adminNewsDto);


        return "redirect:/adminnewslist";
    }



    @GetMapping("/adminchatslist")
    public String adminChatsList(Model model, @AuthenticationPrincipal UserDetails userDetails){
        UserReadDto currentUser = userService.findByUsername(userDetails.getUsername()).get();
        model.addAttribute("countNotReadMessages", chatService.findCountMessagesInAllChats(currentUser.getId(), currentUser.getRole().getAuthority()));
        model.addAttribute("currentUser", currentUser);
        model.addAttribute("chats", chatService.findAllChats());
        model.addAttribute("countUsersInChats", chatService.findCountUsersInChats());
        return "app/_adminchatslist";
    }

    @PostMapping("/adminchat/delete/{id}")
    public String adminChatDelete(@PathVariable("id") Short id,
                                  RedirectAttributes redirectAttributes,
                                  @AuthenticationPrincipal UserDetails userDetails){
        chatService.delete(id);
        redirectAttributes.addFlashAttribute("removeChat", id);
        return "redirect:/adminchatslist";

    }

    @GetMapping("/adminchat/updatepage/{id}")
    public String adminChatUpdate(Model model, @AuthenticationPrincipal UserDetails userDetails,
                                     @PathVariable("id") Short id){
        UserReadDto currentUser = userService.findByUsername(userDetails.getUsername()).get();
        model.addAttribute("countNotReadMessages", chatService.findCountMessagesInAllChats(currentUser.getId(), currentUser.getRole().getAuthority()));
        model.addAttribute("currentUser", currentUser);
        model.addAttribute("chat", chatService.findChatById(id));
        return "app/_admin_chat_update";

    }

    @PostMapping("/adminchat/update/{id}")
    public String adminChatUpdateProcess(@PathVariable("id") Short id,
                                            @ModelAttribute @Validated ChatNameDto chatName,
                                            BindingResult bindingResult,
                                            RedirectAttributes redirectAttributes,
                                            @AuthenticationPrincipal UserDetails userDetails) throws IOException {

        if (bindingResult.hasErrors()) {
            redirectAttributes.addFlashAttribute("errors", "errors");
            return "redirect:/adminchat/updatepage/" + id;
        }

        chatService.updateChatName(id, chatName.getRuName(), chatName.getEnName());

        redirectAttributes.addFlashAttribute("updateChat", chatName);

        return "redirect:/adminchatslist";
    }

    @GetMapping("/adminchatcreate")
    public String adminChatCreate(Model model, @AuthenticationPrincipal UserDetails userDetails){
        UserReadDto currentUser = userService.findByUsername(userDetails.getUsername()).get();
        model.addAttribute("countNotReadMessages", chatService.findCountMessagesInAllChats(currentUser.getId(), currentUser.getRole().getAuthority()));
        model.addAttribute("currentUser", currentUser);
        return "app/_admin_chat_create";

    }

    @PostMapping("/adminchatcreateprocess")
    public String adminChatCreateProcess(@ModelAttribute @Validated ChatNameDto chatNameDto,
                                         BindingResult bindingResult,
                                         RedirectAttributes redirectAttributes,
                                         @AuthenticationPrincipal UserDetails userDetails) throws IOException {

        if (bindingResult.hasErrors()) {
            redirectAttributes.addFlashAttribute("errors", "errors");
            return "redirect:/adminchatcreate";
        }


        ChatReadDto chatReadDto = chatService.createChatForAllUsers(
                new ChatCreateEditDto(new ChatNameDto(chatNameDto.getRuName(), chatNameDto.getEnName()))
        );


        redirectAttributes.addFlashAttribute("createdChat", chatReadDto);

        return "redirect:/adminchatslist";
    }


}
