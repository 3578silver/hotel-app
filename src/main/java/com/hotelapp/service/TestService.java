package com.hotelapp.service;

import com.hotelapp.database.dto.booking.ConveniencyCreateEditDto;
import com.hotelapp.database.dto.booking.ConveniencyReadDto;
import com.hotelapp.database.dto.booking.TypeConveniencesCreateEditDto;
import com.hotelapp.database.entity.Conveniency;
import com.hotelapp.database.entity.Room;
import com.hotelapp.database.entity.RoomType;
import com.hotelapp.database.entity.TypeConveniences;
import com.hotelapp.database.repository.ConveniencyRepository;
import com.hotelapp.database.repository.RoomTypeRepository;
import com.hotelapp.database.repository.TypeConveniencesRepository;
import com.hotelapp.mapper.booking.ConveniencyCreateEditMapper;
import com.hotelapp.mapper.booking.ConveniencyReadMapper;
import com.hotelapp.mapper.booking.TypeConveniencesCreateEditMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class TestService {
    private final ConveniencyCreateEditMapper conveniencyCreateEditMapper;
    private final ConveniencyRepository conveniencyRepository;
    private final ConveniencyReadMapper conveniencyReadMapper;
    private final RoomTypeRepository roomTypeRepository;
    private final TypeConveniencesRepository typeConveniencesRepository;
    private final TypeConveniencesCreateEditMapper typeConveniencesCreateEditMapper;

    public List<ConveniencyReadDto> createAllRoomTypeConveniencies(List<ConveniencyCreateEditDto> conveniencyCreateEditDtos) {
        for (ConveniencyCreateEditDto conveniencyCreateEditDto : conveniencyCreateEditDtos) {
            System.out.println(conveniencyCreateEditDto);
        }
        List<ConveniencyReadDto> conveniencies = conveniencyCreateEditDtos.stream()
                .map(conveniencyCreateEditMapper::map)
                .map(conveniencyRepository::saveAndFlush)
                .map(conveniencyReadMapper::map)
                .toList();

        List<RoomType> roomTypes = roomTypeRepository.findAll();
        for (ConveniencyReadDto conveniency : conveniencies) {
            for (RoomType roomType : roomTypes) {
                Optional.of(new TypeConveniencesCreateEditDto(roomType.getId(), conveniency.getId()))
                        .map(typeConveniencesCreateEditMapper::map)
                        .map(typeConveniencesRepository::saveAndFlush);
            }
        }

        return conveniencies;
    }

}
