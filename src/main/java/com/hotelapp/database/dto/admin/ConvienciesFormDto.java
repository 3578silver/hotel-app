package com.hotelapp.database.dto.admin;

import lombok.Value;

import java.util.ArrayList;
import java.util.List;


@Value
public class ConvienciesFormDto {
    List<Integer> options = new ArrayList<>(); // list of options ids
}
