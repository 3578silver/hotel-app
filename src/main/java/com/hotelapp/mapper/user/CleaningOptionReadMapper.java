package com.hotelapp.mapper.user;

import com.hotelapp.database.dto.services.locales.CleaningOptionReadDto;
import com.hotelapp.database.entity.CleaningOption;
import com.hotelapp.mapper.Mapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class CleaningOptionReadMapper implements Mapper<CleaningOption, CleaningOptionReadDto> {
    @Override
    public CleaningOptionReadDto map(CleaningOption cleaningOption) {
        return new CleaningOptionReadDto(
                cleaningOption.getId(),
                cleaningOption.getRuName(),
                cleaningOption.getEnName()
        );
    }
}
