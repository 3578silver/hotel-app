package com.hotelapp.mapper.booking.locales;

import com.hotelapp.database.dto.booking.locales.ConveniencyDescriptionDto;
import com.hotelapp.database.entity.ConveniencyDescription;
import com.hotelapp.mapper.Mapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class ConveniencyDescriptionReadMapper implements Mapper<ConveniencyDescription, ConveniencyDescriptionDto> {
    @Override
    public ConveniencyDescriptionDto map(ConveniencyDescription conveniencyDescription) {
        return new ConveniencyDescriptionDto(
                conveniencyDescription.getRuName(),
                conveniencyDescription.getEnName()
        );
    }
}
