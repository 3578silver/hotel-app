package com.hotelapp.database.repository;

import com.hotelapp.database.entity.ConveniencyDescription;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface ConveniencyDescriptionRepository extends
        JpaRepository<ConveniencyDescription, Integer>,
        QuerydslPredicateExecutor<ConveniencyDescription> {

    ConveniencyDescription findByRuNameAndEnName(String ruName, String enName);
}
