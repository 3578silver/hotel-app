package com.hotelapp.mapper.user;

import com.hotelapp.database.dto.user.ChatReadDto;
import com.hotelapp.database.dto.user.LoyaltyReadDto;
import com.hotelapp.database.dto.user.UserReadDto;
import com.hotelapp.database.entity.Chat;
import com.hotelapp.database.entity.User;
import com.hotelapp.mapper.Mapper;
import com.hotelapp.mapper.user.locales.ChatNameReadMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@RequiredArgsConstructor
public class ChatReadMapper implements Mapper<Chat, ChatReadDto> {

    private final ChatNameReadMapper chatNameReadMapper;

    @Override
    public ChatReadDto map(Chat chat) {
        return new ChatReadDto(
                chat.getId(),
                Optional.of(chat.getName()).map(chatNameReadMapper::map).orElse(null)
        );
    }
}