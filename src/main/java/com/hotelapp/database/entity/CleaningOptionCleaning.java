package com.hotelapp.database.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "cleaning_options_cleaning")
public class CleaningOptionCleaning implements BaseEntity<Long>{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "cleaning_id", nullable = false)
    private Cleaning cleaning;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "cleaning_option_id", nullable = false)
    private CleaningOption cleaningOption;

    public void setCleaning(Cleaning cleaning) {
        this.cleaning = cleaning;
        this.cleaning.getCleaningOptionCleanings().add(this);
    }

    public void setCleaningOption(CleaningOption cleaningOption) {
        this.cleaningOption = cleaningOption;
        this.cleaningOption.getCleaningOptionCleanings().add(this);
    }
}