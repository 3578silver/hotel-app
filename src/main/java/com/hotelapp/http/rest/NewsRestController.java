package com.hotelapp.http.rest;

import com.hotelapp.database.dto.news.NewsCreateEditDto;
import com.hotelapp.database.dto.news.NewsReadDto;
import com.hotelapp.service.NewsService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/news")
@RequiredArgsConstructor
public class NewsRestController {
    private final NewsService newsService;

    @PostMapping(path = "/createNews", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<NewsReadDto> createNews(@Validated @RequestBody NewsCreateEditDto newsCreateEditDto) {
        return new ResponseEntity<>(newsService.createNews(newsCreateEditDto), HttpStatus.CREATED);
    }

    @GetMapping(path = "/findAllNews", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<NewsReadDto>> findAllNews() {
       return new ResponseEntity<>(newsService.findAllNews(), HttpStatus.OK);
    }

    @DeleteMapping("/deleteNews/{id}")
    public ResponseEntity<Boolean> deleteNews(@PathVariable("id") Long id) {
        return new ResponseEntity<>(newsService.deleteNewsById(id), HttpStatus.OK);
    }
}
