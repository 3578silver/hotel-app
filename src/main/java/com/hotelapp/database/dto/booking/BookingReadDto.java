package com.hotelapp.database.dto.booking;

import com.hotelapp.database.dto.user.UserReadDto;
import com.hotelapp.database.entity.BookingStatus;
import jakarta.validation.constraints.NotEmpty;
import lombok.Value;

import java.time.LocalDate;

@Value
public class BookingReadDto {
    Long id;

    RoomTypeReadDto roomTypeReadDto;

    RoomReadDto roomReadDto;

    BookingStatus bookingStatus;

    LocalDate bookingDate;

    Boolean isPaid;

    Integer totalPrice;

    LocalDate checkIn;

    LocalDate checkOut;

    UserReadDto userReadDto;
}
