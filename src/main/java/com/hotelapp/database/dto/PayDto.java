package com.hotelapp.database.dto;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.Pattern;
import lombok.Value;
import org.hibernate.validator.constraints.Length;

@Value
public class PayDto {
    @Length(min = 16)
    String cardNumber;

    @Pattern(regexp = "^[a-zA-Zа-яА-Я]+\\s[a-zA-Zа-яА-Я]+$")
    String cardHolderName;

    @Pattern(regexp = "^\\d{2}/\\d{2}$")
    String expiryDate;

    @Length(min = 3, max = 3)
    String cardCvc;

    @Email
    String email;
}
