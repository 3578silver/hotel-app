package com.hotelapp.database.dto.food.locales;

import jakarta.validation.constraints.NotEmpty;
import lombok.Value;

@Value
public class FoodDescriptionDto {
    @NotEmpty
    String ruName;
    @NotEmpty
    String enName;
}
