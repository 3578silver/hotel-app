package com.hotelapp.mapper.booking;

import com.hotelapp.database.dto.booking.TypeConveniencesCreateEditDto;
import com.hotelapp.database.entity.TypeConveniences;
import com.hotelapp.database.repository.ConveniencyRepository;
import com.hotelapp.database.repository.RoomRepository;
import com.hotelapp.database.repository.RoomTypeRepository;
import com.hotelapp.mapper.Mapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class TypeConveniencesCreateEditMapper implements Mapper<TypeConveniencesCreateEditDto, TypeConveniences> {
    private final RoomTypeRepository roomTypeRepository;
    private final ConveniencyRepository conveniencyRepository;

    @Override
    public TypeConveniences map(TypeConveniencesCreateEditDto typeConveniencesCreateEditDto) {
        TypeConveniences typeConveniences = new TypeConveniences();
        copy(typeConveniencesCreateEditDto, typeConveniences);
        return typeConveniences;
    }

    @Override
    public TypeConveniences map(TypeConveniencesCreateEditDto typeConveniencesCreateEditDto, TypeConveniences typeConveniences) {
        copy(typeConveniencesCreateEditDto, typeConveniences);
        return typeConveniences;
    }

    private void copy(TypeConveniencesCreateEditDto typeConveniencesCreateEditDto, TypeConveniences typeConveniences) {
        typeConveniences.setRoomType(roomTypeRepository.findById(
                typeConveniencesCreateEditDto.getRoomTypeId()).orElse(null));
        typeConveniences.setConveniency(conveniencyRepository.findById(
                typeConveniencesCreateEditDto.getConveniencyId()).orElse(null));
    }
}
