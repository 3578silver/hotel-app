package com.hotelapp.mapper.booking;

import com.hotelapp.database.dto.booking.RoomTypeCreateEditDto;
import com.hotelapp.database.dto.booking.RoomTypeReadDto;
import com.hotelapp.database.entity.RoomType;
import com.hotelapp.database.entity.RoomTypeDescription;
import com.hotelapp.database.entity.RoomTypeName;
import com.hotelapp.mapper.Mapper;
import com.hotelapp.mapper.booking.locales.RoomTypeDescriptionCreateEditMapper;
import com.hotelapp.mapper.booking.locales.RoomTypeNameCreateEditMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@RequiredArgsConstructor
public class RoomTypeCreateEditMapper implements Mapper<RoomTypeCreateEditDto, RoomType> {

    private final RoomTypeNameCreateEditMapper roomTypeNameCreateEditMapper;
    private final RoomTypeDescriptionCreateEditMapper roomTypeDescriptionCreateEditMapper;

    @Override
    public RoomType map(RoomTypeCreateEditDto roomTypeCreateEditDto, RoomType roomType) {
        copy(roomTypeCreateEditDto, roomType);
        return roomType;
    }

    @Override
    public RoomType map(RoomTypeCreateEditDto roomTypeCreateEditDto) {
        RoomType roomType = new RoomType();
        copy(roomTypeCreateEditDto, roomType);
        return roomType;
    }

    private void copy(RoomTypeCreateEditDto roomTypeCreateEditDto, RoomType roomType) {
        RoomTypeName roomTypeName = Optional.of(roomTypeCreateEditDto.getRoomTypeNameDto())
                        .map(roomTypeNameCreateEditMapper::map).orElse(null);
        RoomTypeDescription roomTypeDescription = Optional.of(roomTypeCreateEditDto.getRoomTypeDescriptionDto())
                .map(roomTypeDescriptionCreateEditMapper::map).orElse(null);
        roomType.setRoomTypeName(roomTypeName);
        roomType.setRoomTypeDescription(roomTypeDescription);
        roomType.setImagePath(roomTypeCreateEditDto.getImagePath());
        roomType.setPrice(roomTypeCreateEditDto.getPrice());
    }
}
