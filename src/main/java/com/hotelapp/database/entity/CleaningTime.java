package com.hotelapp.database.entity;

public enum CleaningTime {
    NOW,
    MORNING,
    AFTERNOON,
    EVENING,
    NIGHT
}
