package com.hotelapp.database.dto.food;

import com.hotelapp.database.dto.user.UserReadDto;
import com.hotelapp.database.dto.booking.RoomReadDto;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Setter;
import lombok.Value;
import lombok.experimental.NonFinal;

@Value
public class FoodCartCreateEditDto {


    Integer sum;
    @NonFinal
    @Setter
    Integer room;

    //очистка комментария, суммы и блюд после заказа
    String comment;
}
