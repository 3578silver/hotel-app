package com.hotelapp.mapper.news;

import com.hotelapp.database.dto.news.NewsCreateEditDto;
import com.hotelapp.database.entity.*;
import com.hotelapp.mapper.Mapper;
import com.hotelapp.mapper.news.locales.NewsDescriptionCreateEditMapper;
import com.hotelapp.mapper.news.locales.NewsNameCreateEditMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@RequiredArgsConstructor
public class NewsCreateEditMapper implements Mapper<NewsCreateEditDto, News> {

    private final NewsDescriptionCreateEditMapper newsDescriptionCreateEditMapper;
    private final NewsNameCreateEditMapper newsNameCreateEditMapper;

    @Override
    public News map(NewsCreateEditDto fromObject, News toObject) {
        copy(fromObject, toObject);
        return toObject;
    }

    @Override
    public News map(NewsCreateEditDto newsCreateEditDto) {
        News news = new News();
        copy(newsCreateEditDto, news);
        return news;
    }

    private void copy(NewsCreateEditDto newsCreateEditDto, News news) {

        NewsDescription newsDescription = Optional.of(newsCreateEditDto.getDescription())
                .map(newsDescriptionCreateEditMapper::map)
                .orElse(null);

        NewsName newsName = Optional.of(newsCreateEditDto.getName())
                .map(newsNameCreateEditMapper::map)
                .orElse(null);

        news.setNewsDescription(newsDescription);
        news.setNewsName(newsName);
        news.setImageLink(newsCreateEditDto.getImageLink());
    }

}
