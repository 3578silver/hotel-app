package com.hotelapp.mapper.food.locales;

import com.hotelapp.database.dto.food.locales.FoodTypeDto;
import com.hotelapp.database.dto.user.LoyaltyReadDto;
import com.hotelapp.database.entity.FoodType;
import com.hotelapp.database.entity.Loyalty;
import com.hotelapp.mapper.Mapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class FoodTypeReadMapper implements Mapper<FoodType, FoodTypeDto> {
    @Override
    public FoodTypeDto map(FoodType foodType) {
        return new FoodTypeDto(
                foodType.getRuName(),
                foodType.getEnName()
        );
    }
}