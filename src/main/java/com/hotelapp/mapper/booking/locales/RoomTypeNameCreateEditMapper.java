package com.hotelapp.mapper.booking.locales;

import com.hotelapp.database.dto.booking.RoomTypeReadDto;
import com.hotelapp.database.dto.booking.locales.RoomTypeNameDto;
import com.hotelapp.database.entity.RoomType;
import com.hotelapp.database.entity.RoomTypeName;
import com.hotelapp.mapper.Mapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class RoomTypeNameCreateEditMapper implements Mapper<RoomTypeNameDto, RoomTypeName> {
    @Override
    public RoomTypeName map(RoomTypeNameDto roomTypeNameDto, RoomTypeName roomTypeName) {
        copy(roomTypeNameDto, roomTypeName);
        return roomTypeName;
    }

    @Override
    public RoomTypeName map(RoomTypeNameDto roomTypeNameDto) {
        RoomTypeName roomTypeName = new RoomTypeName();
        copy(roomTypeNameDto, roomTypeName);
        return roomTypeName;
    }

    private void copy(RoomTypeNameDto roomTypeNameDto, RoomTypeName roomTypeName) {
        roomTypeName.setRuName(roomTypeNameDto.getRuName());
        roomTypeName.setEnName(roomTypeNameDto.getEnName());
    }
}
