package com.hotelapp.database.repository;

import com.hotelapp.database.entity.Chat;
import com.hotelapp.database.entity.Room;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import java.util.Optional;

public interface ChatRepository extends
        JpaRepository<Chat, Short>,
        QuerydslPredicateExecutor<Chat> {


}