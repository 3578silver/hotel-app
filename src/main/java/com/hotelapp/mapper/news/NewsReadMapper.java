package com.hotelapp.mapper.news;

import com.hotelapp.database.dto.news.NewsReadDto;
import com.hotelapp.database.dto.news.locales.NewsDescriptionDto;
import com.hotelapp.database.dto.news.locales.NewsNameDto;
import com.hotelapp.database.entity.News;
import com.hotelapp.mapper.Mapper;
import com.hotelapp.mapper.news.locales.NewsDescriptionReadMapper;
import com.hotelapp.mapper.news.locales.NewsNameReadMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@RequiredArgsConstructor
public class NewsReadMapper implements Mapper<News, NewsReadDto> {

    private final NewsNameReadMapper newsNameReadMapper;
    private final NewsDescriptionReadMapper newsDescriptionReadMapper;


    @Override
    public NewsReadDto map(News news) {
        NewsNameDto newsNameDto = Optional.ofNullable(news.getNewsName())
                .map(newsNameReadMapper::map)
                .orElse(null);

        NewsDescriptionDto newsDescriptionDto = Optional.ofNullable(news.getNewsDescription())
                .map(newsDescriptionReadMapper::map)
                .orElse(null);


        return new NewsReadDto(
                news.getId(),
                newsNameDto,
                newsDescriptionDto,
                news.getImageLink()
        );
    }
}
