package com.hotelapp.database.dto.services;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.hotelapp.database.dto.user.UserReadDto;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.Value;
import lombok.experimental.NonFinal;
@AllArgsConstructor(onConstructor_ = {@JsonCreator(mode = JsonCreator.Mode.PROPERTIES)})
@Value
public class CartCreateEditDto {
    @NotNull
    Integer sum;
}
