package com.hotelapp.database.repository;

import com.hotelapp.database.entity.News;
import com.hotelapp.database.entity.RoomType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import java.util.List;

public interface NewsRepository extends
        JpaRepository<News, Long>,
        QuerydslPredicateExecutor<News> {

    List<News> findAllByOrderByIdDesc();

}
