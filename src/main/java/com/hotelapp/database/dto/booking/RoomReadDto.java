package com.hotelapp.database.dto.booking;

import lombok.Value;

@Value
public class RoomReadDto {
    Integer id;
    RoomTypeReadDto roomTypeReadDto;
}
