package com.hotelapp.database.dto.user;

import com.hotelapp.database.dto.user.locales.ChatNameDto;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Value;

@Value
public class ChatCreateEditDto {
    @NotNull
    ChatNameDto name;
}
