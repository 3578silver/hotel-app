package com.hotelapp.database.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.ColumnDefault;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "food_cart")
public class FoodCart implements BaseEntity<Long>{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(nullable = false)
    private Integer sum = 0;

    @Builder.Default
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "foodCart", orphanRemoval = true)
    private List<FoodCartFood> foodCartFoods = new ArrayList<>();


    @Column(nullable = false)
    private Integer room;

    @Column
    private String comment;



}
