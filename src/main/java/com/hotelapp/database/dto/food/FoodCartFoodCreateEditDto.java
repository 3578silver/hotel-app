package com.hotelapp.database.dto.food;

import jakarta.validation.constraints.NotNull;
import lombok.Setter;
import lombok.Value;
import lombok.experimental.NonFinal;

@Value
public class FoodCartFoodCreateEditDto {
    @NotNull
    Long foodCartId;
    @NotNull
    Integer foodId;
}