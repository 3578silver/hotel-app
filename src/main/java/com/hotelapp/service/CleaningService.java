package com.hotelapp.service;

import com.hotelapp.database.dto.services.*;
import com.hotelapp.database.dto.services.locales.CleaningOptionCreateEditDto;
import com.hotelapp.database.dto.services.locales.CleaningOptionReadDto;
import com.hotelapp.database.repository.CleaningOptionCleaningRepository;
import com.hotelapp.database.repository.CleaningOptionRepository;
import com.hotelapp.database.repository.CleaningRepository;
import com.hotelapp.mapper.user.*;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class CleaningService {
    private final CleaningRepository cleaningRepository;
    private final CleaningReadMapper cleaningReadMapper;
    private final CleaningCreateEditMapper cleaningCreateEditMapper;
    private final CleaningOptionCreateEditMapper cleaningOptionCreateEditMapper;
    private final CleaningOptionCleaningCreateEditMapper cleaningOptionCleaningCreateEditMapper;
    private final CleaningOptionReadMapper cleaningOptionReadMapper;
    private final CleaningOptionCleaningReadMapper cleaningOptionCleaningReadMapper;
    private final CleaningOptionRepository cleaningOptionRepository;
    private final CleaningOptionCleaningRepository cleaningOptionCleaningRepository;


    @Transactional
    public CleaningReadDto createCleaning(CleaningCreateEditDto cleaningCreateEditDto) {
        return Optional.of(cleaningCreateEditDto)
                .map(cleaningCreateEditMapper::map)
                .map(cleaningRepository::save)
                .map(cleaningReadMapper::map)
                .orElseThrow();
    }

    @Transactional
    public CleaningOptionReadDto createCleaningOption(CleaningOptionCreateEditDto cleaningOptionCreateEditDto) {
        return Optional.of(cleaningOptionCreateEditDto)
                .map(cleaningOptionCreateEditMapper::map)
                .map(cleaningOptionRepository::save)
                .map(cleaningOptionReadMapper::map)
                .orElseThrow();
    }

    @Transactional
    public CleaningOptionCleaningReadDto createCleaningOptionsCleaning(CleaningOptionCleaningCreateEditDto cleaningOptionCleaningCreateEditDto) {
        return Optional.of(cleaningOptionCleaningCreateEditDto)
                .map(cleaningOptionCleaningCreateEditMapper::map)
                .map(cleaningOptionCleaningRepository::save)
                .map(cleaningOptionCleaningReadMapper::map)
                .orElseThrow();
    }

    public List<CleaningOptionReadDto> findAllCleaningOptions() {
        return cleaningOptionRepository.findAll()
                .stream()
                .map(cleaningOptionReadMapper::map)
                .toList();
    }

    @Transactional
    public CleaningReadDto updateCleaning(CleaningCreateEditDto cleaningCreateEditDto, Long cleaningId) {
        return Optional.of(cleaningRepository.updateTimeRoomComment(cleaningCreateEditDto.getTime().name(),
                        cleaningCreateEditDto.getRoom(),
                        cleaningCreateEditDto.getComment(),
                        cleaningId))
                .map(cleaningReadMapper::map).orElse(null);

    }
}
