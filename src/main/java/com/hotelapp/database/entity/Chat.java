package com.hotelapp.database.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.hotelapp.database.dto.user.locales.ChatNameDto;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "chat")
public class Chat implements BaseEntity<Short>{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Short id;

    @NotNull
    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(nullable = false)
    private ChatName name;

    @Builder.Default
    @OneToMany(mappedBy = "chat", orphanRemoval = true)
    private List<UserChat> userChats = new ArrayList<>();
}