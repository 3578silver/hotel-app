package com.hotelapp.database.dto.user;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Value;

@Value
public class LoyaltyCreateEditDto {
    @NotEmpty
    String name;
    @NotNull
    Integer minSpent;
    @NotNull
    Short sale;
}
