package com.hotelapp.mapper.services;

import com.hotelapp.database.dto.services.ServiceCartReadDto;
import com.hotelapp.database.entity.ServiceCart;
import com.hotelapp.mapper.Mapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@RequiredArgsConstructor
public class ServiceCartReadMapper implements Mapper<ServiceCart, ServiceCartReadDto> {

    private final ServiceReadMapper serviceReadMapper;

    private final CartReadMapper cartReadMapper;

    @Override
    public ServiceCartReadDto map(ServiceCart serviceCart) {
        return new ServiceCartReadDto(
                serviceCart.getId(),
                Optional.of(serviceCart.getService()).map(serviceReadMapper::map).orElse(null),
                Optional.of(serviceCart.getCart()).map(cartReadMapper::map).orElse(null)
        );
    }
}
