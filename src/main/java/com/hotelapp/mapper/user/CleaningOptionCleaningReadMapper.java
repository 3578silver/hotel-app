package com.hotelapp.mapper.user;

import com.hotelapp.database.dto.services.CleaningOptionCleaningCreateEditDto;
import com.hotelapp.database.dto.services.CleaningOptionCleaningReadDto;
import com.hotelapp.database.dto.services.locales.CleaningOptionReadDto;
import com.hotelapp.database.entity.CleaningOption;
import com.hotelapp.database.entity.CleaningOptionCleaning;
import com.hotelapp.mapper.Mapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@RequiredArgsConstructor
public class CleaningOptionCleaningReadMapper implements Mapper<CleaningOptionCleaning, CleaningOptionCleaningReadDto> {
    private final CleaningReadMapper cleaningReadMapper;
    private final CleaningOptionReadMapper cleaningOptionReadMapper;

    @Override
    public CleaningOptionCleaningReadDto map(CleaningOptionCleaning cleaningOptionCleaning) {
        return new CleaningOptionCleaningReadDto(
                cleaningOptionCleaning.getId(),
                Optional.of(cleaningOptionCleaning.getCleaningOption()).map(cleaningOptionReadMapper::map).orElse(null),
                Optional.of(cleaningOptionCleaning.getCleaning()).map(cleaningReadMapper::map).orElse(null)
        );
    }
}
