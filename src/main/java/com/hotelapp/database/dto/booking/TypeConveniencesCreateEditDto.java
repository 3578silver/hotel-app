package com.hotelapp.database.dto.booking;

import jakarta.validation.constraints.NotNull;
import lombok.Value;

@Value
public class TypeConveniencesCreateEditDto {
    @NotNull
    Integer roomTypeId;
    @NotNull
    Integer conveniencyId;
}
