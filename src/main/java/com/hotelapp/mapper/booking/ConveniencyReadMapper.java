package com.hotelapp.mapper.booking;

import com.hotelapp.database.dto.booking.ConveniencyReadDto;
import com.hotelapp.database.entity.Conveniency;
import com.hotelapp.mapper.Mapper;
import com.hotelapp.mapper.booking.locales.ConveniencyDescriptionReadMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@RequiredArgsConstructor
public class ConveniencyReadMapper implements Mapper<Conveniency, ConveniencyReadDto> {

    private final ConveniencyDescriptionReadMapper conveniencyDescriptionMapper;

    @Override
    public ConveniencyReadDto map(Conveniency conveniency) {
        return new ConveniencyReadDto(
                conveniency.getId(),
                conveniency.getSvgLink(),
                Optional.of(conveniency.getConveniencyDescription()).map(conveniencyDescriptionMapper::map).orElse(null)
        );
    }
}
