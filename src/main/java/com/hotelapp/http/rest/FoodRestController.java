package com.hotelapp.http.rest;

import com.hotelapp.database.dto.food.*;
import com.hotelapp.service.FoodService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/food")
@RequiredArgsConstructor
public class FoodRestController {

    private final FoodService foodService;

    @GetMapping(path = "/findAllFood", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<FoodReadDto>> findAllFood() {
        return new ResponseEntity<>(foodService.findAllFood(), HttpStatus.OK);
    }

    @GetMapping(path = "/findAllFoodCart", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<FoodCartReadDto>> findAllFoodCart() {
        return new ResponseEntity<>(foodService.findAllFoodCart(), HttpStatus.OK);
    }

    @GetMapping(path = "/findAllFoodCartFood", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<FoodCartFoodReadDto>> findAllFoodCartFood() {
        return new ResponseEntity<>(foodService.findAllFoodCartFood(), HttpStatus.OK);
    }


//    @GetMapping("/{id}")
//    public UserReadDto findById(@PathVariable Long id) {
//        return userService.findById(id)
//                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
//    }

    @PostMapping(path = "/createFood", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<FoodReadDto> createFood(@Validated @RequestBody FoodCreateEditDto foodCreateEditDto){
        return new ResponseEntity<>(foodService.createFood(foodCreateEditDto), HttpStatus.CREATED);
    }

    @PostMapping(path = "/createFoodCart", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<FoodCartReadDto> createFoodCart(@Validated @RequestBody FoodCartCreateEditDto foodCartCreateEditDto){
        return new ResponseEntity<>(foodService.createFoodCart(foodCartCreateEditDto), HttpStatus.CREATED);
    }

    @PostMapping(path = "/createFoodCartFood", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<FoodCartFoodReadDto> createFoodCartFood(@Validated @RequestBody FoodCartFoodCreateEditDto foodCartFoodCreateEditDto){
        return new ResponseEntity<>(foodService.createFoodCartFood(foodCartFoodCreateEditDto), HttpStatus.CREATED);
    }

    @DeleteMapping("/deleteFood/{id}")
    public ResponseEntity<Boolean> deleteFood(@PathVariable("id") Integer id) {
        return new ResponseEntity<>(foodService.deleteFoodById(id), HttpStatus.OK);
    }

    @DeleteMapping()
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public boolean cleanFoodCart(Long id){
        return foodService.cleanFoodCart(id);
    }




//    @PutMapping("/{id}")
//    public UserReadDto update(@PathVariable Long id, @Validated @RequestBody UserCreateEditDto user) {
//        return userService.update(id, user)
//                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
//    };
//
//
//    @DeleteMapping("/{id}")
//    @ResponseStatus(HttpStatus.NO_CONTENT)
//    public void delete(@PathVariable Long id){
//        if (!userService.delete(id)){
//            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
//        }
//    }
}