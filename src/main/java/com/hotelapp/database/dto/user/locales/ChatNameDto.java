package com.hotelapp.database.dto.user.locales;

import jakarta.validation.constraints.NotEmpty;
import lombok.Value;

@Value
public class ChatNameDto {
    @NotEmpty
    String ruName;

    @NotEmpty
    String enName;
}
