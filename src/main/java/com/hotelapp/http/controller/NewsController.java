package com.hotelapp.http.controller;

import com.hotelapp.database.dto.AuthenticationRequest;
import com.hotelapp.database.dto.AvailabilityDto;
import com.hotelapp.database.dto.user.UserReadDto;
import com.hotelapp.service.ChatService;
import com.hotelapp.service.NewsService;
import com.hotelapp.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;

@Controller
@RequiredArgsConstructor
public class NewsController {
    private final NewsService newsService;
    private final UserService userService;
    private final ChatService chatService;

    @GetMapping("/news")
    public String getAllNews(Model model,
                             @AuthenticationPrincipal UserDetails userDetails) {
        model.addAttribute("news", newsService.findAllNewsByOrderByIdDesc());
        UserReadDto currentUser = userService.findByUsername(userDetails.getUsername()).get();
        model.addAttribute("countNotReadMessages", chatService.findCountMessagesInAllChats(currentUser.getId(), currentUser.getRole().getAuthority()));
        model.addAttribute("currentUser", currentUser);
        model.addAttribute("url", "news");
        return "app/_feed";
    }

}
