package com.hotelapp.database.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "food")
public class Food implements BaseEntity<Integer>{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotNull
    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(nullable = false)
    private FoodName name;

    @NotNull
    @Column(nullable = false)
    private Integer cost;

    @NotNull
    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "food_description_id", nullable = false)
    private FoodDescription description;

    @NotNull
    @ManyToOne()
    @JoinColumn(name = "food_type_id", nullable = false)
    private FoodType type;

    @Builder.Default
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "food", orphanRemoval = true)
    private List<FoodCartFood> foodCartFoods = new ArrayList<>();

    @NotEmpty
    @Column(name = "image_link")
    private String imageLink;

}
