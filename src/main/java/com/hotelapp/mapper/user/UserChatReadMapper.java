package com.hotelapp.mapper.user;

import com.hotelapp.database.dto.user.ChatReadDto;
import com.hotelapp.database.dto.user.UserChatReadDto;
import com.hotelapp.database.entity.Chat;
import com.hotelapp.database.entity.UserChat;
import com.hotelapp.mapper.Mapper;
import com.hotelapp.mapper.user.locales.ChatNameReadMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@RequiredArgsConstructor
public class UserChatReadMapper implements Mapper<UserChat, UserChatReadDto> {

    private final ChatReadMapper chatReadMapper;
    private final UserReadMapper userReadMapper;

    @Override
    public UserChatReadDto map(UserChat userChat) {
        return new UserChatReadDto(
                userChat.getId(),
                Optional.of(userChat.getUser()).map(userReadMapper::map).orElse(null),
                Optional.of(userChat.getChat()).map(chatReadMapper::map).orElse(null)
        );
    }
}