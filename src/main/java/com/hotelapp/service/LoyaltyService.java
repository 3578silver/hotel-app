package com.hotelapp.service;

import com.hotelapp.database.dto.food.FoodCartCreateEditDto;
import com.hotelapp.database.dto.food.FoodCartReadDto;
import com.hotelapp.database.dto.user.LoyaltyCreateEditDto;
import com.hotelapp.database.dto.user.LoyaltyReadDto;
import com.hotelapp.database.repository.LoyaltyRepository;
import com.hotelapp.mapper.user.LoyaltyCreateEditMapper;
import com.hotelapp.mapper.user.LoyaltyReadMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class LoyaltyService {

    private final LoyaltyCreateEditMapper loyaltyCreateEditMapper;
    private final LoyaltyReadMapper loyaltyReadMapper;
    private final LoyaltyRepository loyaltyRepository;

    @Transactional
    public LoyaltyReadDto createLoyalty(LoyaltyCreateEditDto loyaltyCreateEditDto) {
        return Optional.of(loyaltyCreateEditDto)
                .map(loyaltyCreateEditMapper::map)
                .map(loyaltyRepository::save)
                .map(loyaltyReadMapper::map)
                .orElseThrow();
    }

    @Transactional
    public List<LoyaltyReadDto> getAllLoyalties() {
        return loyaltyRepository.findAll()
              .stream()
              .map(loyaltyReadMapper::map)
              .collect(Collectors.toList());
    }

}
