package com.hotelapp.mapper.booking.locales;

import com.hotelapp.database.dto.booking.RoomTypeReadDto;
import com.hotelapp.database.dto.booking.locales.RoomTypeNameDto;
import com.hotelapp.database.entity.RoomType;
import com.hotelapp.database.entity.RoomTypeName;
import com.hotelapp.mapper.Mapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class RoomTypeNameReadMapper implements Mapper<RoomTypeName, RoomTypeNameDto> {

    @Override
    public RoomTypeNameDto map(RoomTypeName roomTypeName) {
        return new RoomTypeNameDto(
                roomTypeName.getRuName(),
                roomTypeName.getEnName()
        );
    }
}