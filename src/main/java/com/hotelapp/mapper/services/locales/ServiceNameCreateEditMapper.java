package com.hotelapp.mapper.services.locales;

import com.hotelapp.database.dto.services.locales.ServiceNameDto;
import com.hotelapp.database.entity.ServiceName;
import com.hotelapp.mapper.Mapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class ServiceNameCreateEditMapper implements Mapper<ServiceNameDto, ServiceName> {
    @Override
    public ServiceName map(ServiceNameDto serviceNameDto, ServiceName serviceName) {
        copy(serviceNameDto, serviceName);
        return serviceName;
    }

    @Override
    public ServiceName map(ServiceNameDto serviceNameDto) {
        ServiceName serviceName = new ServiceName();
        copy(serviceNameDto, serviceName);
        return serviceName;
    }

    private void copy(ServiceNameDto serviceNameDto, ServiceName serviceName) {
        serviceName.setRuName(serviceNameDto.getRuName());
        serviceName.setEnName(serviceNameDto.getEnName());
    }
}
