package com.hotelapp.mapper.food;

import com.hotelapp.database.dto.food.FoodCartCreateEditDto;
import com.hotelapp.database.dto.user.LoyaltyCreateEditDto;
import com.hotelapp.database.entity.FoodCart;
import com.hotelapp.database.entity.Loyalty;
import com.hotelapp.database.repository.UserRepository;
import com.hotelapp.mapper.Mapper;
import com.hotelapp.mapper.user.UserCreateEditMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@RequiredArgsConstructor
public class FoodCartCreateEditMapper implements Mapper<FoodCartCreateEditDto, FoodCart> {

    private final UserRepository userRepository;

    @Override
    public FoodCart map(FoodCartCreateEditDto foodCartCreateEditDto) {
        FoodCart foodCart = new FoodCart();
        copy(foodCartCreateEditDto, foodCart);
        return foodCart;
    }

    @Override
    public FoodCart map(FoodCartCreateEditDto foodCartCreateEditDto, FoodCart foodCart) {
        copy(foodCartCreateEditDto, foodCart);
        return foodCart;
    }


    private void copy(FoodCartCreateEditDto foodCartCreateEditDto, FoodCart foodCart) {
        foodCart.setComment(foodCartCreateEditDto.getComment());
        foodCart.setRoom(foodCartCreateEditDto.getRoom());
        foodCart.setSum(foodCartCreateEditDto.getSum());
    }
}