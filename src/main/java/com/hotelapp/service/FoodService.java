package com.hotelapp.service;

import com.hotelapp.database.dto.booking.BookingReadDto;
import com.hotelapp.database.dto.booking.locales.RoomTypeDescriptionDto;
import com.hotelapp.database.dto.booking.locales.RoomTypeNameDto;
import com.hotelapp.database.dto.food.*;
import com.hotelapp.database.dto.food.locales.FoodDescriptionDto;
import com.hotelapp.database.dto.food.locales.FoodNameDto;
import com.hotelapp.database.dto.food.locales.FoodTypeDto;
import com.hotelapp.database.entity.FoodCart;
import com.hotelapp.database.entity.User;
import com.hotelapp.database.repository.*;
import com.hotelapp.mapper.food.*;
import com.hotelapp.mapper.food.locales.*;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.AbstractMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class FoodService {

    private final FoodRepository foodRepository;
    private final FoodCartRepository foodCartRepository;
    private final FoodCreateEditMapper foodCreateEditMapper;
    private final FoodReadMapper foodReadMapper;
    private final FoodCartCreateEditMapper foodCartCreateEditMapper;
    private final FoodCartReadMapper foodCartReadMapper;
    private final FoodCartFoodCreateEditMapper foodCartFoodCreateEditMapper;
    private final FoodCartFoodReadMapper foodCartFoodReadMapper;
    private final FoodCartFoodRepository foodCartFoodRepository;
    private final FoodNameRepository foodNameRepository;
    private final FoodDescriptionRepository foodDescriptionRepository;
    private final FoodTypeRepository foodTypeRepository;
    private final FoodNameCreateEditMapper foodNameCreateEditMapper;
    private final FoodDescriptionCreateEditMapper foodDescriptionCreateEditMapper;
    private final FoodTypeCreateEditMapper foodTypeCreateEditMapper;
    private final RoomTypeRepository roomTypeRepository;
    private final FoodDescriptionReadMapper foodDescriptionReadMapper;
    private final FoodNameReadMapper foodNameReadMapper;

    private final FoodTypeReadMapper foodTypeReadMapper;
    private final UserRepository userRepository;


    @Transactional
    public FoodReadDto createFood(FoodCreateEditDto foodCreateEditDto) {
        return Optional.of(foodCreateEditDto)
                .map(foodCreateEditMapper::map)
                .map(foodRepository::save)
                .map(foodReadMapper::map)
                .orElseThrow();
    }

    @Transactional
    public FoodTypeDto createFoodType(FoodTypeDto foodTypeDto) {
        return Optional.of(foodTypeDto)
                .map(foodTypeCreateEditMapper::map)
                .map(foodTypeRepository::save)
                .map(foodTypeReadMapper::map)
                .orElseThrow();
    }

    @Transactional
    public FoodCartReadDto createFoodCart(FoodCartCreateEditDto foodCartCreateEditDto) {
        return Optional.of(foodCartCreateEditDto)
                .map(foodCartCreateEditMapper::map)
                .map(foodCartRepository::save)
                .map(foodCartReadMapper::map)
                .orElseThrow();
    }

    @Transactional
    public FoodCartFoodReadDto createFoodCartFood(FoodCartFoodCreateEditDto foodCartFoodCreateEditDto) {
        return Optional.of(foodCartFoodCreateEditDto)
                .map(foodCartFoodCreateEditMapper::map)
                .map(foodCartFoodRepository::save)
                .map(foodCartFoodReadMapper::map)
                .orElseThrow();
    }

    public List<FoodReadDto> findAllFood() {
        return foodRepository.findAll()
              .stream()
              .map(foodReadMapper::map)
              .toList();
    }

    public List<FoodReadDto> findAllFoodDesc() {
        return foodRepository.findAllDesc()
                .stream()
                .map(foodReadMapper::map)
                .toList();
    }

    public Map<Integer, FoodReadDto> findAllIdWithFood() {
        return foodRepository.findAllIdWithFood().entrySet().stream()
                .collect(Collectors.toMap(obj -> obj.getKey(), obj -> Optional.of(obj.getValue())
                        .map(foodReadMapper::map).get()));
    }

    public List<FoodTypeDto> findAllFoodTypes() {
        return foodTypeRepository.findAll()
                .stream()
                .map(foodTypeReadMapper::map)
                .toList();
    }

    public List<FoodCartReadDto> findAllFoodCart() {
        return foodCartRepository.findAll()
                .stream()
                .map(foodCartReadMapper::map)
                .toList();
    }

    public List<FoodCartFoodReadDto> findAllFoodCartFood() {
        return foodCartFoodRepository.findAll()
                .stream()
                .map(foodCartFoodReadMapper::map)
                .toList();
    }

    public Optional<FoodCartReadDto> findFoodCartById(Long foodCartId) {
        return foodCartRepository.findById(foodCartId)
                .map(foodCartReadMapper::map);
    }

    public List<FoodCartFoodReadDto> findAllFoodCartFoodByFoodCartId(Long foodCartId) {
        return foodCartFoodRepository.findAllByFoodCartId(foodCartId)
                .stream()
                .map(foodCartFoodReadMapper::map)
                .toList();
    }

    public List<FoodReadDto> findAllByFoodTypeId(String type) {
        return foodRepository.findAllByFoodTypeId(foodTypeRepository.findByEnName(type).getId())
                .stream()
                .map(foodReadMapper::map)
                .toList();
    }


    @Transactional
    public boolean cleanFoodCart(Long foodCartId) {
        foodCartFoodRepository.deleteAllByFoodCartId(foodCartId);
        foodCartFoodRepository.flush();
        return true;
    }

    @Transactional
    public boolean deleteFoodById(Integer id) {
        try {
            foodRepository.deleteById(id);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public FoodTypeDto findFoodTypeByEnName(String type) {
        return Optional.of(foodTypeRepository.findByEnName(type))
                .map(foodTypeReadMapper::map)
                .orElseThrow(() -> new IllegalArgumentException("Could not find"));
    }

    public Integer findFoodTypeIdByRuName(String type) {
        return Optional.of(foodTypeRepository.findIdByRuName(type))
                .orElseThrow(() -> new IllegalArgumentException("Could not find"));
    }

    public FoodTypeDto findFoodTypeByRuName(String type) {
        return Optional.of(foodTypeRepository.findByRuName(type))
                .map(foodTypeReadMapper::map)
                .orElseThrow(() -> new IllegalArgumentException("Could not find"));
    }

    public Integer findFoodNameIdByRuName(String foodName) {
        return Optional.of(foodNameRepository.findByRuName(foodName))
                .orElseThrow(() -> new IllegalArgumentException("Could not find"));
    }

    public Integer findFoodDescriptionIdByRuName(String foodDescription) {
        return Optional.of(foodDescriptionRepository.findByRuName(foodDescription))
                .orElseThrow(() -> new IllegalArgumentException("Could not find"));
    }


    @Transactional
    public boolean deleteFoodCartFoodById(Long id) {
        try {
            foodCartFoodRepository.deleteById(id);
            return true;
        } catch (Exception e) {
            return false;
        }
    }


    public Optional<FoodReadDto> findFoodById(Integer foodId) {
        return foodRepository.findById(foodId)
                .map(foodReadMapper::map);
    }

    @Transactional
    public FoodCartReadDto addSumByFoodCartId(Long foodCartId, Integer cost) {
        FoodCartReadDto foodCartReadDto = foodCartRepository.findById(foodCartId)
                .map(foodCartReadMapper::map)
        .orElseThrow(() -> new IllegalArgumentException("Could not find"));

        Integer newSum = foodCartReadDto.getSum() + cost;


        return foodCartRepository.updateSumById(foodCartId, newSum)
                .map(foodCartReadMapper::map)
                .orElse(null);
    }

    @Transactional
    public FoodCartReadDto minusSumByFoodCartId(Long foodCartId, Integer cost) {
        FoodCartReadDto foodCartReadDto = foodCartRepository.findById(foodCartId)
                .map(foodCartReadMapper::map)
                .orElseThrow(() -> new IllegalArgumentException("Could not find"));

        Integer newSum = foodCartReadDto.getSum() - cost;


        return foodCartRepository.updateSumById(foodCartId, newSum)
                .map(foodCartReadMapper::map)
                .orElse(null);
    }

    public FoodCartFoodReadDto findFoodCartFoodById(Long id) {
        return foodCartFoodRepository.findById(id)
                .map(foodCartFoodReadMapper::map)
                .orElse(null);
    }

    @Transactional
    public FoodCartReadDto changeCommentAndRoomInFoodCart(Long foodCartId, String comment, Integer room) {
        return foodCartRepository.updateCommentAndRoomInFoodCart(foodCartId, comment, room)
                .map(foodCartReadMapper::map)
                .orElse(null);
    }

    @Transactional
    public FoodCartReadDto resetSumByFoodCartId(Long foodCartId) {
        return foodCartRepository.updateSumById(foodCartId, 0)
                .map(foodCartReadMapper::map)
                .orElse(null);
    }

    @Transactional
    public FoodReadDto updateCost(Integer id, Integer cost) {
        return Optional.of(foodRepository.updateCost(id, cost))
                .map(foodReadMapper::map)
                .orElseThrow();
    }

    @Transactional
    public FoodDescriptionDto updateFoodDescription(Integer foodDescriptionId, String foodRuDescription, String foodEnDescription) {
        return Optional.of(foodDescriptionRepository.updateFoodDescription(foodDescriptionId, foodRuDescription, foodEnDescription))
                .map(foodDescriptionReadMapper::map)
                .orElseThrow();
    }


    @Transactional
    public FoodNameDto updateFoodName(Integer foodNameId, String foodRuName, String foodEnName) {
        return Optional.of(foodNameRepository.updateFoodName(foodNameId, foodRuName, foodEnName))
                .map(foodNameReadMapper::map)
                .orElseThrow();
    }


    @Transactional
    public FoodReadDto updateFoodTypeInFood(Integer id, Integer foodTypeId) {
        return Optional.of(foodRepository.updateFoodTypeId(id, foodTypeId))
                .map(foodReadMapper::map)
                .orElseThrow();
    }
}
