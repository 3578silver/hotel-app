package com.hotelapp.mapper.user;

import com.hotelapp.database.dto.food.FoodCartCreateEditDto;
import com.hotelapp.database.dto.services.CartCreateEditDto;
import com.hotelapp.database.dto.user.UserCreateEditDto;
import com.hotelapp.database.entity.Cart;
import com.hotelapp.database.entity.Cleaning;
import com.hotelapp.database.entity.FoodCart;
import com.hotelapp.database.entity.User;
import com.hotelapp.database.repository.LoyaltyRepository;
import com.hotelapp.mapper.Mapper;
import com.hotelapp.mapper.food.FoodCartCreateEditMapper;
import com.hotelapp.mapper.services.CartCreateEditMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.Optional;

@Component
@RequiredArgsConstructor
public class UserCreateEditMapper implements Mapper<UserCreateEditDto, User> {

    private final PasswordEncoder passwordEncoder;
    private final LoyaltyRepository loyaltyRepository;

    private final CleaningCreateEditMapper cleaningCreateEditMapper;
    private final CartCreateEditMapper cartCreateEditMapper;

    private final FoodCartCreateEditMapper foodCartCreateEditMapper;

    @Override
    public User map(UserCreateEditDto fromObject, User toObject) {
        copy(fromObject, toObject);
        return toObject;
    }

    @Override
    public User map(UserCreateEditDto userCreateEditDto) {
        User user = new User();
        copy(userCreateEditDto, user);
        return user;
    }

    private void copy(UserCreateEditDto userCreateEditDto, User user) {
        Cleaning cleaning = Optional.of(userCreateEditDto.getCleaningCreateEditDto()).map(cleaningCreateEditMapper::map).orElse(null);
        Cart cart = Optional.of(userCreateEditDto.getCartCreateEditDto()).map(cartCreateEditMapper::map).orElse(null);
        FoodCart foodCart = Optional.of(userCreateEditDto.getFoodCartCreateEditDto()).map(foodCartCreateEditMapper::map).orElse(null);

        user.setUsername(userCreateEditDto.getUsername());
        user.setFirstname(userCreateEditDto.getFirstname());
        user.setLastname(userCreateEditDto.getLastname());
        user.setBirthDate(userCreateEditDto.getBirthDate());
        user.setPhoneNumber(userCreateEditDto.getPhoneNumber());
        user.setLoyalty(loyaltyRepository.findById(userCreateEditDto.getLoyaltyId()).orElse(null));
        user.setCart(cart);
        user.setCleaning(cleaning);
        user.setFoodCart(foodCart);
        if (userCreateEditDto.getRole() != null) {
            user.setRole(userCreateEditDto.getRole());
        }

        Optional.ofNullable(userCreateEditDto.getPassword())
                .filter(StringUtils::hasText)
                .map(passwordEncoder::encode)
                .ifPresent(user::setPassword);
    }
}
