package com.hotelapp.mapper.food.locales;

import com.hotelapp.database.dto.food.locales.FoodDescriptionDto;
import com.hotelapp.database.dto.food.locales.FoodNameDto;
import com.hotelapp.database.entity.FoodDescription;
import com.hotelapp.database.entity.FoodName;
import com.hotelapp.mapper.Mapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class FoodDescriptionCreateEditMapper implements Mapper<FoodDescriptionDto, FoodDescription> {
    @Override
    public FoodDescription map(FoodDescriptionDto foodDescriptionDto) {
        FoodDescription foodDescription = new FoodDescription();
        copy(foodDescriptionDto, foodDescription);
        return foodDescription;
    }

    @Override
    public FoodDescription map(FoodDescriptionDto foodDescriptionDto, FoodDescription foodDescription) {
        copy(foodDescriptionDto, foodDescription);
        return foodDescription;
    }


    private void copy(FoodDescriptionDto foodDescriptionDto, FoodDescription foodDescription) {
        foodDescription.setRuName(foodDescriptionDto.getRuName());
        foodDescription.setEnName(foodDescriptionDto.getEnName());
    }
}