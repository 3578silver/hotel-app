package com.hotelapp.database.repository;

import com.hotelapp.database.entity.FoodCart;
import com.hotelapp.database.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import java.util.Optional;

public interface FoodCartRepository extends
        JpaRepository<FoodCart, Long>,
        QuerydslPredicateExecutor<FoodCart> {

    @Query(value = """
        UPDATE food_cart
        SET sum = :sum
        WHERE id = :id
        RETURNING *;
""", nativeQuery = true)
    Optional<FoodCart> updateSumById(Long id, Integer sum);

    @Query(value = """
        UPDATE food_cart
        SET comment = :comment,
        room = :room
        WHERE id = :foodCartId
        RETURNING *;
""", nativeQuery = true)
    Optional<FoodCart> updateCommentAndRoomInFoodCart(Long foodCartId, String comment, Integer room);


}
