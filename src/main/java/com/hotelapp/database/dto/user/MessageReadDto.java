package com.hotelapp.database.dto.user;

import lombok.Value;

import java.time.LocalDateTime;

@Value
public class MessageReadDto {

    Long id;

    String fromWhom;

    UserChatReadDto userChatReadDto;

    String text;

    String imageLink;

    LocalDateTime sentDate;

    Boolean read;
}
