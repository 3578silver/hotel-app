package com.hotelapp.database.repository;

import com.hotelapp.database.entity.FoodName;
import com.hotelapp.database.entity.ServiceName;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface ServiceNameRepository extends
        JpaRepository<ServiceName, Integer>,
        QuerydslPredicateExecutor<ServiceName> {
    @Query(
            value = """
    SELECT id FROM service_name f WHERE f.ru_name ILIKE :ruName limit 1
""", nativeQuery = true
    )
    Integer findByRuName(String ruName);

    @Query(value = """
UPDATE service_name
SET ru_name = :serviceRuName,
en_name = :serviceEnName
WHERE id = :serviceNameId
RETURNING *;
""", nativeQuery = true)
    ServiceName updateServiceName(Integer serviceNameId, String serviceRuName, String serviceEnName);
}
