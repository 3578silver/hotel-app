package com.hotelapp.database.repository;

import com.hotelapp.database.entity.Room;
import com.hotelapp.database.entity.RoomType;
import com.hotelapp.database.entity.Service;
import com.hotelapp.database.entity.User;
import jakarta.persistence.EntityManager;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

public interface RoomRepository extends
        JpaRepository<Room, Integer>,
        QuerydslPredicateExecutor<Room> {

    List<Room> findAllByRoomTypeId(Integer roomTypeId);

    default Map<Integer, Long> findCountRoomsWithRoomTypeId(EntityManager entityManager){
        Map<Integer, Long> map = new HashMap<>();
        jakarta.persistence.Query q = entityManager.createNativeQuery("""
                        SELECT room_type_id as room_type, Count(id) as count
                                    FROM room
                                    GROUP BY room_type_id
                        """);
        List<Object[]> list = q.getResultList();

        for (Object[] result : list) {
            map.put(Integer.parseInt(result[0].toString()), Long.parseLong(result[1].toString()));
        }

        return map;
    }


}