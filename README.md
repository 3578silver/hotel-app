# Universal Hotel web application

### What has already been implemented in the project:

*  Booking and viewing rooms, booking history with their payment or cancellation
*  Guest profile with loyalty program and settings (with card binding)
*  Restaurant section (ordering food and alcohol in the room, ordering festive and romantic service, shopping cart with payment)
*  Services section (various additional hotel services and offers from partners, shopping cart with payment).
*  Cleaning department (here you can choose by items what needs to be cleaned, or what happened. Next, how soon cleaning is needed. And there will be a point where you can write your own text, for extraordinary problems.
*  The page with the hotel news.
*  FAQ answers to frequently asked questions (at the bottom there will be a button "did not find the answer to the question? Contact us directly" and the button will transfer you to a support chat from the chat section).
*  Admin panel with flexible configuration (adding, editing, deleting) of any application elements, including rooms and amenities in them, users, restaurant dishes, services and chats.
*  The chats section (here we will cram all chats with the hotel staff:
    * The SPIR operator, who will translate the requests of guests by phone to other departments.
    * A restaurant employee on duty to discuss the organization of festive events.
    * The SPA employee on duty, to clarify the details.

### What will be added later:

*  Localization to English and Russian will be added

### Technical features of the project:

*  Authorization and authentication using Spring Security and JWT.
*  A large-scale database with well-structured relationships between tables and entities.
*  Developed rest controllers for external interaction and management via SWAGGER.
*  Optimized database queries using Hibernate and JPA.
*  Automatic creation of tables and their configuration in the database, thanks to Liquibase.

### Project Stack

*  Java
*  Spring Boot
*  PostgreSQL
*  Hibernate/JPA
*  Thymeleaf
*  Liquibase
*  QueryDSL

