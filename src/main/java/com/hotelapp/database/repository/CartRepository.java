package com.hotelapp.database.repository;

import com.hotelapp.database.entity.Cart;
import com.hotelapp.database.entity.FoodCart;
import com.hotelapp.mapper.Mapper;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import java.util.Optional;

public interface CartRepository extends
        JpaRepository<Cart, Long>,
        QuerydslPredicateExecutor<Cart> {


    @Query(value = """
        UPDATE cart
        SET sum = :sum
        WHERE id = :id
        RETURNING *;
""", nativeQuery = true)
    Optional<Cart> updateSumById(Long id, Integer sum);

}
