package com.hotelapp.database.dto.services.locales;

import jakarta.validation.constraints.NotEmpty;
import lombok.Value;

@Value
public class CleaningOptionCreateEditDto {
    @NotEmpty
    String ruName;

    @NotEmpty
    String enName;
}
