package com.hotelapp.database.dto.booking.locales;

import jakarta.validation.constraints.NotEmpty;
import lombok.Value;

@Value
public class RoomTypeNameDto {

    @NotEmpty
    String ruName;

    @NotEmpty
    String enName;
}