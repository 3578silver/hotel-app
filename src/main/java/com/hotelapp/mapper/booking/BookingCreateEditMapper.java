package com.hotelapp.mapper.booking;

import com.hotelapp.database.dto.booking.BookingCreateEditDto;
import com.hotelapp.database.entity.Booking;
import com.hotelapp.database.repository.RoomRepository;
import com.hotelapp.database.repository.RoomTypeRepository;
import com.hotelapp.database.repository.UserRepository;
import com.hotelapp.mapper.Mapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class BookingCreateEditMapper implements Mapper<BookingCreateEditDto, Booking> {

    private final UserRepository userRepository;
    private final RoomRepository roomRepository;
    private final RoomTypeRepository roomTypeRepository;

    @Override
    public Booking map(BookingCreateEditDto fromObject, Booking toObject) {
        copy(fromObject, toObject);
        return toObject;
    }

    @Override
    public Booking map(BookingCreateEditDto bookingCreateEditDto) {
        Booking booking = new Booking();
        copy(bookingCreateEditDto, booking);
        return booking;
    }

    private void copy(BookingCreateEditDto bookingCreateEditDto, Booking booking) {
        booking.setUser(userRepository.findById(bookingCreateEditDto.getUserId()).orElse(null));
        booking.setRoomType(roomTypeRepository.findById(bookingCreateEditDto.getRoomTypeId()).orElse(null));
        booking.setRoom(roomRepository.findById(bookingCreateEditDto.getRoomId()).orElse(null));
        booking.setBookingStatus(bookingCreateEditDto.getBookingStatus());
        booking.setTotalPrice(bookingCreateEditDto.getTotalPrice());
        booking.setCheckIn(bookingCreateEditDto.getCheckIn());
        booking.setCheckOut(bookingCreateEditDto.getCheckOut());
    }
}
