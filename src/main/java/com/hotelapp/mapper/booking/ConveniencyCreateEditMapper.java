package com.hotelapp.mapper.booking;

import com.hotelapp.database.dto.booking.ConveniencyCreateEditDto;
import com.hotelapp.database.entity.Conveniency;
import com.hotelapp.mapper.Mapper;
import com.hotelapp.mapper.booking.locales.ConveniencyDescriptionCreateEditMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@RequiredArgsConstructor
public class ConveniencyCreateEditMapper implements Mapper<ConveniencyCreateEditDto, Conveniency> {

    private final ConveniencyDescriptionCreateEditMapper conveniencyDescriptionCreateEditMapper;

    @Override
    public Conveniency map(ConveniencyCreateEditDto fromObject, Conveniency toObject) {
        copy(fromObject, toObject);
        return toObject;
    }

    @Override
    public Conveniency map(ConveniencyCreateEditDto conveniencyCreateEditDto) {
        Conveniency conveniency = new Conveniency();
        copy(conveniencyCreateEditDto, conveniency);
        return conveniency;
    }

    private void copy(ConveniencyCreateEditDto conveniencyCreateEditDto, Conveniency conveniency) {
        conveniency.setConveniencyDescription(Optional.of(conveniencyCreateEditDto.getConveniencyDescriptionDto())
                .map(conveniencyDescriptionCreateEditMapper::map).orElse(null));
        conveniency.setSvgLink(conveniencyCreateEditDto.getSvg_link());
    }
}
