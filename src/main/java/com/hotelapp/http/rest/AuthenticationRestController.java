package com.hotelapp.http.rest;

import com.hotelapp.database.dto.AuthenticationRequest;
import com.hotelapp.database.dto.AuthenticationResponse;
import com.hotelapp.database.dto.user.UserCreateEditDto;
import com.hotelapp.service.AuthenticationService;
import jakarta.servlet.http.HttpSession;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/auth")
@RequiredArgsConstructor
public class AuthenticationRestController {

    private final AuthenticationService service;

    private final AuthenticationManager authenticationManager;

    @PostMapping("/register")
    public ResponseEntity<AuthenticationResponse> register(
            UserCreateEditDto request
    ) {
        return ResponseEntity.ok(service.register(request));
    }
    @PostMapping("/authenticate")
    public ResponseEntity<AuthenticationResponse> authenticate(
            AuthenticationRequest request,
            HttpSession httpSession
    ) {
        AuthenticationResponse authenticationResponse = service.authenticate(request, authenticationManager);
        httpSession.setAttribute("Authorization",
                "Bearer " + authenticationResponse.getToken());
        ResponseEntity<AuthenticationResponse> authenticationResponseEntity = ResponseEntity.ok()
                .header("Authorization",
                        "Bearer " + authenticationResponse.getToken())
                .body(authenticationResponse);
        return authenticationResponseEntity;
    }


}
