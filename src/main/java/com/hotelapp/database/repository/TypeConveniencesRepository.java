package com.hotelapp.database.repository;

import com.hotelapp.database.entity.Conveniency;
import com.hotelapp.database.entity.RoomType;
import com.hotelapp.database.entity.TypeConveniences;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import java.util.List;

public interface TypeConveniencesRepository extends
        JpaRepository<TypeConveniences, Long>,
        QuerydslPredicateExecutor<TypeConveniences> {


    void deleteByRoomTypeIdAndConveniencyId(Integer roomTypeId, Integer convId);
}