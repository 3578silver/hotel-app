package com.hotelapp.mapper.food.locales;

import com.hotelapp.database.dto.food.locales.FoodNameDto;
import com.hotelapp.database.dto.food.locales.FoodTypeDto;
import com.hotelapp.database.entity.FoodName;
import com.hotelapp.database.entity.FoodType;
import com.hotelapp.mapper.Mapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class FoodNameCreateEditMapper implements Mapper<FoodNameDto, FoodName> {
    @Override
    public FoodName map(FoodNameDto foodNameDto) {
        FoodName foodName = new FoodName();
        copy(foodNameDto, foodName);
        return foodName;
    }

    @Override
    public FoodName map(FoodNameDto foodNameDto, FoodName foodName) {
        copy(foodNameDto, foodName);
        return foodName;
    }


    private void copy(FoodNameDto foodNameDto, FoodName foodName) {
        foodName.setRuName(foodNameDto.getRuName());
        foodName.setEnName(foodNameDto.getEnName());
    }
}