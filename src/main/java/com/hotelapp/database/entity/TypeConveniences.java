package com.hotelapp.database.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "type_conveniences")
public class TypeConveniences implements BaseEntity<Integer>{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "room_type_id", nullable = false)
    private RoomType roomType;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "conveniency_id", nullable = false)
    private Conveniency conveniency;

    public void setRoomType(RoomType roomType) {
        this.roomType = roomType;
        this.roomType.getTypeConveniences().add(this);
    }

    public void setConveniency(Conveniency conveniency) {
        this.conveniency = conveniency;
        this.conveniency.getTypeConveniences().add(this);
    }
}