package com.hotelapp.http.controller;

import com.hotelapp.database.dto.user.UpdatePasswordGroup;
import com.hotelapp.database.dto.user.UpdateUserGroup;
import com.hotelapp.database.dto.user.UserCreateEditDto;
import com.hotelapp.database.dto.user.UserReadDto;
import com.hotelapp.database.repository.ChatRepository;
import com.hotelapp.http.rest.AuthenticationRestController;
import com.hotelapp.service.ChatService;
import com.hotelapp.service.LoyaltyService;
import com.hotelapp.service.UserService;
import jakarta.servlet.http.HttpSession;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Controller
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;
    private final AuthenticationRestController authenticationController;
    private final LoyaltyService loyaltyService;
    private final ChatService chatService;


    @GetMapping("/index")
    public String index() {
        return "test/index";
    }

    @PostMapping("/changeLang")
    public String changeLang() {

        return "test/changeLang";
    }

    @GetMapping("/profile")
    public String profile(Model model, @AuthenticationPrincipal UserDetails userDetails){
        UserReadDto currentUser = userService.findByUsername(userDetails.getUsername()).get();
        model.addAttribute("countNotReadMessages", chatService.findCountMessagesInAllChats(currentUser.getId(), currentUser.getRole().getAuthority()));
        model.addAttribute("currentUser", currentUser);
        if (!model.containsAttribute("errors")) {
            model.addAttribute("errors", new HashMap<>());
        }
        return "app/_profile";
    }

    @GetMapping("/loyalty")
    public String loyalty(Model model, @AuthenticationPrincipal UserDetails userDetails){
        UserReadDto currentUser = userService.findByUsername(userDetails.getUsername()).get();
        model.addAttribute("countNotReadMessages", chatService.findCountMessagesInAllChats(currentUser.getId(), currentUser.getRole().getAuthority()));
        model.addAttribute("currentUser", currentUser);
        model.addAttribute("loyaltyLevels", loyaltyService.getAllLoyalties());
        return "app/_loyalty";
    }

    @PostMapping("/updateUser")
    public String updateUser(@ModelAttribute @Validated({UpdateUserGroup.class}) UserCreateEditDto user,
                           BindingResult bindingResult,
                           RedirectAttributes redirectAttributes,
                             @AuthenticationPrincipal UserDetails userDetails){
        UserReadDto currentUser = userService.findByUsername(userDetails.getUsername()).get();
        redirectAttributes.addFlashAttribute("user", user);
        Map<String, String> errors = new HashMap<>(){{
            put("firstname", "correct");
            put("lastname", "correct");
            put("phoneNumber", "correct");
            put("birthDate", "correct");
        }
        };
        if (bindingResult.hasErrors()) {
            bindingResult.getFieldErrors().stream().map(FieldError::getField).forEach(field -> errors.put(field, "incorrect"));
            redirectAttributes.addFlashAttribute("errors", errors);
            return "redirect:/profile";
        }
        userService.updateUserProfile(currentUser.getId(), user.getFirstname(), user.getLastname(), user.getPhoneNumber(), user.getBirthDate());


        return "redirect:/profile";
    }

    @PostMapping("/changePassword")
    public String changePassword(@ModelAttribute @Validated({UpdatePasswordGroup.class}) UserCreateEditDto user,
                             BindingResult bindingResult,
                             RedirectAttributes redirectAttributes,
                             @AuthenticationPrincipal UserDetails userDetails){
        UserReadDto currentUser = userService.findByUsername(userDetails.getUsername()).get();
        redirectAttributes.addFlashAttribute("user", user);
        Map<String, String> errors = new HashMap<>(){{
            put("password", "correct");
        }
        };
        if (bindingResult.hasErrors()) {
            bindingResult.getFieldErrors().stream().map(FieldError::getField).forEach(field -> errors.put(field, "incorrect"));
            redirectAttributes.addFlashAttribute("errors", errors);
            return "redirect:/profile";
        }
        userService.updateUserPassword(currentUser.getId(), user);


        return "redirect:/profile";
    }


}
