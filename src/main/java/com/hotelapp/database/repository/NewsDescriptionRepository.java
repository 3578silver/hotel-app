package com.hotelapp.database.repository;

import com.hotelapp.database.entity.NewsDescription;
import com.hotelapp.database.entity.ServiceDescription;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface NewsDescriptionRepository extends
        JpaRepository<NewsDescription, Long>,
        QuerydslPredicateExecutor<NewsDescription> {
    @Query(value = """
UPDATE news_description
SET ru_name = :newsRuDescription,
en_name = :newsEnDescription
WHERE id = :newsDescriptionId
RETURNING *;
""", nativeQuery = true)
    NewsDescription updateNewsDescription(Long newsDescriptionId, String newsRuDescription, String newsEnDescription);

    @Query(
            value = """
    SELECT id FROM news_description f WHERE f.ru_name ILIKE :ruName limit 1
""", nativeQuery = true
    )
    Long findByRuName(String ruName);
}
