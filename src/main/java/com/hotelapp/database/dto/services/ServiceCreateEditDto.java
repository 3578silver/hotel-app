package com.hotelapp.database.dto.services;

import com.hotelapp.database.dto.services.locales.ServiceDescriptionDto;
import com.hotelapp.database.dto.services.locales.ServiceNameDto;
import com.hotelapp.database.dto.services.locales.ServiceTypeDto;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Setter;
import lombok.Value;
import lombok.experimental.NonFinal;

@Value
public class ServiceCreateEditDto {
    @NonFinal
    @Setter
    ServiceNameDto serviceNameDto;
    @NonFinal
    @Setter
    ServiceDescriptionDto serviceDescriptionDto;
    @NonFinal
    @Setter
    ServiceTypeDto serviceTypeDto;
    @NotEmpty
    String imageLink;
    @NotNull
    Integer price;
}
