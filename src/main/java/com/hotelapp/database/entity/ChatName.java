package com.hotelapp.database.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "chat_name")
public class ChatName implements BaseEntity<Short> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Short id;

    @NotEmpty
    @Column(name = "ru_name", nullable = false)
    private String ruName;

    @NotEmpty
    @Column(name = "en_name", nullable = false)
    private String enName;
}
