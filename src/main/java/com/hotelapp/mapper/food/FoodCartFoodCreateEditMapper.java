package com.hotelapp.mapper.food;

import com.hotelapp.database.dto.food.FoodCartCreateEditDto;
import com.hotelapp.database.dto.food.FoodCartFoodCreateEditDto;
import com.hotelapp.database.dto.food.FoodCartFoodReadDto;
import com.hotelapp.database.dto.food.FoodReadDto;
import com.hotelapp.database.dto.user.LoyaltyCreateEditDto;
import com.hotelapp.database.entity.FoodCartFood;
import com.hotelapp.database.entity.Loyalty;
import com.hotelapp.database.repository.FoodCartFoodRepository;
import com.hotelapp.database.repository.FoodCartRepository;
import com.hotelapp.database.repository.FoodRepository;
import com.hotelapp.mapper.Mapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@RequiredArgsConstructor
public class FoodCartFoodCreateEditMapper implements Mapper<FoodCartFoodCreateEditDto, FoodCartFood> {
    private final FoodCartRepository foodCartRepository;

    private final FoodRepository foodRepository;

    @Override
    public FoodCartFood map(FoodCartFoodCreateEditDto foodCartFoodCreateEditDto) {
        FoodCartFood foodCartFood = new FoodCartFood();
        copy(foodCartFoodCreateEditDto, foodCartFood);
        return foodCartFood;
    }

    @Override
    public FoodCartFood map(FoodCartFoodCreateEditDto foodCartFoodCreateEditDto, FoodCartFood foodCartFood) {
        copy(foodCartFoodCreateEditDto, foodCartFood);
        return foodCartFood;
    }

    private void copy(FoodCartFoodCreateEditDto foodCartFoodCreateEditDto, FoodCartFood foodCartFood) {

        foodCartFood.setFoodCart(foodCartRepository.findById(
                foodCartFoodCreateEditDto.getFoodCartId()).orElse(null));
        foodCartFood.setFood(foodRepository.findById(
                foodCartFoodCreateEditDto.getFoodId()).orElse(null));

    }
}