package com.hotelapp.mapper.booking;

import com.hotelapp.database.dto.booking.RoomReadDto;
import com.hotelapp.database.dto.booking.RoomTypeReadDto;
import com.hotelapp.database.entity.Room;
import com.hotelapp.mapper.Mapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@RequiredArgsConstructor
public class RoomReadMapper implements Mapper<Room, RoomReadDto> {

    private final RoomTypeReadMapper roomTypeReadMapper;

    @Override
    public RoomReadDto map(Room room) {
        RoomTypeReadDto roomTypeReadDto = Optional.of(room.getRoomType())
                .map(roomTypeReadMapper::map)
                .orElse(null);

        return new RoomReadDto(
                room.getId(),
                roomTypeReadDto
        );
    }
}
