package com.hotelapp.http.rest;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.MimeType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

@Controller
@RequiredArgsConstructor
public class ImageRestController {

    @GetMapping("/image/{imageId}")
    public ResponseEntity<byte[]> getImage(@PathVariable String imageId) throws IOException {
        HttpHeaders headers = new HttpHeaders();
        String fileExtension = imageId.substring(imageId.lastIndexOf(".") + 1);
        switch (fileExtension) {
            case "png" -> headers.setContentType(MediaType.IMAGE_PNG);
            case "gif" -> headers.setContentType(MediaType.IMAGE_GIF);
            case "svg" -> headers.setContentType(MediaType.APPLICATION_XML);
            default -> headers.setContentType(MediaType.IMAGE_JPEG);
        }
        File initialFile = new File("src/main/resources/templates/images/" + imageId);
        InputStream imageStream = new FileInputStream(initialFile);
        byte[] imageBytes = imageStream.readAllBytes();
        imageStream.close();

        headers.setContentLength(imageBytes.length);
        return new ResponseEntity<byte[]>(imageBytes, headers, HttpStatus.OK);
    }

    @GetMapping("/image/{folder}/{imageId}")
    public ResponseEntity<byte[]> getImage(@PathVariable String imageId, @PathVariable String folder) throws IOException {
        HttpHeaders headers = new HttpHeaders();
        String fileExtension = imageId.substring(imageId.lastIndexOf(".") + 1);
        switch (fileExtension) {
            case "png" -> headers.setContentType(MediaType.IMAGE_PNG);
            case "gif" -> headers.setContentType(MediaType.IMAGE_GIF);
            case "svg" -> headers.setContentType(MediaType.APPLICATION_XML);
            default -> headers.setContentType(MediaType.IMAGE_JPEG);
        }
        File initialFile = new File("src/main/resources/templates/images/" + folder + "/" + imageId);
        InputStream imageStream = new FileInputStream(initialFile);
        byte[] imageBytes = imageStream.readAllBytes();
        imageStream.close();

        headers.setContentLength(imageBytes.length);
        return new ResponseEntity<byte[]>(imageBytes, headers, HttpStatus.OK);
    }

    @GetMapping("/image/{folder1}/{folder2}/{imageId}")
    public ResponseEntity<byte[]> getImage(@PathVariable String imageId, @PathVariable String folder1, @PathVariable String folder2) throws IOException {
        HttpHeaders headers = new HttpHeaders();
        String fileExtension = imageId.substring(imageId.lastIndexOf(".") + 1);
        switch (fileExtension) {
            case "png" -> headers.setContentType(MediaType.IMAGE_PNG);
            case "gif" -> headers.setContentType(MediaType.IMAGE_GIF);
            case "svg" -> headers.setContentType(MediaType.APPLICATION_XML);
            default -> headers.setContentType(MediaType.IMAGE_JPEG);
        }
        File initialFile = new File("src/main/resources/templates/images/" + folder1 + "/" + folder2 + "/" + imageId);
        InputStream imageStream = new FileInputStream(initialFile);
        byte[] imageBytes = imageStream.readAllBytes();
        imageStream.close();

        headers.setContentLength(imageBytes.length);
        return new ResponseEntity<byte[]>(imageBytes, headers, HttpStatus.OK);
    }
}
