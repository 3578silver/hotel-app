package com.hotelapp.mapper.services;

import com.hotelapp.database.dto.services.ServiceCartCreateEditDto;
import com.hotelapp.database.entity.ServiceCart;
import com.hotelapp.database.repository.CartRepository;
import com.hotelapp.database.repository.RoomRepository;
import com.hotelapp.database.repository.ServiceRepository;
import com.hotelapp.mapper.Mapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class ServiceCartCreateEditMapper implements Mapper<ServiceCartCreateEditDto, ServiceCart> {

    private final ServiceRepository serviceRepository;

    private final CartRepository cartRepository;

    @Override
    public ServiceCart map(ServiceCartCreateEditDto serviceCartCreateEditDto) {
        ServiceCart serviceCart = new ServiceCart();
        copy(serviceCartCreateEditDto, serviceCart);
        return serviceCart;
    }

    @Override
    public ServiceCart map(ServiceCartCreateEditDto serviceCartCreateEditDto, ServiceCart serviceCart) {
        copy(serviceCartCreateEditDto, serviceCart);
        return serviceCart;
    }

    private void copy(ServiceCartCreateEditDto serviceCartCreateEditDto, ServiceCart service) {
        service.setService(serviceRepository.findById(
                serviceCartCreateEditDto.getServiceId()).orElse(null));
        service.setCart(cartRepository.findById(
                serviceCartCreateEditDto.getCartId()).orElse(null));
    }
}
