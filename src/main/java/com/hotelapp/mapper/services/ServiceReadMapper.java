package com.hotelapp.mapper.services;

import com.hotelapp.database.dto.services.ServiceReadDto;
import com.hotelapp.database.dto.services.locales.ServiceDescriptionDto;
import com.hotelapp.database.dto.services.locales.ServiceNameDto;
import com.hotelapp.database.dto.services.locales.ServiceTypeDto;
import com.hotelapp.database.entity.Service;
import com.hotelapp.mapper.Mapper;
import com.hotelapp.mapper.services.locales.ServiceDescriptionReadMapper;
import com.hotelapp.mapper.services.locales.ServiceNameReadMapper;
import com.hotelapp.mapper.services.locales.ServiceTypeReadMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@RequiredArgsConstructor
public class ServiceReadMapper implements Mapper<Service, ServiceReadDto> {
    private final ServiceNameReadMapper serviceNameReadMapper;
    private final ServiceTypeReadMapper serviceTypeReadMapper;
    private final ServiceDescriptionReadMapper serviceDescriptionReadMapper;

    @Override
    public ServiceReadDto map(Service service) {
        ServiceNameDto serviceNameDto = Optional.of(service.getServiceName())
                .map(serviceNameReadMapper::map)
                .orElse(null);
        ServiceTypeDto serviceTypeDto = Optional.of(service.getServiceType())
                .map(serviceTypeReadMapper::map)
                .orElse(null);
        ServiceDescriptionDto serviceDescriptionDto = Optional.of(service.getServiceDescription())
                .map(serviceDescriptionReadMapper::map)
                .orElse(null);

        return new ServiceReadDto(
                service.getId(),
                serviceNameDto,
                serviceDescriptionDto,
                serviceTypeDto,
                service.getImageLink(),
                service.getPrice()
        );
    }
}
