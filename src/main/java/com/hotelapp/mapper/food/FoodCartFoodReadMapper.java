package com.hotelapp.mapper.food;

import com.hotelapp.database.dto.food.FoodCartFoodReadDto;
import com.hotelapp.database.dto.user.LoyaltyReadDto;
import com.hotelapp.database.entity.FoodCartFood;
import com.hotelapp.database.entity.Loyalty;
import com.hotelapp.mapper.Mapper;
import com.hotelapp.mapper.user.LoyaltyReadMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@RequiredArgsConstructor
public class FoodCartFoodReadMapper implements Mapper<FoodCartFood, FoodCartFoodReadDto> {
    private final FoodReadMapper foodReadMapper;
    private final FoodCartReadMapper foodCartReadMapper;

    @Override
    public FoodCartFoodReadDto map(FoodCartFood foodCartFood) {
        return new FoodCartFoodReadDto(
                foodCartFood.getId(),
                Optional.of(foodCartFood.getFoodCart()).map(foodCartReadMapper::map).orElse(null),
                Optional.of(foodCartFood.getFood()).map(foodReadMapper::map).orElse(null)
        );
    }
}