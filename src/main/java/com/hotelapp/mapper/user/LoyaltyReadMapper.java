package com.hotelapp.mapper.user;

import com.hotelapp.database.dto.user.LoyaltyReadDto;
import com.hotelapp.database.entity.Loyalty;
import com.hotelapp.mapper.Mapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class LoyaltyReadMapper implements Mapper<Loyalty, LoyaltyReadDto> {
    @Override
    public LoyaltyReadDto map(Loyalty loyalty) {
        return new LoyaltyReadDto(
                loyalty.getId(),
                loyalty.getName(),
                loyalty.getMinSpent(),
                loyalty.getSale()
        );
    }
}
