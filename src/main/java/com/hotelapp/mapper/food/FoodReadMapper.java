package com.hotelapp.mapper.food;

import com.hotelapp.database.dto.food.FoodReadDto;
import com.hotelapp.database.dto.user.LoyaltyReadDto;
import com.hotelapp.database.entity.Food;
import com.hotelapp.database.entity.Loyalty;
import com.hotelapp.mapper.Mapper;
import com.hotelapp.mapper.food.locales.FoodDescriptionReadMapper;
import com.hotelapp.mapper.food.locales.FoodNameReadMapper;
import com.hotelapp.mapper.food.locales.FoodTypeReadMapper;
import com.hotelapp.mapper.user.LoyaltyReadMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Optional;


@Component
@RequiredArgsConstructor
public class FoodReadMapper implements Mapper<Food, FoodReadDto> {

    private final FoodNameReadMapper foodNameReadMapper;
    private final FoodDescriptionReadMapper foodDescriptionReadMapper;
    private final FoodTypeReadMapper foodTypeReadMapper;

    @Override
    public FoodReadDto map(Food food) {
        return new FoodReadDto(
                food.getId(),
                Optional.of(food.getName()).map(foodNameReadMapper::map).orElse(null),
                food.getCost(),
                Optional.of(food.getDescription()).map(foodDescriptionReadMapper::map).orElse(null),
                Optional.of(food.getType()).map(foodTypeReadMapper::map).orElse(null),
                food.getImageLink()
        );
    }
}