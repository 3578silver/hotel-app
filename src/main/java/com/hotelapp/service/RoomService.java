package com.hotelapp.service;

import com.hotelapp.database.dto.booking.*;
import com.hotelapp.database.dto.booking.locales.ConveniencyDescriptionDto;
import com.hotelapp.database.dto.booking.locales.RoomTypeDescriptionDto;
import com.hotelapp.database.dto.booking.locales.RoomTypeNameDto;
import com.hotelapp.database.entity.TypeConveniences;
import com.hotelapp.database.entity.User;
import com.hotelapp.database.repository.*;
import com.hotelapp.mapper.booking.*;
import com.hotelapp.mapper.booking.locales.*;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class RoomService {
    private final RoomRepository roomRepository;
    private final RoomReadMapper roomReadMapper;
    private final RoomCreateEditMapper roomCreateEditMapper;

    private final RoomTypeRepository roomTypeRepository;
    private final RoomTypeReadMapper roomTypeReadMapper;
    private final RoomTypeCreateEditMapper roomTypeCreateEditMapper;

    private final RoomTypeNameRepository roomTypeNameRepository;
    private final RoomTypeNameReadMapper roomTypeNameReadMapper;
    private final RoomTypeNameCreateEditMapper roomTypeNameCreateEditMapper;

    private final RoomTypeDescriptionRepository roomTypeDescriptionRepository;
    private final RoomTypeDescriptionReadMapper roomTypeDescriptionReadMapper;
    private final RoomTypeDescriptionCreateEditMapper roomTypeDescriptionCreateEditMapper;

    private final BookingRepository bookingRepository;
    private final BookingReadMapper bookingReadMapper;
    private final BookingCreateEditMapper bookingCreateEditMapper;

    private final ConveniencyRepository conveniencyRepository;
    private final ConveniencyReadMapper conveniencyReadMapper;
    private final ConveniencyCreateEditMapper conveniencyCreateEditMapper;

    private final ConveniencyDescriptionRepository conveniencyDescriptionRepository;
    private final ConveniencyDescriptionReadMapper conveniencyDescriptionReadMapper;
    private final ConveniencyDescriptionCreateEditMapper conveniencyDescriptionCreateEditMapper;

    private final TypeConveniencesRepository typeConveniencesRepository;
    private final TypeConveniencesReadMapper typeConveniencesReadMapper;
    private final TypeConveniencesCreateEditMapper typeConveniencesCreateEditMapper;
    private final UserRepository userRepository;

    @Transactional
    public BookingReadDto createBooking(BookingCreateEditDto bookingCreateEditDto, UserDetails userDetails) {

        User user = userRepository.findByUsername(userDetails.getUsername()).orElseThrow(() -> new UsernameNotFoundException("User not found"));

        //Если нет нужного номера, то возвращаем ошибку

        bookingCreateEditDto.setRoomId(getAvailableRoomIdByRoomType(
                bookingCreateEditDto.getRoomTypeId(),
                bookingCreateEditDto.getCheckIn(),
                bookingCreateEditDto.getCheckOut()));

        //на верхнем уровне ловит исключение, если нет нужного номера. Пишет, что номера этого типа забронированы.

        Integer countNights = (int) ChronoUnit.DAYS.between(bookingCreateEditDto.getCheckIn(), bookingCreateEditDto.getCheckOut());
        Integer totalPrice = getTotalPrice(bookingCreateEditDto.getRoomTypeId()) * countNights;
        if (user.getLoyalty().getSale() != 0) {
            totalPrice = totalPrice - (totalPrice / 100 * user.getLoyalty().getSale());
        }
        bookingCreateEditDto.setTotalPrice(
                totalPrice
        );
        bookingCreateEditDto.setUserId(user.getId());
        return Optional.of(bookingCreateEditDto)
                .map(bookingCreateEditMapper::map)
                .map(bookingRepository::save)
                .map(bookingReadMapper::map)
                .orElseThrow();
    }

    public List<BookingReadDto> findAllBookingByUserId(UserDetails userDetails) {
        User user = userRepository.findByUsername(userDetails.getUsername()).orElseThrow(() -> new UsernameNotFoundException("User not found"));
        return bookingRepository.findTop10ByUserIdOrderByIdDesc(user.getId())
              .stream()
              .map(bookingReadMapper::map)
              .toList();

    }

    public Integer getTotalPrice(Integer roomTypeId) {
        return roomTypeRepository.findById(roomTypeId).orElseThrow().getPrice();
    }

    public Integer getAvailableRoomIdByRoomType(Integer roomTypeId, LocalDate checkInDate, LocalDate checkOutDate) {
        return Optional.ofNullable(bookingRepository.findAvailableRoomIdByRoomType(roomTypeId, checkInDate, checkOutDate))
                .orElseThrow(() -> new IllegalArgumentException ("Room not found"));
    }

    public Integer getCountAvailableRoomByRoomType(Integer roomTypeId, LocalDate checkInDate, LocalDate checkOutDate) {
        return bookingRepository.findAllAvailableRoomsByRoomType(roomTypeId, checkInDate, checkOutDate);
    }


    public List<RoomReadDto> findAllRoomsByType(Integer type) {
        return roomRepository.findAllByRoomTypeId(type)
              .stream()
              .map(roomReadMapper::map)
              .toList();
    }




    public List<BookingReadDto> findAllBooking() {
        return bookingRepository.findAll()
             .stream()
             .map(bookingReadMapper::map)
             .toList();
    }

    @Transactional
    public RoomReadDto createRoom(RoomCreateEditDto roomCreateEditDto) {
        return Optional.of(roomCreateEditDto)
              .map(roomCreateEditMapper::map)
              .map(roomRepository::save)
              .map(roomReadMapper::map)
              .orElseThrow();
    }

    @Transactional
    public List<RoomReadDto> createRoomByCountAndType(Integer roomTypeId, Integer count) {
        List<RoomReadDto> rooms = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            rooms.add(
                    Optional.of(new RoomCreateEditDto(roomTypeId))
                    .map(roomCreateEditMapper::map)
                    .map(roomRepository::save)
                    .map(roomReadMapper::map)
                    .orElseThrow());
        }
        return rooms;
    }

    public List<RoomReadDto> findAllRoom() {
        return roomRepository.findAll()
             .stream()
             .map(roomReadMapper::map)
             .toList();
    }

    @Transactional
    public ConveniencyReadDto createConveniency(ConveniencyCreateEditDto conveniencyCreateEditDto) {
        return Optional.of(conveniencyCreateEditDto)
              .map(conveniencyCreateEditMapper::map)
              .map(conveniencyRepository::save)
              .map(conveniencyReadMapper::map)
              .orElseThrow();
    }

    public List<ConveniencyReadDto> findAllConveniency() {
        return conveniencyRepository.findAll()
             .stream()
             .map(conveniencyReadMapper::map)
             .toList();
    }

//    addConToRoomType

    @Transactional
    public RoomTypeReadDto createRoomType(RoomTypeCreateEditDto roomTypeCreateEditDto) {
        return Optional.of(roomTypeCreateEditDto)
                .map(roomTypeCreateEditMapper::map)
                .map(roomTypeRepository::save)
                .map(roomTypeReadMapper::map)
                .orElseThrow();
    }

    public List<RoomTypeReadDto> findAllRoomType() {
        return roomTypeRepository.findAll()
              .stream()
              .map(roomTypeReadMapper::map)
              .toList();
    }


    public List<RoomTypeReadDto> findAllRoomTypeOrderedByPrice() {
        return roomTypeRepository.findAllOrderedByPrice()
                .stream()
                .map(roomTypeReadMapper::map)
                .toList();
    }

    @Transactional
    public TypeConveniencesReadDto addConveniencyToRoomType(TypeConveniencesCreateEditDto typeConveniencesCreateEditDto) {
        return Optional.of(typeConveniencesCreateEditDto)
                .map(typeConveniencesCreateEditMapper::map)
                .map(typeConveniencesRepository::save)
                .map(typeConveniencesReadMapper::map)
                .orElseThrow();
    }


    @Transactional
    public void deleteAllRooms() {
        roomRepository.deleteAll();
    }

    @Transactional
    public void deleteAllRoomTypes() {
        roomTypeRepository.deleteAll();
    }

    public BookingReadDto findBookingById(Long bookingId) {
        return bookingRepository.findById(bookingId).map(bookingReadMapper::map).orElseThrow();
    }

    public RoomReadDto findRoomById(Integer roomId) {
        return roomRepository.findById(roomId).map(roomReadMapper::map).orElse(null);
    }

    public RoomTypeReadDto findRoomTypeById(Integer roomTypeId) {
        return roomTypeRepository.findById(roomTypeId).map(roomTypeReadMapper::map).orElseThrow();
    }

    public List<ConveniencyReadDto> findAllConveniencesByRoomType(Integer roomTypeId) {
        return conveniencyRepository.findAllByRoomTypeId(roomTypeId)
             .stream()
             .map(conveniencyReadMapper::map)
             .toList();
    }

    @Transactional
    public TypeConveniencesReadDto addConveniencyToRoomTypeWithoutConveniency(String conveniencyRuName, String conveniencyEnName, Integer roomTypeId, String svgLink) {
        try {
            return Optional.of(conveniencyDescriptionRepository.findByRuNameAndEnName(conveniencyRuName, conveniencyEnName))
                    .map(convDesc -> conveniencyRepository.findByConveniencyDescription_Id(convDesc.getId()))
                    .map(conveniency -> typeConveniencesRepository.saveAndFlush(new TypeConveniences(0, roomTypeRepository.findRoomTypeById(roomTypeId), conveniency)))
                    .map(typeConveniencesReadMapper::map)
                    .orElseThrow(() -> new IllegalArgumentException("Conveniency not found"));
        } catch (IllegalArgumentException e) {
            return Optional.of(new ConveniencyCreateEditDto(svgLink,
                    new ConveniencyDescriptionDto(conveniencyRuName, conveniencyEnName)))
                    .map(conveniencyCreateEditMapper::map)
                    .map(conveniencyRepository::saveAndFlush)
                    .map(convenience ->
                            typeConveniencesRepository.saveAndFlush(new TypeConveniences(0, roomTypeRepository.findRoomTypeById(roomTypeId), convenience)))
                    .map(typeConveniencesReadMapper::map).orElseThrow();
        }
    }


    public List<ConveniencyReadDto> findAllConvienciesByType(RoomTypeReadDto roomTypeReadDto){
        return conveniencyRepository.findAllConvienciesByType(roomTypeReadDto.getId())
                .stream().map(conveniencyReadMapper::map)
                .toList();
    };

    @Transactional
    public BookingReadDto cancelBooking(Long bookingId) {
        return bookingRepository.cancelBooking(bookingId).map(bookingReadMapper::map).orElseThrow();
    }

    @Transactional
    public BookingReadDto payBooking(Long bookingId) {
        return bookingRepository.payBooking(bookingId).map(bookingReadMapper::map).orElseThrow();
    }

    public Integer findRoomTypeByRuName(String ruName) {
        return Optional.of(roomTypeRepository.findRoomTypeByRuName(ruName)).orElseThrow();
    }

    @Transactional
    public void deleteRoomTypeById(Integer id) {
        roomTypeRepository.deleteById(id);
    }

    @Transactional
    public void deleteConveniency(Integer id) {
        conveniencyRepository.deleteById(id);
    }

    public ConveniencyReadDto findConveniencyById(Integer id) {
        return conveniencyRepository.findById(id)
                .map(conveniencyReadMapper::map)
                .orElseThrow();
    }

    @Transactional
    public void deleteTypeConvenience(Integer roomTypeId, Integer convId) {
        typeConveniencesRepository.deleteByRoomTypeIdAndConveniencyId(roomTypeId, convId);
    }

    @Transactional
    public RoomTypeReadDto updatePriceInRoomType(Integer id, Integer price) {
        return Optional.of(roomTypeRepository.updatePrice(id, price))
                .map(roomTypeReadMapper::map)
                .orElseThrow();
    }

    public Long findRoomTypeDescriptionByEnName(String enName) {
        return roomTypeDescriptionRepository.findIdByEnName(enName);
    }

    @Transactional
    public RoomTypeDescriptionDto updateRoomTypeDescription(Long roomTypeDescriptionId, String roomTypeRuDescription, String roomTypeEnDescription) {
        return Optional.of(roomTypeDescriptionRepository.updateRoomTypeDescription(roomTypeDescriptionId, roomTypeRuDescription, roomTypeEnDescription))
                .map(roomTypeDescriptionReadMapper::map)
                .orElseThrow();
    }

    public Integer findRoomTypeNameByEnName(String enName) {
        return roomTypeNameRepository.findIdByEnName(enName);
    }


    @Transactional
    public RoomTypeNameDto updateRoomTypeName(Integer roomTypeNameId, String roomTypeRuName, String roomTypeEnName) {
        return Optional.of(roomTypeNameRepository.updateRoomTypeName(roomTypeNameId, roomTypeRuName, roomTypeEnName))
                .map(roomTypeNameReadMapper::map)
                .orElseThrow();
    }
}
