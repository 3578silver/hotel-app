package com.hotelapp.mapper.user;

import com.hotelapp.database.dto.user.ChatCreateEditDto;
import com.hotelapp.database.dto.user.UserChatCreateEditDto;
import com.hotelapp.database.entity.Chat;
import com.hotelapp.database.entity.ChatName;
import com.hotelapp.database.entity.UserChat;
import com.hotelapp.database.repository.ChatRepository;
import com.hotelapp.database.repository.UserRepository;
import com.hotelapp.mapper.Mapper;
import com.hotelapp.mapper.user.locales.ChatNameCreateEditMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@RequiredArgsConstructor
public class UserChatCreateEditMapper implements Mapper<UserChatCreateEditDto, UserChat> {

    private final ChatRepository chatRepository;
    private final UserRepository userRepository;

    @Override
    public UserChat map(UserChatCreateEditDto userChatCreateEditDto, UserChat userChat) {
        copy(userChatCreateEditDto, userChat);
        return userChat;
    }

    @Override
    public UserChat map(UserChatCreateEditDto userChatCreateEditDto) {
        UserChat userChat = new UserChat();
        copy(userChatCreateEditDto, userChat);
        return userChat;
    }

    private void copy(UserChatCreateEditDto userChatCreateEditDto, UserChat userChat) {
        userChat.setChat(chatRepository.findById(userChatCreateEditDto.getChatId()).orElse(null));
        userChat.setUser(userRepository.findById(userChatCreateEditDto.getUserId()).orElse(null));
    }
}