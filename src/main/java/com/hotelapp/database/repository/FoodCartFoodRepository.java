package com.hotelapp.database.repository;

import com.hotelapp.database.entity.FoodCartFood;
import com.hotelapp.database.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import java.util.List;
import java.util.Optional;

public interface FoodCartFoodRepository extends
        JpaRepository<FoodCartFood, Long>,
        QuerydslPredicateExecutor<FoodCartFood> {

    void deleteAllByFoodCartId(Long foodCartId);

    List<FoodCartFood> findAllByFoodCartId(Long foodCartId);

}
