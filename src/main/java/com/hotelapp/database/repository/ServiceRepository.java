package com.hotelapp.database.repository;

import com.hotelapp.database.entity.Food;
import com.hotelapp.database.entity.Service;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public interface ServiceRepository extends
        JpaRepository<Service, Integer>,
        QuerydslPredicateExecutor<Service> {

    @Query(
            value = """
    SELECT * FROM service f WHERE f.service_type_id = :serviceTypeId
""", nativeQuery = true
    )
    List<Service> findAllByServiceTypeId(Integer serviceTypeId);


    default Map<Integer, Service> findAllIdWithService() {
        return findAll().stream().collect(Collectors.toMap(Service::getId, o -> o));
    }

    @Query(value = """
UPDATE service
SET price = :price
WHERE id = :id
RETURNING *;
""", nativeQuery = true)
    Service updatePrice(Integer id, Integer price);

    @Query(value = """
UPDATE service
SET service_type_id = :serviceTypeId
WHERE id = :id
RETURNING *;
""", nativeQuery = true)
    Service updateServiceTypeId(Integer id, Integer serviceTypeId);

    @Query(value = """
SELECT *
FROM service
ORDER BY service.id desc
""", nativeQuery = true)
    List<Service> findAllDesc();
}
