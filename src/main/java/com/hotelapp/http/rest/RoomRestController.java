package com.hotelapp.http.rest;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.hotelapp.database.dto.booking.*;
import com.hotelapp.database.entity.Conveniency;
import com.hotelapp.service.RoomService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("/api/v1/rooms")
@RequiredArgsConstructor
public class RoomRestController {
    private final RoomService roomService;

    @GetMapping(path = "/findAllRoom", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<RoomReadDto> findAllRoom() {
        return roomService.findAllRoom();
    }

    @GetMapping(path = "/findAllRoomType", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<RoomTypeReadDto> findAllRoomType() {
        return roomService.findAllRoomType();
    }
    @GetMapping(path = "/findAllConveniency", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ConveniencyReadDto> findAllConveniency() {
        return roomService.findAllConveniency();
    }
    @GetMapping(path = "/findAllBooking", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<BookingReadDto> findAllBooking() {
        return roomService.findAllBooking();
    }

    @GetMapping(path = "/findAllConvienciesByRoomType", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ConveniencyReadDto> findAllConveniencesByRoomType(Integer roomTypeId) {
        return roomService.findAllConveniencesByRoomType(roomTypeId);
    }

    public TypeConveniencesReadDto addConveniencyToRoomType(Integer conveniencyId, Integer roomTypeId) {
        return roomService.addConveniencyToRoomType(new TypeConveniencesCreateEditDto(roomTypeId, conveniencyId));
    }

    //потом нужно будет сделать обычный контроллер, в котором можно загрузить фотки и они закинутся в папку с id типа комнаты.
    public TypeConveniencesReadDto createOrAddConveniencyToRoomType(String conveniencyRuName, String conveniencyEnName, Integer roomTypeId, String svgLink) {
        return roomService.addConveniencyToRoomTypeWithoutConveniency(conveniencyRuName, conveniencyEnName, roomTypeId, svgLink);

    }

    @PostMapping(path = "/createRoom", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public RoomReadDto createRoom(@Validated @RequestBody RoomCreateEditDto roomCreateEditDto) {
        return roomService.createRoom(roomCreateEditDto);
    }

    @PostMapping(path = "/createRoomByCountAndCount")
    @ResponseStatus(HttpStatus.CREATED)
    public List<RoomReadDto> createRoomByCountAndType(Integer roomTypeId,
                                         Integer count) {
        return roomService.createRoomByCountAndType(roomTypeId, count);
    }




    @PostMapping(path = "/createRoomType", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public RoomTypeReadDto createRoomType(@Validated @RequestBody RoomTypeCreateEditDto roomTypeCreateEditDto) {
        return roomService.createRoomType(roomTypeCreateEditDto);
    }

    @PostMapping(path = "/createConveniency", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public ConveniencyReadDto createConveniency(@Validated @RequestBody ConveniencyCreateEditDto conveniencyCreateEditDto) {
        return roomService.createConveniency(conveniencyCreateEditDto);
    }

    @GetMapping(path = "/findAllRoomsByType", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<RoomReadDto> findAllRoomsByType(Integer roomTypeId) {
        return roomService.findAllRoomsByType(roomTypeId);
    }


    @PostMapping(path = "/createBooking", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public BookingReadDto createBooking(@Validated @RequestBody BookingCreateEditDto bookingCreateEditDto,
                                        @AuthenticationPrincipal UserDetails userDetails) {
        return roomService.createBooking(bookingCreateEditDto, userDetails);
    }

    @DeleteMapping("/deleteAllRoomTypes")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public boolean deleteAllRoomTypes(){
        try {
            roomService.deleteAllRoomTypes();
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    @DeleteMapping("/deleteAllRoom")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public boolean deleteAllRoom(){
        try {
            roomService.deleteAllRooms();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @GetMapping(path = "/findRoomById")
    public RoomReadDto findRoomById(Integer roomId) {
        return roomService.findRoomById(roomId);
    }

    @GetMapping(path = "/getCountAvailableRoomByRoomType")
    public Integer getCountAvailableRoomByRoomType(Integer roomTypeId, LocalDate checkInDate, LocalDate checkOutDate) {
        return roomService.getCountAvailableRoomByRoomType(roomTypeId,
                checkInDate, checkOutDate);
    }
    @GetMapping(path = "/getAvailableRoomIdByRoomType")
    public Integer getAvailableRoomIdByRoomType(Integer roomTypeId, LocalDate checkInDate, LocalDate checkOutDate){
        return roomService.getAvailableRoomIdByRoomType(roomTypeId,
                checkInDate,
                checkOutDate);
    }


}
