package com.hotelapp.database.dto.validate;

import com.hotelapp.database.dto.AvailabilityDto;
import com.hotelapp.database.dto.validate.annotation.BookInPast;
import com.hotelapp.database.dto.validate.annotation.CheckInBeforeCheckOut;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

import java.time.LocalDate;


public class BookInPastValidator implements ConstraintValidator<BookInPast, AvailabilityDto> {

    @Override
    public void initialize(BookInPast bookInPast) {
    }

    @Override
    public boolean isValid(AvailabilityDto value, ConstraintValidatorContext context) {
        LocalDate checkIn = value.getCheckIn();

        if (checkIn.isBefore(LocalDate.now())) {
            // Дата заезда раньше текущей даты, валидация не пройдена

            context.buildConstraintViolationWithTemplate("{checkInBeforeCheckOut.message}")
                    .addPropertyNode("checkIn")
                    .addConstraintViolation()
                    .disableDefaultConstraintViolation();
            return false;
        }

        return true;
    }
}
