package com.hotelapp.database.dto.food;

import com.hotelapp.database.dto.food.locales.FoodDescriptionDto;
import com.hotelapp.database.dto.food.locales.FoodNameDto;
import com.hotelapp.database.dto.food.locales.FoodTypeDto;
import lombok.Value;

@Value
public class FoodReadDto {
    Integer id;
    FoodNameDto foodName;
    Integer cost;
    FoodDescriptionDto foodDescription;
    FoodTypeDto foodType;
    String imageLink;

}
