package com.hotelapp.http.rest;

import com.hotelapp.database.dto.food.FoodCreateEditDto;
import com.hotelapp.database.dto.food.FoodReadDto;
import com.hotelapp.database.dto.services.CleaningCreateEditDto;
import com.hotelapp.database.dto.services.CleaningReadDto;
import com.hotelapp.service.CleaningService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/cleaning")
@RequiredArgsConstructor
public class CleaningRestController {

    private final CleaningService cleaningService;

    @PostMapping(path = "/createCleaning", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<CleaningReadDto> createCleaning(@Validated @RequestBody CleaningCreateEditDto cleaningCreateEditDto){
        return new ResponseEntity<>(cleaningService.createCleaning(cleaningCreateEditDto), HttpStatus.CREATED);
    }

}
