package com.hotelapp.database.dto.user;

import com.hotelapp.database.dto.user.locales.ChatNameDto;
import lombok.Value;

import java.util.List;

@Value
public class ChatReadDto {
    Short id;
    ChatNameDto name;
//    List<UserChatReadDto> userChats;
}
