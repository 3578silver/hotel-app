package com.hotelapp.database.repository;

import com.hotelapp.database.entity.CleaningOption;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface CleaningOptionRepository extends
        JpaRepository<CleaningOption, Integer>,
        QuerydslPredicateExecutor<CleaningOption> {



}
