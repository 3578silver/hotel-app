package com.hotelapp.database.dto.validate.annotation;

import com.hotelapp.database.dto.validate.BookInPastValidator;
import com.hotelapp.database.dto.validate.CheckInBeforeCheckOutValidator;
import jakarta.validation.Constraint;
import jakarta.validation.Payload;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = BookInPastValidator.class)
public @interface BookInPast {

    String message() default "Дата заезда не должна быть раньше текущей даты";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}