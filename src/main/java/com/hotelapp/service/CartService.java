package com.hotelapp.service;

import com.hotelapp.database.dto.services.CartCreateEditDto;
import com.hotelapp.database.dto.services.CartReadDto;
import com.hotelapp.database.dto.user.ChatCreateEditDto;
import com.hotelapp.database.dto.user.ChatReadDto;
import com.hotelapp.database.dto.user.locales.ChatNameDto;
import com.hotelapp.database.repository.CartRepository;
import com.hotelapp.mapper.services.CartCreateEditMapper;
import com.hotelapp.mapper.services.CartReadMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class CartService {

    private final CartCreateEditMapper cartCreateEditMapper;
    private final CartRepository cartRepository;
    private final CartReadMapper cartReadMapper;

    @Transactional
    public CartReadDto createCart(CartCreateEditDto cartCreateEditDto) {
        return Optional.of(cartCreateEditDto)
                .map(cartCreateEditMapper::map)
                .map(cartRepository::save)
                .map(cartReadMapper::map)
                .orElseThrow();
    }
}
