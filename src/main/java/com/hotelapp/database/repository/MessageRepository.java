package com.hotelapp.database.repository;

import com.hotelapp.database.entity.Message;
import com.hotelapp.database.entity.NewsName;
import com.hotelapp.database.entity.RoomType;
import com.hotelapp.database.entity.UserChat;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public interface MessageRepository extends
        JpaRepository<Message, Long>,
        QuerydslPredicateExecutor<Message> {


    List<Message> findAllMessagesByUserChatIdOrderById(Long userChatId);

    @Query(value = """
UPDATE message
SET image_link = :imageLink
WHERE id = :id
RETURNING *;
""", nativeQuery = true)
    Message updateImageLinkById(Long id, String imageLink);

    default Map<Short, Long> findCountMessagesInChats(Long userId, String role) {
        return findAll().stream()
                .filter(message -> !message.getRead())
                .filter(message -> message.getUserChat().getUser().getId() == userId)
                .filter(message -> !message.getFromWhom().equals(role))
                .collect(Collectors.groupingBy(o -> o.getUserChat().getChat().getId(), Collectors.counting()));
    }

    default Map<Short, Long> findCountMessagesInChatsForAdmin() {
        return findAll().stream()
                .filter(message -> !message.getRead())
                .filter(message -> message.getFromWhom().equals("USER"))
                .collect(Collectors.groupingBy(o -> o.getUserChat().getChat().getId(), Collectors.counting()));
    }

    @Query(value = """
SELECT count(message)
FROM message
LEFT JOIN users_chat uc on uc.id = message.userchat_id
WHERE uc.user_id = :userId
AND message.from_whom != :role
AND message.read = 'N'
""", nativeQuery = true)
    Long findCountMessagesInAllChats(Long userId, String role);


    default Map<Long, Long> findCountMessagesInUserChats(Short chatId) {
        return findAll().stream()
                .filter(message -> !message.getRead())
                .filter(message -> !message.getFromWhom().equals("ADMIN"))
                .filter(message -> message.getUserChat().getChat().getId() == chatId)
                .collect(Collectors.groupingBy(o -> o.getUserChat().getId(), Collectors.counting()));
    }

    @Query(value = """
SELECT count(message)
FROM message
LEFT JOIN users_chat uc on uc.id = message.userchat_id
WHERE message.from_whom != 'ADMIN'
AND message.read = 'N'
""", nativeQuery = true)
    Long findCountMessagesInAllChatsForAdmin();


    @Query(value = """
UPDATE message
SET read = 'Y'
WHERE from_whom != :role
AND read = 'N'
AND userchat_id = :userChatId
RETURNING *;
""", nativeQuery = true)
    List<Message> makeReadableMessages(Long userChatId, String role);

}