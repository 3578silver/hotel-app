package com.hotelapp.mapper.user;

import com.hotelapp.database.dto.user.LoyaltyCreateEditDto;
import com.hotelapp.database.dto.user.UserCreateEditDto;
import com.hotelapp.database.entity.Loyalty;
import com.hotelapp.database.entity.User;
import com.hotelapp.mapper.Mapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.Optional;

@Component
@RequiredArgsConstructor
public class LoyaltyCreateEditMapper implements Mapper<LoyaltyCreateEditDto, Loyalty> {
    @Override
    public Loyalty map(LoyaltyCreateEditDto loyaltyCreateEditDto) {
        Loyalty loyalty = new Loyalty();
        copy(loyaltyCreateEditDto, loyalty);
        return loyalty;
    }

    @Override
    public Loyalty map(LoyaltyCreateEditDto loyaltyCreateEditDto, Loyalty loyalty) {
        copy(loyaltyCreateEditDto, loyalty);
        return loyalty;
    }


    private void copy(LoyaltyCreateEditDto loyaltyCreateEditDto, Loyalty loyalty) {
        loyalty.setName(loyaltyCreateEditDto.getName());
        loyalty.setSale(loyaltyCreateEditDto.getSale());
        loyalty.setMinSpent(loyaltyCreateEditDto.getMinSpent());
    }
}
