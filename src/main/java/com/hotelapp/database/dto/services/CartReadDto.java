package com.hotelapp.database.dto.services;

import com.hotelapp.database.dto.user.UserReadDto;
import com.hotelapp.database.entity.Service;
import lombok.Value;

import java.util.List;

@Value
public class CartReadDto {
    Long id;

    Integer sum;

//    List<Service> services;
}
