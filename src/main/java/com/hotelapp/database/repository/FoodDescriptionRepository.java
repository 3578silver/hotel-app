package com.hotelapp.database.repository;

import com.hotelapp.database.entity.Food;
import com.hotelapp.database.entity.FoodDescription;
import com.hotelapp.database.entity.FoodType;
import com.hotelapp.database.entity.RoomTypeDescription;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface FoodDescriptionRepository extends
        JpaRepository<FoodDescription, Integer>,
        QuerydslPredicateExecutor<FoodDescription> {

    @Query(
            value = """
    SELECT id FROM food_description f WHERE f.ru_name ILIKE :ruName limit 1
""", nativeQuery = true
    )
    Integer findByRuName(String ruName);

    @Query(value = """
UPDATE food_description
SET ru_name = :foodRuDescription,
en_name = :foodEnDescription
WHERE id = :foodDescriptionId
RETURNING *;
""", nativeQuery = true)
    FoodDescription updateFoodDescription(Integer foodDescriptionId, String foodRuDescription, String foodEnDescription);

}