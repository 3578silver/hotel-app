package com.hotelapp.database.dto.booking;

import com.hotelapp.database.dto.booking.locales.ConveniencyDescriptionDto;
import jakarta.validation.constraints.NotEmpty;
import lombok.Value;

@Value
public class ConveniencyCreateEditDto {

    String svg_link;
    ConveniencyDescriptionDto conveniencyDescriptionDto;
}
