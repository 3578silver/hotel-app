package com.hotelapp.database.repository;

import com.hotelapp.database.entity.RoomTypeDescription;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface RoomTypeDescriptionRepository extends
        JpaRepository<RoomTypeDescription, Long>,
        QuerydslPredicateExecutor<RoomTypeDescription> {
    @Query(value = """
SELECT id
FROM room_type_description
WHERE en_name = :enName
""", nativeQuery = true)
    Long findIdByEnName(String enName);

    @Query(value = """
UPDATE room_type_description
SET ru_name = :roomTypeRuDescription,
en_name = :roomTypeEnDescription
WHERE id = :roomTypeDescriptionId
RETURNING *;
""", nativeQuery = true)
    RoomTypeDescription updateRoomTypeDescription(Long roomTypeDescriptionId, String roomTypeRuDescription, String roomTypeEnDescription);
}
