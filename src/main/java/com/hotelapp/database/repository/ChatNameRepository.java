package com.hotelapp.database.repository;

import com.hotelapp.database.entity.Chat;
import com.hotelapp.database.entity.ChatName;
import com.hotelapp.database.entity.Message;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface ChatNameRepository extends
        JpaRepository<ChatName, Short>,
        QuerydslPredicateExecutor<ChatName> {

    @Query(value = """
UPDATE chat_name
SET ru_name = :ruName,
en_name = :enName
WHERE id = :id
RETURNING *;
""", nativeQuery = true)
    ChatName updateChatName(Short id, String ruName, String enName);
}