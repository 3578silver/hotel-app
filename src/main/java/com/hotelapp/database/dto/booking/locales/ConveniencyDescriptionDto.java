package com.hotelapp.database.dto.booking.locales;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Size;
import lombok.Value;

@Value
public class ConveniencyDescriptionDto {
    @NotEmpty
    @Size(min = 1, max = 255)
    String ruName;

    @NotEmpty
    @Size(min = 1, max = 255)
    String enName;
}
