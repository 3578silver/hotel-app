package com.hotelapp.mapper.user;

import com.hotelapp.database.dto.user.ChatCreateEditDto;
import com.hotelapp.database.dto.user.locales.ChatNameDto;
import com.hotelapp.database.entity.*;
import com.hotelapp.mapper.Mapper;
import com.hotelapp.mapper.user.locales.ChatNameCreateEditMapper;
import com.hotelapp.mapper.user.locales.ChatNameReadMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@RequiredArgsConstructor
public class ChatCreateEditMapper implements Mapper<ChatCreateEditDto, Chat> {


    private final ChatNameCreateEditMapper chatNameCreateEditMapper;

    @Override
    public Chat map(ChatCreateEditDto chatCreateEditDto, Chat chat) {
        copy(chatCreateEditDto, chat);
        return chat;
    }

    @Override
    public Chat map(ChatCreateEditDto chatCreateEditDto) {
        Chat chat = new Chat();
        copy(chatCreateEditDto, chat);
        return chat;
    }

    private void copy(ChatCreateEditDto chatCreateEditDto, Chat chat) {
        ChatName chatName = Optional.of(chatCreateEditDto.getName())
                        .map(chatNameCreateEditMapper::map)
                                .orElse(null);
        chat.setName(chatName);
    }
}