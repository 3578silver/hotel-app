package com.hotelapp.database.repository;

import com.hotelapp.database.dto.booking.RoomTypeReadDto;
import com.hotelapp.database.entity.Room;
import com.hotelapp.database.entity.RoomType;
import org.springframework.data.domain.Example;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import java.util.List;
import java.util.Optional;

public interface RoomTypeRepository extends
        JpaRepository<RoomType, Integer>,
        QuerydslPredicateExecutor<RoomType> {

    @Query(
            value = """
            SELECT *
            FROM room_type
            ORDER BY price asc
""", nativeQuery = true
    )
    List<RoomType> findAllOrderedByPrice();

    RoomType findRoomTypeById(Integer roomTypeId);

    @Query(value = """
SELECT room_type.id
FROM room_type
JOIN room_type_name rtn on rtn.id = room_type.type_name_id
WHERE ru_name = :ruName
""", nativeQuery = true)
    Integer findRoomTypeByRuName(String ruName);


    @Query(value = """
UPDATE room_type
SET price = :price
WHERE id = :id
RETURNING *;
""", nativeQuery = true)
    RoomType updatePrice(Integer id, Integer price);
}
