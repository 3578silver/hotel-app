package com.hotelapp.mapper.news.locales;

import com.hotelapp.database.dto.news.locales.NewsDescriptionDto;
import com.hotelapp.database.dto.user.UserReadDto;
import com.hotelapp.database.entity.NewsDescription;
import com.hotelapp.database.entity.User;
import com.hotelapp.mapper.Mapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class NewsDescriptionReadMapper implements Mapper<NewsDescription, NewsDescriptionDto> {
    @Override
    public NewsDescriptionDto map(NewsDescription newsDescription) {
        return new NewsDescriptionDto(
                newsDescription.getRuName(),
                newsDescription.getEnName()
        );
    }
}
