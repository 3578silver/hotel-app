package com.hotelapp.database.dto.food;

import lombok.Value;

@Value
public class FoodCartFoodReadDto {
    Long id;

    FoodCartReadDto foodCartReadDto;

    FoodReadDto foodReadDto;
}
