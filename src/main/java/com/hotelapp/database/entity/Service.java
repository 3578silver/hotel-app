package com.hotelapp.database.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "service")
public class Service {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "service_name_id", nullable = false)
    private ServiceName serviceName;

    @ManyToOne()
    @JoinColumn(name = "service_type_id", nullable = false)
    private ServiceType serviceType;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "service_description_id", nullable = false)
    private ServiceDescription serviceDescription;

    @Builder.Default
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "service", orphanRemoval = true)
    private List<ServiceCart> serviceCarts = new ArrayList<>();

    @NotEmpty
    @Column(name = "image_link", nullable = false)
    private String imageLink;

    @NotNull
    @Column(name = "price", nullable = false)
    private Integer price;
}
