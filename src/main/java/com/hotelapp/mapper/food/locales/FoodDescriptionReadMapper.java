package com.hotelapp.mapper.food.locales;

import com.hotelapp.database.dto.food.locales.FoodDescriptionDto;
import com.hotelapp.database.dto.food.locales.FoodNameDto;
import com.hotelapp.database.entity.FoodDescription;
import com.hotelapp.database.entity.FoodName;
import com.hotelapp.mapper.Mapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class FoodDescriptionReadMapper implements Mapper<FoodDescription, FoodDescriptionDto> {
    @Override
    public FoodDescriptionDto map(FoodDescription foodDescription) {
        return new FoodDescriptionDto(
                foodDescription.getRuName(),
                foodDescription.getEnName()
        );
    }
}