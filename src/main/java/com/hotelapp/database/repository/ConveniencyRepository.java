package com.hotelapp.database.repository;

import com.hotelapp.database.dto.booking.ConveniencyReadDto;
import com.hotelapp.database.entity.Conveniency;
import com.hotelapp.database.entity.FoodCartFood;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import java.util.List;
import java.util.Optional;

public interface ConveniencyRepository extends
        JpaRepository<Conveniency, Integer>,
        QuerydslPredicateExecutor<Conveniency> {

    @Query(value = """
        SELECT c.*
        FROM conveniency c
        LEFT JOIN type_conveniences tc on c.id = tc.conveniency_id
        WHERE tc.room_type_id = :roomTypeId
        ORDER BY c.id ASC
        """, nativeQuery = true)
    List<Conveniency> findAllByRoomTypeId(Integer roomTypeId);

    Conveniency findByConveniencyDescription_Id(Integer id);


    @Query(value = """
SELECT c.*
FROM conveniency c
    left join type_conveniences tc
        on c.id = tc.conveniency_id
WHERE c.id = tc.conveniency_id AND tc.room_type_id = :roomTypeId
""",
            nativeQuery = true)
    List<Conveniency> findAllConvienciesByType(Integer roomTypeId);
}
