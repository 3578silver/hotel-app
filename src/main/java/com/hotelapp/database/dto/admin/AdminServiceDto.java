package com.hotelapp.database.dto.admin;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Size;
import lombok.Value;

@Value
public class AdminServiceDto {
    @Size(min = 1, max = 255)
    String ruName;
    @Size(min = 1, max = 255)
    String enName;
    @Size(min = 1, max = 3071)
    String ruDescription;
    @Size(min = 1, max = 3071)
    String enDescription;
    String serviceType;
    Integer price;
    String imageLink;
}