package com.hotelapp.mapper.user;

import com.hotelapp.database.dto.services.CleaningOptionCleaningCreateEditDto;
import com.hotelapp.database.entity.CleaningOption;
import com.hotelapp.database.entity.CleaningOptionCleaning;
import com.hotelapp.database.repository.CleaningOptionRepository;
import com.hotelapp.database.repository.CleaningRepository;
import com.hotelapp.mapper.Mapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class CleaningOptionCleaningCreateEditMapper implements Mapper<CleaningOptionCleaningCreateEditDto, CleaningOptionCleaning> {

    private final CleaningRepository cleaningRepository;

    private final CleaningOptionRepository cleaningOptionRepository;

    @Override
    public CleaningOptionCleaning map(CleaningOptionCleaningCreateEditDto cleaningOptionCleaningCreateEditDto) {
        CleaningOptionCleaning cleaningOptionCleaning = new CleaningOptionCleaning();
        copy(cleaningOptionCleaningCreateEditDto, cleaningOptionCleaning);
        return cleaningOptionCleaning;
    }

    @Override
    public CleaningOptionCleaning map(CleaningOptionCleaningCreateEditDto cleaningOptionCleaningCreateEditDto, CleaningOptionCleaning cleaningOptionCleaning) {
        copy(cleaningOptionCleaningCreateEditDto, cleaningOptionCleaning);
        return cleaningOptionCleaning;
    }

    private void copy(CleaningOptionCleaningCreateEditDto cleaningOptionCleaningCreateEditDto, CleaningOptionCleaning cleaningOptionCleaning) {
        cleaningOptionCleaning.setCleaning(cleaningRepository.findById(
                cleaningOptionCleaningCreateEditDto.getCleaningId()).orElse(null));
        cleaningOptionCleaning.setCleaningOption(cleaningOptionRepository.findById(
                cleaningOptionCleaningCreateEditDto.getCleaningOptionId()).orElse(null));
    }
}
