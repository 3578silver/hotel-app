package com.hotelapp.database.dto.services;

import com.hotelapp.database.dto.services.locales.CleaningOptionCreateEditDto;
import com.hotelapp.database.dto.services.locales.CleaningOptionReadDto;
import com.hotelapp.database.entity.CleaningOption;
import com.hotelapp.database.entity.CleaningTime;
import lombok.Value;

import java.util.ArrayList;
import java.util.List;


@Value
public class CleaningForm {
    List<Integer> options = new ArrayList<>(); // list of options ids
}
