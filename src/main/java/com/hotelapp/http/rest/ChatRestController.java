package com.hotelapp.http.rest;

import com.hotelapp.database.dto.food.*;
import com.hotelapp.database.dto.user.*;
import com.hotelapp.database.dto.user.locales.ChatNameDto;
import com.hotelapp.service.ChatService;
import com.hotelapp.service.FoodService;
import com.hotelapp.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/chat")
@RequiredArgsConstructor
public class ChatRestController {

    private final ChatService chatService;
    private final UserService userService;

    @GetMapping(path = "/findAllChats", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ChatReadDto> findAllChats() {
        return chatService.findAllChats();
    }

    @GetMapping(path = "/findAllUserChats", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<UserChatReadDto> findAllUserChats() {
        return chatService.findAllUserChats();
    }

    @GetMapping(path = "/findAllMessages", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<MessageReadDto> findAllMessages(Long userChatId) {
        return chatService.findAllMessagesByUserChatId(userChatId);
    }

    @GetMapping("/getUnreadMessageCount")
    public ResponseEntity<Long> getUnreadMessageCount(@AuthenticationPrincipal UserDetails userDetails) {
        try {
            UserReadDto currentUser = userService.findByUsername(userDetails.getUsername()).get();

            return ResponseEntity.ok(chatService.findCountMessagesInAllChats(currentUser.getId(), currentUser.getRole().getAuthority()));
        } catch (NullPointerException e) {
            return ResponseEntity.ok(0L);
        }

    }


    @PostMapping(path = "/createChat", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public ChatReadDto createChat(@Validated @RequestBody ChatNameDto chatNameDto){
        return chatService.createChat(chatNameDto);
    }

    @PostMapping(path = "/createChatForAllUsers", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public ChatReadDto createChatForAllUsers(@Validated @RequestBody ChatCreateEditDto chatCreateEditDto){
        return chatService.createChatForAllUsers(chatCreateEditDto);
    }

    @PostMapping(path = "/createUserChat", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public UserChatReadDto createUserChat(@Validated @RequestBody UserChatCreateEditDto userChatCreateEditDto){
        return chatService.createUserChat(userChatCreateEditDto);
    }




    @DeleteMapping("/deleteAllChats")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public boolean deleteAllChats(){
        try {
            chatService.deleteAllChats();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @DeleteMapping("/deleteAllUserChats")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public boolean deleteAllUserChats(){
        try {
            chatService.deleteAllUserChats();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

}