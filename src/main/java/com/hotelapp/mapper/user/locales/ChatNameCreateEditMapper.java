package com.hotelapp.mapper.user.locales;

import com.hotelapp.database.dto.food.locales.FoodTypeDto;
import com.hotelapp.database.dto.user.locales.ChatNameDto;
import com.hotelapp.database.entity.ChatName;
import com.hotelapp.database.entity.FoodType;
import com.hotelapp.mapper.Mapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class ChatNameCreateEditMapper implements Mapper<ChatNameDto, ChatName> {
    @Override
    public ChatName map(ChatNameDto chatNameDto) {
        ChatName chatName = new ChatName();
        copy(chatNameDto, chatName);
        return chatName;
    }

    @Override
    public ChatName map(ChatNameDto chatNameDto, ChatName chatName) {
        copy(chatNameDto, chatName);
        return chatName;
    }


    private void copy(ChatNameDto chatNameDto, ChatName chatName) {
        chatName.setRuName(chatNameDto.getRuName());
        chatName.setEnName(chatNameDto.getEnName());
    }
}