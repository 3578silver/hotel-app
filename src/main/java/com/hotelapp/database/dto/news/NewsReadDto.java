package com.hotelapp.database.dto.news;

import com.hotelapp.database.dto.news.locales.NewsDescriptionDto;
import com.hotelapp.database.dto.news.locales.NewsNameDto;
import com.hotelapp.database.entity.NewsDescription;
import lombok.Value;

@Value
public class NewsReadDto {
    Long id;
    NewsNameDto newsName;
    NewsDescriptionDto newsDescription;
    String imageLink;
}
