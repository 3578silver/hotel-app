package com.hotelapp.mapper.services.locales;

import com.hotelapp.database.dto.services.locales.ServiceNameDto;
import com.hotelapp.database.dto.services.locales.ServiceTypeDto;
import com.hotelapp.database.entity.ServiceName;
import com.hotelapp.database.entity.ServiceType;
import com.hotelapp.mapper.Mapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class ServiceTypeReadMapper implements Mapper<ServiceType, ServiceTypeDto> {
    @Override
    public ServiceTypeDto map(ServiceType serviceType) {
        return new ServiceTypeDto(
                serviceType.getRuName(),
                serviceType.getEnName()
        );
    }
}
