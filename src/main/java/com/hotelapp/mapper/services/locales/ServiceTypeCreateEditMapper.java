package com.hotelapp.mapper.services.locales;

import com.hotelapp.database.dto.services.locales.ServiceNameDto;
import com.hotelapp.database.dto.services.locales.ServiceTypeDto;
import com.hotelapp.database.entity.ServiceName;
import com.hotelapp.database.entity.ServiceType;
import com.hotelapp.mapper.Mapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class ServiceTypeCreateEditMapper implements Mapper<ServiceTypeDto, ServiceType> {
    @Override
    public ServiceType map(ServiceTypeDto serviceTypeDto, ServiceType serviceType) {
        copy(serviceTypeDto, serviceType);
        return serviceType;
    }

    @Override
    public ServiceType map(ServiceTypeDto serviceTypeDto) {
        ServiceType serviceType = new ServiceType();
        copy(serviceTypeDto, serviceType);
        return serviceType;
    }

    private void copy(ServiceTypeDto serviceTypeDto, ServiceType serviceType) {
        serviceType.setRuName(serviceTypeDto.getRuName());
        serviceType.setEnName(serviceTypeDto.getEnName());
    }
}
