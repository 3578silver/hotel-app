package com.hotelapp.mapper.services.locales;

import com.hotelapp.database.dto.news.locales.NewsDescriptionDto;
import com.hotelapp.database.dto.services.locales.ServiceDescriptionDto;
import com.hotelapp.database.entity.NewsDescription;
import com.hotelapp.database.entity.ServiceDescription;
import com.hotelapp.mapper.Mapper;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class ServiceDescriptionReadMapper implements Mapper<ServiceDescription, ServiceDescriptionDto> {
    @Override
    public ServiceDescriptionDto map(ServiceDescription serviceDescription) {
        return new ServiceDescriptionDto(
                serviceDescription.getRuName(),
                serviceDescription.getEnName()
        );
    }
}
