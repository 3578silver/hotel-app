package com.hotelapp.database.dto.admin;

import com.hotelapp.database.dto.food.locales.FoodDescriptionDto;
import com.hotelapp.database.dto.food.locales.FoodNameDto;
import com.hotelapp.database.dto.food.locales.FoodTypeDto;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Value;

@Value
public class AdminFoodDto {
    @Size(min = 1, max = 255)
    String ruName;
    @Size(min = 1, max = 255)
    String enName;
    @Size(min = 1, max = 3071)
    String ruDescription;
    @Size(min = 1, max = 3071)
    String enDescription;
    String foodType;
    Integer cost;
    String imageLink;
}
