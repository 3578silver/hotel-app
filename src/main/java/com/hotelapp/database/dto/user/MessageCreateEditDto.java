package com.hotelapp.database.dto.user;

import com.hotelapp.database.entity.User;
import com.hotelapp.database.entity.UserChat;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.Value;
import org.hibernate.validator.constraints.Length;

import java.time.LocalDate;

@Value
public class MessageCreateEditDto {

    @Length(max = 256)
    String fromWhom;

    Long userChatId;

    @Length(max = 6142)
    String text;

    @Length(max = 256)
    String imageLink;
}
