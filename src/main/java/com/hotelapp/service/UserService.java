package com.hotelapp.service;

import com.hotelapp.database.dto.food.FoodCartCreateEditDto;
import com.hotelapp.database.dto.food.FoodCartReadDto;
import com.hotelapp.database.dto.services.CartCreateEditDto;
import com.hotelapp.database.dto.services.CartReadDto;
import com.hotelapp.database.dto.services.CleaningCreateEditDto;
import com.hotelapp.database.dto.services.CleaningReadDto;
import com.hotelapp.database.dto.user.LoyaltyReadDto;
import com.hotelapp.database.dto.user.UserCreateEditDto;
import com.hotelapp.database.dto.user.UserReadDto;
import com.hotelapp.database.entity.CleaningTime;
import com.hotelapp.database.entity.User;
import com.hotelapp.database.repository.LoyaltyRepository;
import com.hotelapp.database.repository.UserRepository;
import com.hotelapp.mapper.user.LoyaltyCreateEditMapper;
import com.hotelapp.mapper.user.UserCreateEditMapper;
import com.hotelapp.mapper.user.UserReadMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.session.SessionInformation;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class UserService implements UserDetailsService {

    private final UserRepository userRepository;
    private final UserReadMapper userReadMapper;
    private final UserCreateEditMapper userCreateEditMapper;

    private final ChatService chatService;
    private final LoyaltyRepository loyaltyRepository;
    private final LoyaltyService loyaltyService;

    private final LoyaltyCreateEditMapper loyaltyCreateEditMapper;
    private final PasswordEncoder passwordEncoder;
    public List<UserReadDto> findAll() {
        return userRepository.findAll().stream()
                .map(userReadMapper::map)
                .toList();

    }

    public Page<UserReadDto> findAll(Pageable pageable) {
        return userRepository.findAllByOrderByIdAsc(pageable)
                .map(userReadMapper::map);

    }

    public List<UserReadDto> findAllOrderedById() {
        return userRepository.findAllOrderedById().stream()
                .map(userReadMapper::map)
                .toList();

    }


    public Optional<UserReadDto> findById(Long id) {
        return userRepository.findById(id)
                .map(userReadMapper::map);
    }

    public Optional<UserReadDto> findByUsername(String username) {
        return userRepository.findByUsername(username)
                .map(userReadMapper::map);
    }

    @Transactional
    public User create(UserCreateEditDto userDto) {
        if (userRepository.findByUsername(userDto.getUsername()).isPresent()) {
            throw new IllegalArgumentException("Username already exists");
        }
        userDto.setCartCreateEditDto(new CartCreateEditDto(
                0
        ));
        userDto.setFoodCartCreateEditDto(new FoodCartCreateEditDto(
                0,
                0,
                ""
        ));
        userDto.setCleaningCreateEditDto(new CleaningCreateEditDto(
                CleaningTime.NOW,
                "0",
                ""
        ));
        userDto.setLoyaltyId(loyaltyRepository.findLoyaltyWithMinId());
        User user = Optional.of(userDto)
                .map(userCreateEditMapper::map)
                .map(userRepository::save)
                .orElseThrow();
        chatService.createAllUserChatsForUser(user.getId());
        return user;
    }


    @Transactional
    public Optional<UserReadDto> update(Long id, UserCreateEditDto userDto) {
        return userRepository.findById(id)
                .map(user -> userCreateEditMapper.map(userDto, user))
                .map(userRepository::saveAndFlush)
                .map(userReadMapper::map);
    }

    @Transactional
    public boolean delete(Long id) {
        return userRepository.findById(id)
                .map(entity -> {
                    userRepository.delete(entity);
                    userRepository.flush();
                    return true;
                })
                .orElse(false);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return userRepository.findByUsername(username)
                .map(user -> new org.springframework.security.core.userdetails.User(
                        user.getUsername(),
                        user.getPassword(),
                        Collections.singleton(user.getRole())
                )).orElseThrow(() -> new UsernameNotFoundException("Failed to retrieve user: " + username));
    }


    @Transactional
    public UserReadDto updateUserProfile(Long id, String firstname, String lastname, String phoneNumber, LocalDate birthDate) {
        return userRepository.updateFirstnameLastnamePhoneNumberBirthDate(id, firstname, lastname, phoneNumber, birthDate)
                .map(userReadMapper::map)
                .orElseThrow(() -> new UsernameNotFoundException("Failed to retrieve user: " + id));
    }

    @Transactional
    public UserReadDto updateUserPassword(Long id, UserCreateEditDto userCreateEditDto) {
        String password = Optional.of(userCreateEditDto.getPassword())
                .filter(StringUtils::hasText)
                .map(passwordEncoder::encode).orElse(null);

        return userRepository.updatePassword(id, password)
                .map(userReadMapper::map)
                .orElseThrow(() -> new UsernameNotFoundException("Failed to retrieve user: " + id));

    }

    @Transactional
    public UserReadDto updateUserRole(Long id, UserCreateEditDto userCreateEditDto) {
        String role = userCreateEditDto.getRole().getAuthority();

        return userRepository.updateRole(id, role)
                .map(userReadMapper::map)
                .orElseThrow(() -> new UsernameNotFoundException("Failed to retrieve user: " + id));

    }


    @Transactional
    public UserReadDto addCost(Integer price, Long userId) {
        UserReadDto userReadDto = userRepository.addCost(price, userId)
                .map(userReadMapper::map)
                .orElseThrow(() -> new UsernameNotFoundException("Failed to retrieve user: " + userId));
        List<LoyaltyReadDto> loyalties = loyaltyService.getAllLoyalties();
        if (loyalties.size() != userReadDto.getLoyalty().getId()){ // проверка на достижение пользователем максимального уровня лояльности
            if (loyalties.get(userReadDto.getLoyalty().getId() - 1 + 1).getMinSpent() <= userReadDto.getSpent()){
                userRepository.updateLoyalty(userReadDto.getId(), (short) (userReadDto.getLoyalty().getId() + 1))
                        .map(userReadMapper::map)
                        .orElseThrow(() -> new IllegalStateException("Invalid"));
            }
        }
        return userReadDto;

    }
}
