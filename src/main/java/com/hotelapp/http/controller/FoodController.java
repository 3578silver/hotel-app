package com.hotelapp.http.controller;

import com.hotelapp.database.dto.PayDto;
import com.hotelapp.database.dto.food.*;
import com.hotelapp.database.dto.services.CleaningCreateEditDto;
import com.hotelapp.database.dto.user.UserReadDto;
import com.hotelapp.database.entity.FoodCart;
import com.hotelapp.database.entity.User;
import com.hotelapp.service.ChatService;
import com.hotelapp.service.FoodService;
import com.hotelapp.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;
import java.util.Optional;

@Controller
@RequiredArgsConstructor
public class FoodController {
    private final FoodService foodService;

    private final UserService userService;
    private final ChatService chatService;

    @GetMapping("/food")
    public String getAllFood(Model model,
                             @AuthenticationPrincipal UserDetails userDetails) {
        UserReadDto currentUser = userService.findByUsername(userDetails.getUsername()).get();
        model.addAttribute("countNotReadMessages", chatService.findCountMessagesInAllChats(currentUser.getId(), currentUser.getRole().getAuthority()));
        model.addAttribute("currentUser", currentUser);
        model.addAttribute("food", foodService.findAllFood());
        model.addAttribute("foodTypes", foodService.findAllFoodTypes());
        model.addAttribute("url", "food");
        return "app/_food-menu";
    }

    @GetMapping("/food/{type}")
    public String getAllFoodByType(@PathVariable("type") String type,
                                   Model model,
                                   @AuthenticationPrincipal UserDetails userDetails) {
        UserReadDto currentUser = userService.findByUsername(userDetails.getUsername()).get();
        model.addAttribute("countNotReadMessages", chatService.findCountMessagesInAllChats(currentUser.getId(), currentUser.getRole().getAuthority()));
        model.addAttribute("currentUser", currentUser);
        model.addAttribute("food", foodService.findAllByFoodTypeId(type));
        model.addAttribute("foodTypes", foodService.findAllFoodTypes());
        model.addAttribute("currentFoodType", type);
        model.addAttribute("url", "food");
        return "app/_food-menu";
    }

    @GetMapping("/foodCart")
    public String foodCartPage(Model model,
                               @AuthenticationPrincipal UserDetails userDetails) {
        UserReadDto userReadDto = userService.findByUsername(userDetails.getUsername()).get();
        model.addAttribute("currentUser", userReadDto);
        model.addAttribute("foodCart", foodService.findFoodCartById(userReadDto.getFoodCartId()).get());
        List<FoodCartFoodReadDto> userFood = foodService.findAllFoodCartFoodByFoodCartId(userReadDto.getFoodCartId());
        if (!userFood.isEmpty()) {
            model.addAttribute("userFood", userFood);
        }
        model.addAttribute("countNotReadMessages", chatService.findCountMessagesInAllChats(userReadDto.getId(), userReadDto.getRole().getAuthority()));
        model.addAttribute("allFood", foodService.findAllIdWithFood());
        model.addAttribute("url", "foodCart");
        return "app/_food-cart";
    }

    @PostMapping("/food/add/{id}")
    public String addFoodToCart(@PathVariable("id") Integer id,
                                @AuthenticationPrincipal UserDetails userDetails) {
        UserReadDto userReadDto = userService.findByUsername(userDetails.getUsername()).get();
        FoodReadDto foodReadDto = foodService.findFoodById(id).orElse(null);
        foodService.createFoodCartFood(new FoodCartFoodCreateEditDto(userReadDto.getFoodCartId(), id));
        foodService.addSumByFoodCartId(userReadDto.getFoodCartId(), foodReadDto.getCost());
        return "redirect:/food";
    }

    @PostMapping("/food/delete/{id}")
    public String deleteFoodFromCart(@PathVariable("id") Long id,
                                @AuthenticationPrincipal UserDetails userDetails) {
        UserReadDto userReadDto = userService.findByUsername(userDetails.getUsername()).get();
        // id - это и есть foodCartFoodId
        FoodCartFoodReadDto foodCartFoodReadDto = foodService.findFoodCartFoodById(id);
        FoodReadDto foodReadDto = foodService.findFoodById(foodCartFoodReadDto.getFoodReadDto().getId()).orElse(null);
        foodService.deleteFoodCartFoodById(id);
        foodService.minusSumByFoodCartId(userReadDto.getFoodCartId(), foodReadDto.getCost());
        return "redirect:/foodCart";
    }



    @PostMapping("/orderFood")
    public String orderFood(@ModelAttribute @Validated FoodCartCreateEditDto foodCartCreateEditDto,
                            BindingResult bindingResult,
                            RedirectAttributes redirectAttributes,
                                @AuthenticationPrincipal UserDetails userDetails) {
        UserReadDto userReadDto = userService.findByUsername(userDetails.getUsername()).get();
        if (bindingResult.hasErrors()) {
            redirectAttributes.addFlashAttribute("errors", "error");
            return "redirect:/foodCart";
        }
        foodService.changeCommentAndRoomInFoodCart(userReadDto.getFoodCartId(),
                foodCartCreateEditDto.getComment(),
                foodCartCreateEditDto.getRoom()
        );
        return "redirect:/payFood";
    }

    @GetMapping("/payFood")
    public String payFood(Model model,
                            @AuthenticationPrincipal UserDetails userDetails) {
        UserReadDto currentUser = userService.findByUsername(userDetails.getUsername()).get();
        model.addAttribute("countNotReadMessages", chatService.findCountMessagesInAllChats(currentUser.getId(), currentUser.getRole().getAuthority()));
        model.addAttribute("currentUser", currentUser);
        FoodCartReadDto foodCartReadDto = foodService.findFoodCartById(currentUser.getFoodCartId()).orElse(null);
        model.addAttribute("currentUser", currentUser);
        model.addAttribute("foodCart", foodCartReadDto);
        model.addAttribute("url", "foodCart");
        return "app/_payfood";
    }

    @PostMapping("/payFoodProcess")
    public String payFoodProcess(@ModelAttribute @Validated PayDto payDto,
                              BindingResult bindingResult,
                              RedirectAttributes redirectAttributes,
                              @AuthenticationPrincipal UserDetails userDetails) {
        if (bindingResult.hasErrors()) {
            redirectAttributes.addFlashAttribute("errors", "error");
            return "redirect:/payFood";
        }
        UserReadDto userReadDto = userService.findByUsername(userDetails.getUsername()).get();
        foodService.cleanFoodCart(userReadDto.getFoodCartId());
        foodService.resetSumByFoodCartId(userReadDto.getFoodCartId());

        redirectAttributes.addFlashAttribute("createdOrder", payDto);

        return "redirect:/food";
    }
}
