package com.hotelapp.mapper.booking;

import com.hotelapp.database.dto.booking.BookingReadDto;
import com.hotelapp.database.dto.booking.RoomReadDto;
import com.hotelapp.database.dto.booking.RoomTypeReadDto;
import com.hotelapp.database.dto.user.UserReadDto;
import com.hotelapp.database.entity.Booking;
import com.hotelapp.database.entity.RoomType;
import com.hotelapp.mapper.Mapper;
import com.hotelapp.mapper.user.UserReadMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@RequiredArgsConstructor
public class BookingReadMapper implements Mapper<Booking, BookingReadDto> {

    private final RoomTypeReadMapper roomTypeReadMapper;
    private final RoomReadMapper roomReadMapper;
    private final UserReadMapper userReadMapper;
    @Override
    public BookingReadDto map(Booking booking) {
        RoomTypeReadDto roomTypeReadDto = Optional.of(booking.getRoomType()).map(roomTypeReadMapper::map).orElse(null);
        RoomReadDto roomReadDto = Optional.of(booking.getRoom()).map(roomReadMapper::map).orElse(null);
        UserReadDto userReadDto = Optional.of(booking.getUser()).map(userReadMapper::map).orElse(null);
        return new BookingReadDto(
                booking.getId(),
                roomTypeReadDto,
                roomReadDto,
                booking.getBookingStatus(),
                booking.getBookingDate(),
                booking.isPaid(),
                booking.getTotalPrice(),
                booking.getCheckIn(),
                booking.getCheckOut(),
                userReadDto
        );
    }
}
