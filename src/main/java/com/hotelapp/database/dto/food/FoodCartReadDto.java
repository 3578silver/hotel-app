package com.hotelapp.database.dto.food;

import com.hotelapp.database.dto.user.UserReadDto;
import com.hotelapp.database.dto.booking.RoomReadDto;
import lombok.Value;

import java.util.List;

@Value
public class FoodCartReadDto {
    Long id;
    Integer sum;
//    List<FoodCardFoodReadDto> foodCardFoodReadDtos;
    Integer room;
    String comment;
}
