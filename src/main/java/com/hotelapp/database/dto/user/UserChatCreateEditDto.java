package com.hotelapp.database.dto.user;

import jakarta.validation.constraints.NotNull;
import lombok.Setter;
import lombok.Value;
import lombok.experimental.NonFinal;

@Value
public class UserChatCreateEditDto {
    @NotNull
    Long userId;
    @NotNull
    Short chatId;
}
