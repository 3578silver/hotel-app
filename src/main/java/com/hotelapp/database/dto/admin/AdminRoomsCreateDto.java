package com.hotelapp.database.dto.admin;

import lombok.Value;

@Value
public class AdminRoomsCreateDto {
    String roomType;
    Integer roomCount;
}
