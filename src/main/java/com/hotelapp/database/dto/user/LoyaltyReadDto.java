package com.hotelapp.database.dto.user;

import lombok.Value;

@Value
public class LoyaltyReadDto {
    Short id;
    String name;
    Integer minSpent;
    Short sale;
}
