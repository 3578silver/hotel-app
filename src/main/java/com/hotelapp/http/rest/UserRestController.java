package com.hotelapp.http.rest;

import com.hotelapp.database.dto.user.UserCreateEditDto;
import com.hotelapp.database.dto.user.UserReadDto;
import com.hotelapp.database.repository.UserRepository;
import com.hotelapp.mapper.user.UserReadMapper;
import com.hotelapp.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/users")
@RequiredArgsConstructor
public class UserRestController {

    private final UserService userService;
    private final UserRepository userRepository;
    private final UserReadMapper userReadMapper;
    private final PasswordEncoder passwordEncoder;

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<UserReadDto> findAll() {
        return userService.findAll();
    }

    @GetMapping("/{id}")
    public UserReadDto findById(@PathVariable Long id) {
        return userService.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }


    @PutMapping("/{id}")
    public UserReadDto update(@PathVariable Long id, @Validated @RequestBody UserCreateEditDto user) {
        return userService.update(id, user)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    };


    @PutMapping(path = "/updatePassword/{id}")
    public UserReadDto updatePassword(@PathVariable Long id, String password) {

        password = Optional.of(password)
                .filter(StringUtils::hasText)
                .map(passwordEncoder::encode).orElse(null);

        return userRepository.updatePassword(id, password)
                .map(userReadMapper::map).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    };


    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable Long id){
        if (!userService.delete(id)){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }
}
