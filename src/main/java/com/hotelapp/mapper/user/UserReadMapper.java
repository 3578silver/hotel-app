package com.hotelapp.mapper.user;

import com.hotelapp.database.dto.user.LoyaltyReadDto;
import com.hotelapp.database.dto.user.UserReadDto;
import com.hotelapp.database.entity.User;
import com.hotelapp.mapper.Mapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@RequiredArgsConstructor
public class UserReadMapper implements Mapper<User, UserReadDto> {

    private final LoyaltyReadMapper loyaltyReadMapper;

    @Override
    public UserReadDto map(User user) {
        LoyaltyReadDto loyaltyDto = Optional.ofNullable(user.getLoyalty())
                .map(loyaltyReadMapper::map)
                .orElse(null);

        return new UserReadDto(
                user.getId(),
                user.getUsername(),
                user.getFirstname(),
                user.getLastname(),
                user.getPhoneNumber(),
                user.getBirthDate(),
                user.getSpent(),
                loyaltyDto,
                user.getCleaning().getId(),
                user.getFoodCart().getId(),
                user.getCart().getId(),
                user.getRole());
    }
}
