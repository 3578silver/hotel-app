package com.hotelapp.mapper.user;

import com.hotelapp.database.dto.user.MessageReadDto;
import com.hotelapp.database.entity.Message;
import com.hotelapp.mapper.Mapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@RequiredArgsConstructor
public class MessageReadMapper implements Mapper<Message, MessageReadDto> {

    private final UserChatReadMapper userChatReadMapper;
    private final UserReadMapper userReadMapper;

    @Override
    public MessageReadDto map(Message message) {
        return new MessageReadDto(
                message.getId(),
                message.getFromWhom(),
                Optional.of(message.getUserChat()).map(userChatReadMapper::map).orElse(null),
                message.getText(),
                message.getImageLink(),
                message.getSentDate(),
                message.getRead()
        );
    }
}