package com.hotelapp.mapper.services;

import com.hotelapp.database.dto.services.ServiceCreateEditDto;
import com.hotelapp.database.dto.services.locales.ServiceDescriptionDto;
import com.hotelapp.database.dto.services.locales.ServiceNameDto;
import com.hotelapp.database.dto.services.locales.ServiceTypeDto;
import com.hotelapp.database.entity.Service;
import com.hotelapp.database.entity.ServiceDescription;
import com.hotelapp.database.entity.ServiceName;
import com.hotelapp.database.entity.ServiceType;
import com.hotelapp.database.repository.ServiceTypeRepository;
import com.hotelapp.mapper.Mapper;
import com.hotelapp.mapper.services.locales.ServiceDescriptionCreateEditMapper;
import com.hotelapp.mapper.services.locales.ServiceNameCreateEditMapper;
import com.hotelapp.mapper.services.locales.ServiceTypeCreateEditMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;


import java.util.Optional;

@Component
@RequiredArgsConstructor
public class ServiceCreateEditMapper implements Mapper<ServiceCreateEditDto, Service> {

    private final ServiceTypeCreateEditMapper serviceTypeCreateEditMapper;
    private final ServiceNameCreateEditMapper serviceNameCreateEditMapper;
    private final ServiceDescriptionCreateEditMapper serviceDescriptionCreateEditMapper;
    private final ServiceTypeRepository serviceTypeRepository;

    @Override
    public Service map(ServiceCreateEditDto fromObject, Service toObject) {
        copy(fromObject, toObject);
        return toObject;
    }

    @Override
    public Service map(ServiceCreateEditDto serviceCreateEditDto) {
        Service service = new Service();
        copy(serviceCreateEditDto, service);
        return service;
    }

    private void copy(ServiceCreateEditDto serviceCreateEditDto, Service service) {
//        ServiceName serviceName = Optional.of(serviceCreateEditDto.getServiceNameDto())
//                .map(serviceNameCreateEditMapper::map)
//                .orElse(null);
//
//        ServiceType serviceType = Optional.of(serviceCreateEditDto.getServiceTypeDto())
//                .map(serviceTypeCreateEditMapper::map)
//                .orElse(null);
//
//        ServiceDescription serviceDescription = Optional.of(serviceCreateEditDto.getServiceDescriptionDto())
//                .map(serviceDescriptionCreateEditMapper::map)
//                .orElse(null);

        service.setServiceName(Optional.of(serviceCreateEditDto.getServiceNameDto())
                .map(serviceNameCreateEditMapper::map).orElse(null));
        service.setServiceType(Optional.of(serviceTypeRepository.findByEnName(serviceCreateEditDto.getServiceTypeDto().getEnName())).orElse(null));
        service.setServiceDescription(Optional.of(serviceCreateEditDto.getServiceDescriptionDto())
                .map(serviceDescriptionCreateEditMapper::map).orElse(null));
        service.setImageLink(serviceCreateEditDto.getImageLink());
        service.setPrice(serviceCreateEditDto.getPrice());
    }
}
