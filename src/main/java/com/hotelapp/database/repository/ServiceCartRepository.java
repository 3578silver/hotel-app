package com.hotelapp.database.repository;

import com.hotelapp.database.entity.FoodCartFood;
import com.hotelapp.database.entity.ServiceCart;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import java.util.List;
import java.util.Optional;

public interface ServiceCartRepository extends
        JpaRepository<ServiceCart, Long>,
        QuerydslPredicateExecutor<ServiceCart> {
    List<ServiceCart> findAllByCartId(Long cartId);


    void deleteAllByCartId(Long cartId);
}
