package com.hotelapp.database.dto.news.locales;

import jakarta.validation.constraints.NotEmpty;
import lombok.Value;

@Value
public class NewsNameDto {
    @NotEmpty
    String ruName;
    @NotEmpty
    String enName;
}
