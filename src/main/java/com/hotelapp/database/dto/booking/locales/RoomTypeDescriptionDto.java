package com.hotelapp.database.dto.booking.locales;

import com.hotelapp.database.dto.food.locales.FoodDescriptionDto;
import com.hotelapp.database.dto.food.locales.FoodNameDto;
import com.hotelapp.database.dto.food.locales.FoodTypeDto;
import jakarta.persistence.Column;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Value;

@Value
public class RoomTypeDescriptionDto {

    @NotEmpty
    String ruName;

    @NotEmpty
    String enName;
}
