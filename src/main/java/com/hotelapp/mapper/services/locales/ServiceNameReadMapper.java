package com.hotelapp.mapper.services.locales;

import com.hotelapp.database.dto.services.locales.ServiceNameDto;
import com.hotelapp.database.entity.ServiceName;
import com.hotelapp.mapper.Mapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class ServiceNameReadMapper implements Mapper<ServiceName, ServiceNameDto> {
    @Override
    public ServiceNameDto map(ServiceName serviceName) {
        return new ServiceNameDto(
                serviceName.getRuName(),
                serviceName.getEnName()
        );
    }
}
