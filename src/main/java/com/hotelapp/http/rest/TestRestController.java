package com.hotelapp.http.rest;

import com.hotelapp.database.dto.booking.*;
import com.hotelapp.database.dto.booking.locales.ConveniencyDescriptionDto;
import com.hotelapp.database.dto.booking.locales.RoomTypeDescriptionDto;
import com.hotelapp.database.dto.booking.locales.RoomTypeNameDto;
import com.hotelapp.database.dto.food.FoodCreateEditDto;
import com.hotelapp.database.dto.food.FoodReadDto;
import com.hotelapp.database.dto.food.locales.FoodDescriptionDto;
import com.hotelapp.database.dto.food.locales.FoodNameDto;
import com.hotelapp.database.dto.food.locales.FoodTypeDto;
import com.hotelapp.database.dto.news.NewsCreateEditDto;
import com.hotelapp.database.dto.news.NewsReadDto;
import com.hotelapp.database.dto.news.locales.NewsDescriptionDto;
import com.hotelapp.database.dto.news.locales.NewsNameDto;
import com.hotelapp.database.dto.services.ServiceCreateEditDto;
import com.hotelapp.database.dto.services.ServiceReadDto;
import com.hotelapp.database.dto.services.locales.*;
import com.hotelapp.database.dto.user.ChatCreateEditDto;
import com.hotelapp.database.dto.user.ChatReadDto;
import com.hotelapp.database.dto.user.LoyaltyCreateEditDto;
import com.hotelapp.database.dto.user.LoyaltyReadDto;
import com.hotelapp.database.dto.user.locales.ChatNameDto;
import com.hotelapp.database.entity.CleaningOption;
import com.hotelapp.database.repository.CleaningOptionRepository;
import com.hotelapp.database.repository.ConveniencyRepository;
import com.hotelapp.mapper.booking.ConveniencyCreateEditMapper;
import com.hotelapp.service.*;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/v1/test")
@RequiredArgsConstructor
public class TestRestController {

    private final RoomService roomService;

    private final LoyaltyService loyaltyService;

    private final TestService testService;

    private final NewsService newsService;

    private final FoodService foodService;

    private final ServicesService servicesService;

    private final CleaningService cleaningService;
    private final ChatService chatService;


    @PostMapping(path = "/createAllServiceTypes")
    @ResponseStatus(HttpStatus.CREATED)
    public List<ServiceTypeDto> createAllServiceTypes() {
        List<ServiceTypeDto> serviceTypeDtos = new ArrayList<>();
        serviceTypeDtos.add(servicesService.createServiceType(new ServiceTypeDto("СПА", "SPA")));
        serviceTypeDtos.add(servicesService.createServiceType(new ServiceTypeDto("От партнёров", "Partners")));
        serviceTypeDtos.add(servicesService.createServiceType(new ServiceTypeDto("Прочие", "Others")));
        return serviceTypeDtos;
    }

    @PostMapping(path = "/createAllServices")
    @ResponseStatus(HttpStatus.CREATED)
    public List<ServiceReadDto> createAllServices() {
        List<ServiceReadDto> serviceReadDtos = new ArrayList<>();
        serviceReadDtos.add(servicesService.createService(new ServiceCreateEditDto(
                new ServiceNameDto("Тайский массаж", "Thai massage"),
                new ServiceDescriptionDto("Тайский массаж не только метод расслабления, но и средство лечения тела и ума. После сеанса вы почувствуете себя бодрее, свежее, полными энергии и готовыми к новым достижениям и вызовам.",
                        "Thai massage is not only a method of relaxation, but also a means of treating the body and mind. After the session, you will feel more cheerful, fresher, full of energy and ready for new achievements and challenges."),
                servicesService.findServiceTypeByEnName("SPA"),
                "spa/thai_massage.jpeg",
                2500
        )));
        serviceReadDtos.add(servicesService.createService(new ServiceCreateEditDto(
                new ServiceNameDto("Ванна с маслами", "Bath with oils"),
                new ServiceDescriptionDto("Роскошный и расслабляющий способ отдохнуть после тяжелого дня и забот. Когда вы окунетесь в теплую воду с добавлением ароматических масел, вы почувствуете, как напряжение уходит из вашего тела, а ум успокаивается.",
                        "A luxurious and relaxing way to relax after a hard day and worries. When you plunge into the warm water with the addition of aromatic oils, you will feel the tension go out of your body, and the mind calms down."),
                servicesService.findServiceTypeByEnName("SPA"),
                "spa/bath_with_oils.jpg",
                2000
        )));
        serviceReadDtos.add(servicesService.createService(new ServiceCreateEditDto(
                new ServiceNameDto("Ночная яхта", "Night yacht"),
                new ServiceDescriptionDto("Путешествие на яхте позволит вам ощутить атмосферу ночного города во всей его красе и роскоши, а также насладиться великолепными коктейлями и закусками, которые будут поданы на борту.",
                        "A yacht trip will allow you to experience the atmosphere of the night city in all its glory and luxury, as well as enjoy great cocktails and snacks that will be served on board."),
                servicesService.findServiceTypeByEnName("Partners"),
                "partners/night_yacht.jpg",
                6000
        )));
        serviceReadDtos.add(servicesService.createService(new ServiceCreateEditDto(
                new ServiceNameDto("Конная прогулка", "Horse riding"),
                new ServiceDescriptionDto("Вы сможете увидеть Москву совершенно по-новому, в седле лошади, наслаждаясь красотой лесопарков и исторических достопримечательностей столицы России.",
                        "You will be able to see Moscow in a completely new way, in the saddle of a horse, enjoying the beauty of forest parks and historical sights of the capital of Russia."),
                servicesService.findServiceTypeByEnName("Partners"),
                "partners/horse_riding.jpeg",
                3500
        )));
        serviceReadDtos.add(servicesService.createService(new ServiceCreateEditDto(
                new ServiceNameDto("Аренда транспорта", "Transport rental"),
                new ServiceDescriptionDto("Независимо от того, нужно ли вам автомобиль для деловой поездки, экскурсий по городу или просто для отдыха, аренда транспорта предоставляет вам удобство передвижения.",
                        "Regardless of whether you need a car for a business trip, city tours or just for recreation, renting a vehicle provides you with the convenience of movement."),
                servicesService.findServiceTypeByEnName("Others"),
                "others/transport_rental.jpg",
                4000
        )));
        return serviceReadDtos;
    }

    @PostMapping(path = "/createAllFoodTypes")
    @ResponseStatus(HttpStatus.CREATED)
    public List<FoodTypeDto> createAllFoodTypes() {
        List<FoodTypeDto> foodTypeDtos = new ArrayList<>();
        foodTypeDtos.add(foodService.createFoodType(new FoodTypeDto("Основные", "Main")));
        foodTypeDtos.add(foodService.createFoodType(new FoodTypeDto("Салаты", "Salads")));
        foodTypeDtos.add(foodService.createFoodType(new FoodTypeDto("Закуски", "Appetizers")));
        foodTypeDtos.add(foodService.createFoodType(new FoodTypeDto("Десерты", "Desserts")));
        foodTypeDtos.add(foodService.createFoodType(new FoodTypeDto("Напитки", "Drinks")));
        return foodTypeDtos;
    }

    @PostMapping(path = "/createAllFood")
    @ResponseStatus(HttpStatus.CREATED)
    public List<FoodReadDto> createAllFood(){
        List<FoodReadDto> foodReadDtos = new ArrayList<>();
        foodReadDtos.add(foodService.createFood(new FoodCreateEditDto(
            new FoodNameDto("Лапша удон с курицей", "Udon with chicken"),
                500,
                new FoodDescriptionDto("Плотная по текстуре лапша, кусочки нежного куриного мяса и сочные, чуть хрустящие овощи. Всё приправлено соусом терияки, который придаёт блюду приятный оттенок и пикантный вкус.",
                        "Dense noodles in texture, pieces of tender chicken meat and juicy, slightly crispy vegetables. Everything is seasoned with teriyaki sauce, which gives the dish a pleasant shade and a piquant taste."),
                foodService.findFoodTypeByEnName("Main"),
                "main/udon_with_chicken.jpg"
        )));
        foodReadDtos.add(foodService.createFood(new FoodCreateEditDto(
                new FoodNameDto("Мидии в томатном соусе", "Mussels in tomato sauce"),
                700,
                new FoodDescriptionDto("Соленый вкус морепродуктов в нежнейшем томатном соусе с ярко выраженными нотками пряностей – это то, что навсегда запомнится вам и ни за что не оставит равнодушным.",
                        "The salty taste of seafood in the most delicate tomato sauce with pronounced notes of spices is something that you will always remember and will never leave you indifferent."),
                foodService.findFoodTypeByEnName("Main"),
                "main/mussels_in_tomato_sauce.jpg"
        )));
        foodReadDtos.add(foodService.createFood(new FoodCreateEditDto(
                new FoodNameDto("Свиные ребрышки барбекю", "Barbecue pork ribs"),
                670,
                new FoodDescriptionDto("Нежные и сочные ребрышки медленно приготовленные на гриле, покрытые слоем барбекю-соуса, который придает блюду особую пикантность и глубину вкуса.",
                        "Tender and juicy ribs slowly grilled, covered with a layer of barbecue sauce, which gives the dish a special piquancy and depth of taste."),
                foodService.findFoodTypeByEnName("Main"),
                "main/barbecue_pork_ribs.jpg"
        )));
        foodReadDtos.add(foodService.createFood(new FoodCreateEditDto(
                new FoodNameDto("Колбаски куриные с моцареллой", "Chicken sausages with mozzarella"),
                400,
                new FoodDescriptionDto("Колбаски из нежного фарша, приготовленного из куриного мяса, специй и трав, а также кусочков сыра моцарелла, которые придают блюду оригинальный и насыщенный вкус.",
                        "Sausages made of tender minced meat made from chicken meat, spices and herbs, as well as pieces of mozzarella cheese, which give the dish an original and rich taste."),
                foodService.findFoodTypeByEnName("Main"),
                "main/chicken_sausages_with_mozzarella.jpg"
        )));
        foodReadDtos.add(foodService.createFood(new FoodCreateEditDto(
                new FoodNameDto("Фетучини с сыром", "Fettuccine with cheese"),
                500,
                new FoodDescriptionDto("Нежные ленточки фетучини, приготовленные по классическому рецепту, и щедрая порция ароматного сыра образуют неповторимый дуэт, который дарит настоящее наслаждение.",
                        "Delicate ribbons of fettuccine, cooked according to a classic recipe, and a generous portion of fragrant cheese form a unique duet that gives real pleasure."),
                foodService.findFoodTypeByEnName("Main"),
                "main/fettuccine_with_cheese.jpg"
        )));
        foodReadDtos.add(foodService.createFood(new FoodCreateEditDto(
                new FoodNameDto("Сербская плескавица", "Serbian pleskavica"),
                550,
                new FoodDescriptionDto("Мясо маринованное в специальном соусе, состоящем из масла, лука, чеснока, паприки и других ароматных специй, которые придают насыщенный вкус и запах.",
                        "Meat marinated in a special sauce consisting of butter, onion, garlic, paprika and other aromatic spices that give a rich taste and smell."),
                foodService.findFoodTypeByEnName("Main"),
                "main/serbian_pleskavica.jpg"
        )));
        foodReadDtos.add(foodService.createFood(new FoodCreateEditDto(
                new FoodNameDto("Салат с угрём", "Salad with eel"),
                380,
                new FoodDescriptionDto("Нежный и изысканный салат. Сочный угорь, овощи и ароматный соус - все это создает баланс между кислинкой и сладостью и порадует гурманов.",
                        "A delicate and exquisite salad. Juicy eel, vegetables and fragrant sauce - all this creates a balance between sourness and sweetness and will delight gourmets."),
                foodService.findFoodTypeByEnName("Salads"),
                "salads/salad_with_eel.jpg"
        )));
        foodReadDtos.add(foodService.createFood(new FoodCreateEditDto(
                new FoodNameDto("Цезарь с цыпленком", "Caesar with chicken"),
                420,
                new FoodDescriptionDto("Всеми любимый салат, приготовлен из свежих листьев салата ромэн, сочного кусочка куриного мяса, хрустящих крутонов, тертого пармезана и аппетитного соуса Цезарь.",
                        "Everyone's favorite salad is made from fresh romaine lettuce leaves, a juicy piece of chicken meat, crispy croutons, grated parmesan and delicious Caesar sauce."),
                foodService.findFoodTypeByEnName("Salads"),
                "salads/caesar_with_chicken.jpg"
        )));
        foodReadDtos.add(foodService.createFood(new FoodCreateEditDto(
                new FoodNameDto("Салат с кальмаром", "Salad with squid"),
                470,
                new FoodDescriptionDto("Сочный кальмар с золотистой корочкой, маслины, оливки, каперсы и специи – всё это придаёт блюду насыщенный и интересный вкус.",
                        "Juicy squid with a golden crust, olives, olives, capers and spices – all this gives the dish a rich and interesting taste."),
                foodService.findFoodTypeByEnName("Salads"),
                "salads/salad_with_squid.jpg"
        )));
        foodReadDtos.add(foodService.createFood(new FoodCreateEditDto(
                new FoodNameDto("Хрустящий сыр", "Crunchy cheese"),
                200,
                new FoodDescriptionDto("Идеальное блюдо для любителей сыра и хрустящей текстуры. Сыр обваливается в яйце и панировке, затем обжариваются до золотистой корочки.",
                        "The perfect dish for lovers of cheese and crispy texture. The cheese is crumbled in egg and breaded, then fried until golden brown."),
                foodService.findFoodTypeByEnName("Appetizers"),
                "appetizers/crunchy_cheese.jpg"
        )));
        foodReadDtos.add(foodService.createFood(new FoodCreateEditDto(
                new FoodNameDto("Кесадилья с цыплёнком", "Quesadilla with chicken"),
                330,
                new FoodDescriptionDto("Блюдо мексиканской кухни. Тонкие тортильи, сочный фарш из цыпленка, приправленный специями и овощами, а также обильным количеством тертого сыра.",
                        "A dish of Mexican cuisine. Thin tortillas, juicy minced chicken, seasoned with spices and vegetables, as well as an abundant amount of grated cheese."),
                foodService.findFoodTypeByEnName("Appetizers"),
                "appetizers/quesadilla_with_chicken.jpg"
        )));
        foodReadDtos.add(foodService.createFood(new FoodCreateEditDto(
                new FoodNameDto("Острые куриные крылья Ким-чи", "Spicy Kimchi Chicken Wings"),
                250,
                new FoodDescriptionDto("Куриные крылья в золотистой корочке обмакиваются в соусе, приготовленном на основе острого соуса Ким-чи, который придает им пикантность и насыщенный вкус.",
                        "Chicken wings in a golden crust are dipped in a sauce made on the basis of hot Kimchi sauce, which gives them a piquancy and rich taste."),
                foodService.findFoodTypeByEnName("Appetizers"),
                "appetizers/spicy_kimchi_chicken_wings.jpg"
        )));
        foodReadDtos.add(foodService.createFood(new FoodCreateEditDto(
                new FoodNameDto("Тирамису", "Tiramisu"),
                150,
                new FoodDescriptionDto("Десерт, который идеально подходит для любителей кофе и сладостей. Он обладает богатым вкусом и ароматом, который подчеркивается добавлением ликера Марсала.",
                        "A dessert that is perfect for lovers of coffee and sweets. It has a rich taste and aroma, which is emphasized by the addition of Marsala liqueur."),
                foodService.findFoodTypeByEnName("Desserts"),
                "desserts/tiramisu.jpg"
        )));
        foodReadDtos.add(foodService.createFood(new FoodCreateEditDto(
                new FoodNameDto("Панакота Манго и маракуйя", "Mango and Passion Fruit Panacota"),
                150,
                new FoodDescriptionDto("Легкий и освежающий десерт, который идеально подходит для жаркого дня. Его фруктовый и кремовый вкус прекрасно сочетаются и создают неповторимый вкусовой букет.",
                        "A light and refreshing dessert that is perfect for a hot day. Its fruity and creamy taste perfectly combine and create a unique flavor bouquet."),
                foodService.findFoodTypeByEnName("Desserts"),
                "desserts/mango_and_passion_fruit_panacota.jpg"
        )));
        foodReadDtos.add(foodService.createFood(new FoodCreateEditDto(
                new FoodNameDto("Пшеничное пиво", "Wheat beer"),
                400,
                new FoodDescriptionDto("Пиво имеет нежный, слегка сладковатый вкус и аромат, а также легкую и освежающую текстуру.",
                        "The beer has a delicate, slightly sweet taste and aroma, as well as a light and refreshing texture."),
                foodService.findFoodTypeByEnName("Drinks"),
                "drinks/wheat_beer.jpg"
        )));
        foodReadDtos.add(foodService.createFood(new FoodCreateEditDto(
                new FoodNameDto("Лагер Золотой", "Golden Lager"),
                400,
                new FoodDescriptionDto("Сбалансированный вкус пива, с мягкими нотками сладости и горечи, и аромат хмеля и цветов.",
                        "The balanced taste of beer, with soft notes of sweetness and bitterness, and the aroma of hops and flowers."),
                foodService.findFoodTypeByEnName("Drinks"),
                "drinks/golden_lager.jpg"
        )));
        return foodReadDtos;
    }

    @PostMapping(path = "/createAllRoomTypes")
    @ResponseStatus(HttpStatus.CREATED)
    public List<RoomTypeReadDto> createAllRoomTypes(){
        List<RoomTypeReadDto> roomTypeReadDtos = new ArrayList<>();
        roomTypeReadDtos.add(roomService.createRoomType(new RoomTypeCreateEditDto(
                new RoomTypeNameDto("Делюкс с видом на Пушкинскую площадь", "Deluxe room with king size bed"),
                new RoomTypeDescriptionDto("Просторные номера категории Deluxe площадью от 36 до 43 кв.м располагают всеми необходимыми удобствами для комфортного отдыха. Из панорамных окон открывается вид на исторический центр города: Тверскую улицу и Пушкинскую площадь. Особую ценность номерам Deluxe придают уникальные ретрофутуристичные картины художника Станислава Трацевского.",
                        "Spacious deluxe rooms ranging from 36 to 43 sq.m have all the necessary amenities for a comfortable stay. Panoramic windows overlook the historical center of the city: Tverskaya street and Pushkinskaya square. The unique retro-futuristic paintings of the artist Stanislav Tratsevsky give a special value to the Deluxe rooms."),
                "Deluxe_room_with_king_size_bed.png",
                11898
        )));
        roomTypeReadDtos.add(roomService.createRoomType(new RoomTypeCreateEditDto(
                new RoomTypeNameDto("Делюкс с видом во внутренний дворик", "Deluxe Patio View"),
                new RoomTypeDescriptionDto("Комфортный номер Делюкс площадью от 36 до 43 кв.м. располагают всеми удобствами для комфортного проживания двух гостей. Особую эксклюзивность номерам этой категории придают картины художника Станислава Трацевского, выполненные специально для дизайн-отеля «СтандАрт». Окна номера выходят на классический московский дворик, что обеспечивает идеальную тишину для наполняющего силами отдыха.",
                        "Comfortable deluxe room from 36 to 43 sq.m. have all the amenities for a comfortable stay of two guests. Paintings by the artist Stanislav Tratsevsky, made especially for the Standard Design Hotel, give a special exclusivity to the rooms of this category. The windows of the room overlook the classic Moscow courtyard, which provides perfect silence for a rest that fills you with strength."),
                "Deluxe_Patio_View.png",
                12623
        )));
        roomTypeReadDtos.add(roomService.createRoomType(new RoomTypeCreateEditDto(
                new RoomTypeNameDto("Делюкс с двумя отдельными кроватями", "Deluxe with two separate beds"),
                new RoomTypeDescriptionDto("Просторные номера категории Deluxe площадью от 36 кв.м располагают всеми необходимыми удобствами для комфортного отдыха. Номера, выходящие окнами на московский дворик, гарантируют умиротворение и тишину в самом центре Москвы. Особую ценность номерам Deluxe придают уникальные ретрофутуристичные картины художника Станислава Трацевского.",
                        "Spacious Deluxe rooms with an area of 36 sq.m or more have all the necessary amenities for a comfortable stay. Rooms overlooking the Moscow courtyard guarantee peace and quiet in the very center of Moscow. The unique retro-futuristic paintings of the artist Stanislav Tratsevsky give a special value to the Deluxe rooms."),
                "Deluxe_with_two_separate_beds.png",
                12624
        )));
        roomTypeReadDtos.add(roomService.createRoomType(new RoomTypeCreateEditDto(
                new RoomTypeNameDto("Люкс с большой кроватью", "Luxury suite"),
                new RoomTypeDescriptionDto("Элегантные номера категории Luxe площадью от 70 до 87 кв.м оснащены панорамными окнами c великолепным видом на Страстной бульвар, Пушкинскую площадь и Тверскую улицу. Большая гостиная зона, просторная спальня и огромная ванная комната созданы для тех, кто особо ценит эксклюзивный подход и комфорт личного пространства. Каждый номер  имеет индивидуальное цветовое решение и уникальный дизайн.",
                        "Elegant Luxe rooms ranging from 70 to 87 sq.m are equipped with panoramic windows with a magnificent view of Strastnoy Boulevard, Pushkinskaya Square and Tverskaya Street. A large living area, a spacious bedroom and a huge bathroom are designed for those who especially appreciate the exclusive approach and comfort of personal space. Each room has an individual color scheme and a unique design."),
                "Luxury_suite.png",
                23970
        )));
        roomTypeReadDtos.add(roomService.createRoomType(new RoomTypeCreateEditDto(
                new RoomTypeNameDto("Люкс Премьер", "Luxe Premier"),
                new RoomTypeDescriptionDto("Элегантный номер площадью 83 кв.м выполнен в стиле минимализм в графитовых оттенках. Особенность этого номера в том, что две жилые зоны разделены: спальная зона и большая ванная комната выходят окнами во двор, а гостиная и гостевая уборная – на Пушкинскую площадь. Как и во всех Люксах, из окна открывается впечатляющий вид на исторический центр Москвы. Люкс Премьер создан специально для тех, кто ценит эксклюзивный стиль и комфорт личного пространства. ",
                        "An elegant room with an area of 83 sq.m is made in a minimalist style in graphite shades. The peculiarity of this room is that two living areas are separated: the sleeping area and the large bathroom overlook the courtyard, while the living room and guest toilet face Pushkinskaya Square. As in all suites, the window offers an impressive view of the historical center of Moscow. The Premier Suite was created especially for those who appreciate the exclusive style and comfort of their personal space."),
                "Luxe_Premier.png",
                26520
        )));
        roomTypeReadDtos.add(roomService.createRoomType(new RoomTypeCreateEditDto(
                new RoomTypeNameDto("Гранд Люкс с большой кроватью", "Grand Suite"),
                new RoomTypeDescriptionDto("Гранд люкс – премиальный и самый просторный номер дизайн-отеля \"СтандАрт\", представленный в единственном экземпляре. Общая площадь номера составляет 130 кв.м. Номер разделен на три зоны – светлую спальню с гардеробной, уютную гостиную и отдельную столовую с примыкающей к ней кухней и баром. Номер расположен на 7 этаже с панорамными окнами и головокружительным видом на Страстной бульвар, Пушкинскую площадь и улицу Тверская.",
                        "The Grand Suite is the premium and most spacious room of the Standard Design Hotel, presented in a single copy. The total area of the room is 130 sq.m. The room is divided into three zones - a bright bedroom with a dressing room, a cozy living room and a separate dining room with an adjoining kitchen and a bar. The room is located on the 7th floor with panoramic windows and a breathtaking view of Strastnoy Boulevard, Pushkinskaya Square and Tverskaya Street."),
                "Grand_Suite.png",
                35955
        )));
        return roomTypeReadDtos;
    }

    @PostMapping(path = "/createAllRoomTypeConveniencies")
    @ResponseStatus(HttpStatus.CREATED)
    public List<ConveniencyReadDto> createAllRoomTypeConveniencies() throws IOException {
        List<ConveniencyCreateEditDto> conveniencyCreateEditDtos = new ArrayList<>(){{
            add(new ConveniencyCreateEditDto("Hyppoallergy", new ConveniencyDescriptionDto("Гипоаллергенный номер", "Hyppoallergy")));
            add(new ConveniencyCreateEditDto("Conditioner", new ConveniencyDescriptionDto("Кондиционер", "Conditioner")));
            add(new ConveniencyCreateEditDto("Sound_isolation", new ConveniencyDescriptionDto("Звукоизоляция номеров", "Sound isolation")));
            add(new ConveniencyCreateEditDto("Safe", new ConveniencyDescriptionDto("Сейф", "Safe")));
            add(new ConveniencyCreateEditDto("Iron", new ConveniencyDescriptionDto("Утюг", "Iron")));
            add(new ConveniencyCreateEditDto("Service_of_wake-up", new ConveniencyDescriptionDto("Услуга побудки", "Service of wake-up")));
            add(new ConveniencyCreateEditDto("Hair_dryer", new ConveniencyDescriptionDto("Фен", "Hair dryer")));
            add(new ConveniencyCreateEditDto("Bidet", new ConveniencyDescriptionDto("Биде", "Bidet")));
            add(new ConveniencyCreateEditDto("Floor_clothes_hanger", new ConveniencyDescriptionDto("Напольная вешалка для одежды", "Floor clothes hanger")));
            add(new ConveniencyCreateEditDto("Mini-kitchen", new ConveniencyDescriptionDto("Мини-кухня", "Mini-kitchen")));
            add(new ConveniencyCreateEditDto("Fridge", new ConveniencyDescriptionDto("Холодильник", "Fridge")));
            add(new ConveniencyCreateEditDto("Coffee_and_Tea_maker", new ConveniencyDescriptionDto("Кофеварка и чайник", "Coffee and Tea maker")));


        }};


        byte[] defSvg = Files.readAllBytes(Paths.get("src", "main", "resources", "templates", "images", "conveniencies", "rezerv", "cloth" + ".svg"));
        Files.write(Paths.get("src", "main", "resources", "templates", "images", "conveniencies", "Floor_clothes_hanger" + ".svg"), defSvg);

        byte[] defSvg2 = Files.readAllBytes(Paths.get("src", "main", "resources", "templates", "images", "conveniencies", "rezerv", "conditioner" + ".svg"));
        Files.write(Paths.get("src", "main", "resources", "templates", "images", "conveniencies", "Conditioner" + ".svg"), defSvg2);

        byte[] defSvg3 = Files.readAllBytes(Paths.get("src", "main", "resources", "templates", "images", "conveniencies", "rezerv", "food" + ".svg"));
        Files.write(Paths.get("src", "main", "resources", "templates", "images", "conveniencies", "Fridge" + ".svg"), defSvg3);
        Files.write(Paths.get("src", "main", "resources", "templates", "images", "conveniencies", "Coffee_and_Tea_maker" + ".svg"), defSvg3);

        byte[] defSvg4 = Files.readAllBytes(Paths.get("src", "main", "resources", "templates", "images", "conveniencies", "rezerv", "have" + ".svg"));
        Files.write(Paths.get("src", "main", "resources", "templates", "images", "conveniencies", "Safe" + ".svg"), defSvg4);
        Files.write(Paths.get("src", "main", "resources", "templates", "images", "conveniencies", "Iron" + ".svg"), defSvg4);
        Files.write(Paths.get("src", "main", "resources", "templates", "images", "conveniencies", "Hair_dryer" + ".svg"), defSvg4);
        Files.write(Paths.get("src", "main", "resources", "templates", "images", "conveniencies", "Bidet" + ".svg"), defSvg4);

        byte[] defSvg5 = Files.readAllBytes(Paths.get("src", "main", "resources", "templates", "images", "conveniencies", "rezerv", "hyppoallergy" + ".svg"));
        Files.write(Paths.get("src", "main", "resources", "templates", "images", "conveniencies", "Hyppoallergy" + ".svg"), defSvg5);

        byte[] defSvg6 = Files.readAllBytes(Paths.get("src", "main", "resources", "templates", "images", "conveniencies", "rezerv", "kitchenette" + ".svg"));
        Files.write(Paths.get("src", "main", "resources", "templates", "images", "conveniencies", "Mini-kitchen" + ".svg"), defSvg6);

        byte[] defSvg7 = Files.readAllBytes(Paths.get("src", "main", "resources", "templates", "images", "conveniencies", "rezerv", "soundisolation" + ".svg"));
        Files.write(Paths.get("src", "main", "resources", "templates", "images", "conveniencies", "Sound_isolation" + ".svg"), defSvg7);


        byte[] defSvg8 = Files.readAllBytes(Paths.get("src", "main", "resources", "templates", "images", "conveniencies", "rezerv", "wake-up" + ".svg"));
        Files.write(Paths.get("src", "main", "resources", "templates", "images", "conveniencies", "Service_of_wake-up" + ".svg"), defSvg8);

        return testService.createAllRoomTypeConveniencies(conveniencyCreateEditDtos);
    }

    @PostMapping(path = "/createAllRooms")
    @ResponseStatus(HttpStatus.CREATED)
    public List<RoomReadDto> createAllRooms(){
        List<RoomReadDto> roomReadDtos = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            roomReadDtos.add(roomService.createRoom(new RoomCreateEditDto(
                    1
            )));
        }
        for (int i = 0; i < 5; i++) {
            roomReadDtos.add(roomService.createRoom(new RoomCreateEditDto(
                    2
            )));
        }
        for (int i = 0; i < 4; i++) {
            roomReadDtos.add(roomService.createRoom(new RoomCreateEditDto(
                    3
            )));
        }
        for (int i = 0; i < 3; i++) {
            roomReadDtos.add(roomService.createRoom(new RoomCreateEditDto(
                    4
            )));
        }
        for (int i = 0; i < 2; i++) {
            roomReadDtos.add(roomService.createRoom(new RoomCreateEditDto(
                    5
            )));
        }
        for (int i = 0; i < 1; i++) {
            roomReadDtos.add(roomService.createRoom(new RoomCreateEditDto(
                    6
            )));
        }

        return roomReadDtos;
    }
    @PostMapping(path = "/createAllLoyaltyLevels")
    @ResponseStatus(HttpStatus.CREATED)
    public List<LoyaltyReadDto> createAllLoyaltyLevels(){
        List<LoyaltyReadDto> loyaltyReadDtos = new ArrayList<>();
        loyaltyReadDtos.add(loyaltyService.createLoyalty(new LoyaltyCreateEditDto(
                "GUEST",
                0,
                (short) 0
        )));
        loyaltyReadDtos.add(loyaltyService.createLoyalty(new LoyaltyCreateEditDto(
                "VIP",
                100000,
                (short) 5
        )));
        loyaltyReadDtos.add(loyaltyService.createLoyalty(new LoyaltyCreateEditDto(
                "PREMIUM",
                300000,
                (short) 10
        )));
        return loyaltyReadDtos;
    }

    @PostMapping(path = "/createAllNews")
    @ResponseStatus(HttpStatus.CREATED)
    public List<NewsReadDto> createAllNews(){
        List<NewsReadDto> newsReadDtos = new ArrayList<>();
        newsReadDtos.add(newsService.createNews(new NewsCreateEditDto(
                new NewsNameDto("Япония расцвела", "Japan"),
                new NewsDescriptionDto("Япония расцвела. Все в этом мире расцветает благодаря аниме", "Japan"),
                "japan.jpg"
        )));
        newsReadDtos.add(newsService.createNews(new NewsCreateEditDto(
                new NewsNameDto("Рыба пельмень", "Fish"),
                new NewsDescriptionDto("Пельмени из рыбы, как вино из персиков", "Japan"),
                "fish.png"
        )));
        newsReadDtos.add(newsService.createNews(new NewsCreateEditDto(
                new NewsNameDto("Липтон лучше всего", "Lipton"),
                new NewsDescriptionDto("Лягуха пьет чай, пока ты отчаян из-за малого количества чая", "Japan"),
                "lipton.jpg"
        )));
        return newsReadDtos;
    }

    @PostMapping(path = "/createAllCleaningOptions")
    @ResponseStatus(HttpStatus.CREATED)
    public List<CleaningOptionReadDto> createAllCleaningOptions() {
        List<CleaningOptionReadDto> cleaningOptions = new ArrayList<>();
        cleaningOptions.add(cleaningService.createCleaningOption(
                new CleaningOptionCreateEditDto("Замена постельного белья", "Change of bed linen")));
        cleaningOptions.add(cleaningService.createCleaningOption(
                new CleaningOptionCreateEditDto("Замена полотенец", "Change of towels")));
        cleaningOptions.add(cleaningService.createCleaningOption(
                new CleaningOptionCreateEditDto("Дополнительные банные принадлежности", "Additional bath accessories")));
        cleaningOptions.add(cleaningService.createCleaningOption(
                new CleaningOptionCreateEditDto("Помыть полы", "Wash floors")));
        cleaningOptions.add(cleaningService.createCleaningOption(
                new CleaningOptionCreateEditDto("Полноценная уборка комнаты", "Complete room cleaning")));
        cleaningOptions.add(cleaningService.createCleaningOption(
                new CleaningOptionCreateEditDto("Услуги глажки и стирки", "Ironing and laundry services")));

        return cleaningOptions;
    }


    @PostMapping(path = "/createAllChats")
    @ResponseStatus(HttpStatus.CREATED)
    public List<ChatReadDto> createAllChats(){
        List<ChatReadDto> chatReadDtos = new ArrayList<>();
        chatReadDtos.add(chatService.createChat(new ChatNameDto("Служба размещения", "Room Service")));
        chatReadDtos.add(chatService.createChat(new ChatNameDto("Ресторан", "Restaurant")));
        chatReadDtos.add(chatService.createChat(new ChatNameDto("СПА", "SPA")));
        return chatReadDtos;
    }

    @PostMapping(path = "/createAllChatsForAllUsers")
    @ResponseStatus(HttpStatus.CREATED)
    public List<ChatReadDto> createAllChatsForAllUsers(){
        List<ChatReadDto> chatReadDtos = new ArrayList<>();
        chatReadDtos.add(chatService.createChatForAllUsers(new ChatCreateEditDto(
                new ChatNameDto("Служба размещения", "Room Service")
        )));
        chatReadDtos.add(chatService.createChatForAllUsers(new ChatCreateEditDto(
                new ChatNameDto("Ресторан", "Restaurant")
        )));
        chatReadDtos.add(chatService.createChatForAllUsers(new ChatCreateEditDto(
                new ChatNameDto("СПА", "SPA"))));
        return chatReadDtos;
    }


    @PostMapping(path = "/createAll")
    @ResponseStatus(HttpStatus.CREATED)
    public void createAll() throws IOException {
        createAllCleaningOptions();
        createAllFoodTypes();
        createAllFood();
        createAllLoyaltyLevels();
        createAllNews();
        createAllRoomTypes();
        createAllRoomTypeConveniencies();
        createAllRooms();
        createAllServiceTypes();
        createAllServices();
        createAllChats();
    }

}
