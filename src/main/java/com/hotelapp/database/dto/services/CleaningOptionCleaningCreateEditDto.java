package com.hotelapp.database.dto.services;

import com.hotelapp.database.dto.services.locales.CleaningOptionReadDto;
import com.hotelapp.database.entity.Cleaning;
import lombok.Setter;
import lombok.Value;
import lombok.experimental.NonFinal;

@Value
public class CleaningOptionCleaningCreateEditDto {
    @NonFinal
    @Setter
    Integer cleaningOptionId;
    @NonFinal
    @Setter
    Long cleaningId;
}
