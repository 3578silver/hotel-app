package com.hotelapp.database.dto.booking;

import com.hotelapp.database.entity.BookingStatus;
import jakarta.validation.constraints.NotNull;
import lombok.Setter;
import lombok.Value;
import lombok.experimental.NonFinal;

import java.time.LocalDate;

@Value
public class BookingCreateEditDto {
    @NonFinal
    @Setter
    BookingStatus bookingStatus;

    @NonFinal
    @Setter
    Long userId;

    @NotNull
    Integer roomTypeId;

    @NonFinal
    @Setter
    Integer roomId;

    @NonFinal
    @Setter
    Integer totalPrice;

    @NotNull
    LocalDate checkIn;

    @NotNull
    LocalDate checkOut;
}
