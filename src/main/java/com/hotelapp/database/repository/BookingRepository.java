package com.hotelapp.database.repository;

import com.hotelapp.database.entity.Booking;
import com.hotelapp.database.entity.Room;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface BookingRepository extends
        JpaRepository<Booking, Long>,
        QuerydslPredicateExecutor<Booking> {


//    @Query(
//            value = """
//                    SELECT r.id
//                    FROM room r
//                    LEFT JOIN booking b on r.id = b.room_id
//                    WHERE (b.room_type_id = :roomTypeId
//                    AND b.check_in > :checkIn
//                    AND b.check_out <= :checkIn
//                    AND b.check_in >= :checkOut
//                    AND b.check_out < :checkOut)
//                    OR (b.room_type_id = :roomTypeId
//                    )
//                    LIMIT 1
//                    """,
//            nativeQuery = true
//    )

    @Query(
            value = """
                    SELECT r.id
                    FROM room r
                    WHERE r.room_type_id = :roomTypeId
                    except
                    SELECT r.id
                    FROM room r
                             LEFT JOIN booking b on r.id = b.room_id
                    WHERE r.room_type_id = :roomTypeId AND b.booking_status != 'CANCELLED'
                        AND
                          ((b.check_in <= :checkIn
                              AND b.check_out > :checkIn)

                       OR (b.check_in < :checkOut
                            AND b.check_out >= :checkOut)

                              OR (b.check_out > :checkIn
                                  AND b.check_out < :checkOut)

                              OR (b.check_in < :checkOut
                                  AND b.check_in > :checkIn))
                        LIMIT 1;
                                                                                """,
            nativeQuery = true
    )
    Integer findAvailableRoomIdByRoomType(Integer roomTypeId, LocalDate checkIn, LocalDate checkOut);

    @Query(
            value = """
                    SELECT count(a.*)
                    FROM (
                    SELECT r.id
                        FROM room r
                        WHERE r.room_type_id = :roomTypeId
                        except
                        SELECT r.id
                        FROM room r
                                 LEFT JOIN booking b on r.id = b.room_id
                        WHERE r.room_type_id = :roomTypeId AND b.booking_status != 'CANCELLED'
                            AND
                              ((b.check_in <= :checkIn
                                  AND b.check_out > :checkIn)
                        
                           OR (b.check_in < :checkOut
                                AND b.check_out >= :checkOut)
                        
                                  OR (b.check_out > :checkIn
                                      AND b.check_out < :checkOut)
                        
                                  OR (b.check_in < :checkOut
                                      AND b.check_in > :checkIn))) a;
                                       """,
            nativeQuery = true
    )
    Integer findAllAvailableRoomsByRoomType(Integer roomTypeId, LocalDate checkIn, LocalDate checkOut);


    List<Booking> findTop10ByUserIdOrderByIdDesc(Long userId);


    @Query(value = """
        UPDATE booking
        SET booking_status = 'CANCELED'
        WHERE id = :bookingId
        RETURNING *;
""", nativeQuery = true)
    Optional<Booking> cancelBooking(Long bookingId);

    @Query(value = """
        UPDATE booking
        SET booking_status = 'PAID',
        is_paid = true
        WHERE id = :bookingId
        RETURNING *;
""", nativeQuery = true)
    Optional<Booking> payBooking(Long bookingId);






}
