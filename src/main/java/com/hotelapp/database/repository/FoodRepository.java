package com.hotelapp.database.repository;

import com.hotelapp.database.entity.Food;
import com.hotelapp.database.entity.FoodType;
import com.hotelapp.database.entity.User;
import jakarta.validation.constraints.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public interface FoodRepository extends
        JpaRepository<Food, Integer>,
        QuerydslPredicateExecutor<Food> {

    @Query (
            value = """
    SELECT * FROM food f WHERE f.food_type_id = :foodTypeId
""", nativeQuery = true
    )
    List<Food> findAllByFoodTypeId(Integer foodTypeId);

    @Query(value = """
SELECT *
FROM food
ORDER BY food.id desc
""", nativeQuery = true)
    List<Food> findAllDesc();

//    @Query (
//            value = """
//    SELECT id, * FROM food f
//""", nativeQuery = true
//    )
//    Map<Integer, Food> findAllIdWithFood();

    default Map<Integer, Food> findAllIdWithFood() {
        return findAll().stream().collect(Collectors.toMap(Food::getId, o -> o));
    }

    @Query(value = """
UPDATE food
SET cost = :cost
WHERE id = :id
RETURNING *;
""", nativeQuery = true)
    Food updateCost(Integer id, Integer cost);

    @Query(value = """
UPDATE food
SET food_type_id = :foodTypeId
WHERE id = :id
RETURNING *;
""", nativeQuery = true)
    Food updateFoodTypeId(Integer id, Integer foodTypeId);
}
