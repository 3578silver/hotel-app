package com.hotelapp.mapper.booking.locales;

import com.hotelapp.database.dto.booking.locales.ConveniencyDescriptionDto;
import com.hotelapp.database.entity.ConveniencyDescription;
import com.hotelapp.mapper.Mapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class ConveniencyDescriptionCreateEditMapper implements Mapper<ConveniencyDescriptionDto, ConveniencyDescription> {
    @Override
    public ConveniencyDescription map(ConveniencyDescriptionDto conveniencyDescriptionDto) {
        ConveniencyDescription conveniencyDescription = new ConveniencyDescription();
        copy(conveniencyDescriptionDto, conveniencyDescription);
        return conveniencyDescription;
    }

    @Override
    public ConveniencyDescription map(ConveniencyDescriptionDto conveniencyDescriptionDto, ConveniencyDescription conveniencyDescription) {
        copy(conveniencyDescriptionDto, conveniencyDescription);
        return conveniencyDescription;
    }

    private void copy(ConveniencyDescriptionDto conveniencyDescriptionDto, ConveniencyDescription conveniencyDescription) {
        conveniencyDescription.setRuName(conveniencyDescriptionDto.getRuName());
        conveniencyDescription.setEnName(conveniencyDescriptionDto.getEnName());
    }
}
