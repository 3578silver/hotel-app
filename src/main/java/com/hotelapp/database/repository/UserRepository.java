package com.hotelapp.database.repository;

import com.hotelapp.database.entity.User;
import com.hotelapp.mapper.Mapper;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.history.RevisionRepository;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface UserRepository extends
        JpaRepository<User, Long>,
        QuerydslPredicateExecutor<User> {

//    Optional<User> findById(Long id);
//
//    @Override
//    List<User> findAll();

    @Query(value = "SELECT u.* FROM users u WHERE u.username = :username",
            nativeQuery = true)
    List<User> findAllByUsername (String username);

    Optional<User> findUserById (Long id);
    Optional<User> findByUsername(String username) throws UsernameNotFoundException;

    @Query(value = """
        UPDATE users
        SET firstname = :firstname, lastname = :lastname, phone_number = :phoneNumber, birth_date = :birthDate
        WHERE id = :id
        RETURNING *;
""", nativeQuery = true)
    Optional<User> updateFirstnameLastnamePhoneNumberBirthDate(Long id, String firstname, String lastname,
                                                               String phoneNumber, LocalDate birthDate);

    @Query(value = """
        UPDATE users
        SET password = :password
        WHERE id = :id
        RETURNING *;
""", nativeQuery = true)
    Optional<User> updatePassword(Long id, String password);

    @Query(value = """
        UPDATE users
        SET role = :role
        WHERE id = :id
        RETURNING *;
""", nativeQuery = true)
    Optional<User> updateRole(Long id, String role);

    @Query(value = """
UPDATE users
SET spent = spent + :price
WHERE id = :userId
RETURNING *;
""", nativeQuery = true)
    Optional<User> addCost(Integer price, Long userId);

    @Query(value = """
        UPDATE users
        SET loyalty_id = :loyaltyId
        WHERE id = :id
        RETURNING *;
""", nativeQuery = true)
    Optional<User> updateLoyalty(Long id, Short loyaltyId);

    @Query(value = """
SELECT *
FROM users
ORDER BY id
""", nativeQuery = true)
    List<User> findAllOrderedById();

    Page<User> findAllByOrderByIdAsc(Pageable pageable);
}
