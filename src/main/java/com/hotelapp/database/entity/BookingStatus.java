package com.hotelapp.database.entity;

public enum BookingStatus {
    RESERVED,
    PAID,
    CANCELED
}
