package com.hotelapp.http.rest;

import com.hotelapp.database.dto.food.FoodCartCreateEditDto;
import com.hotelapp.database.dto.food.FoodCartReadDto;
import com.hotelapp.database.dto.user.LoyaltyCreateEditDto;
import com.hotelapp.database.dto.user.LoyaltyReadDto;
import com.hotelapp.database.repository.LoyaltyRepository;
import com.hotelapp.service.LoyaltyService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/loyalty")
@RequiredArgsConstructor
public class LoyaltyRestController {

    private final LoyaltyService loyaltyService;

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<LoyaltyReadDto> createLoyaltyLevel(@Validated @RequestBody LoyaltyCreateEditDto loyaltyCreateEditDto){
        return  new ResponseEntity<>(loyaltyService.createLoyalty(loyaltyCreateEditDto), HttpStatus.CREATED);
    }

}
