package com.hotelapp.database.dto.services.locales;

import jakarta.validation.constraints.NotEmpty;
import lombok.Value;

@Value
public class CleaningOptionReadDto {
    Integer id;

    @NotEmpty
    String ruName;

    @NotEmpty
    String enName;
}
