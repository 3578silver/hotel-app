package com.hotelapp.mapper.booking.locales;

import com.hotelapp.database.dto.booking.locales.RoomTypeDescriptionDto;
import com.hotelapp.database.entity.RoomTypeDescription;
import com.hotelapp.mapper.Mapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class RoomTypeDescriptionCreateEditMapper implements Mapper<RoomTypeDescriptionDto, RoomTypeDescription> {
    @Override
    public RoomTypeDescription map(RoomTypeDescriptionDto roomTypeDescriptionDto, RoomTypeDescription roomTypeDescription) {
        copy(roomTypeDescriptionDto, roomTypeDescription);
        return roomTypeDescription;
    }

    @Override
    public RoomTypeDescription map(RoomTypeDescriptionDto roomTypeDescriptionDto) {
        RoomTypeDescription roomTypeDescription = new RoomTypeDescription();
        copy(roomTypeDescriptionDto, roomTypeDescription);
        return roomTypeDescription;
    }

    private void copy(RoomTypeDescriptionDto roomTypeDescriptionDto, RoomTypeDescription roomTypeDescription) {
        roomTypeDescription.setRuName(roomTypeDescriptionDto.getRuName());
        roomTypeDescription.setEnName(roomTypeDescriptionDto.getEnName());
    }
}
