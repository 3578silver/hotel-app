package com.hotelapp.database.dto.booking;

import com.hotelapp.database.dto.booking.locales.ConveniencyDescriptionDto;
import lombok.Value;

@Value
public class ConveniencyReadDto {
    Integer id;

    String svg_link;

    ConveniencyDescriptionDto conveniencyDescriptionDto;
}
