package com.hotelapp.database.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "cleaning_option")
public class CleaningOption implements BaseEntity<Integer> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    @NotEmpty
    @Column(name = "ru_name", nullable = false)
    private String ruName;

    @NotEmpty
    @Column(name = "en_name", nullable = false)
    private String enName;

    @Builder.Default
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "cleaningOption", orphanRemoval = true)
    private List<CleaningOptionCleaning> cleaningOptionCleanings = new ArrayList<>();
}
