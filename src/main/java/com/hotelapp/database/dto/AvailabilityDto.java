package com.hotelapp.database.dto;

import com.hotelapp.database.dto.validate.annotation.BookInPast;
import com.hotelapp.database.dto.validate.annotation.CheckInBeforeCheckOut;
import com.hotelapp.database.entity.BookingStatus;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Setter;
import lombok.Value;
import lombok.experimental.NonFinal;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

@Value
@BookInPast
@CheckInBeforeCheckOut
public class AvailabilityDto {
    @NotNull
    LocalDate checkIn;

    @NotNull
    LocalDate checkOut;
}
