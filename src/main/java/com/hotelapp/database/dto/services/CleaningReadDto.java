package com.hotelapp.database.dto.services;

import com.hotelapp.database.dto.services.locales.CleaningOptionReadDto;
import com.hotelapp.database.dto.user.UserReadDto;
import com.hotelapp.database.entity.CleaningTime;
import lombok.Value;

import java.util.List;

@Value
public class CleaningReadDto {
    Long id;
    CleaningTime time;
    String room;
    String comment;

}
