package com.hotelapp.service;

import com.hotelapp.database.dto.news.NewsCreateEditDto;
import com.hotelapp.database.dto.news.NewsReadDto;
import com.hotelapp.database.dto.news.locales.NewsDescriptionDto;
import com.hotelapp.database.dto.news.locales.NewsNameDto;
import com.hotelapp.database.dto.services.locales.ServiceDescriptionDto;
import com.hotelapp.database.dto.services.locales.ServiceNameDto;
import com.hotelapp.database.repository.NewsDescriptionRepository;
import com.hotelapp.database.repository.NewsNameRepository;
import com.hotelapp.database.repository.NewsRepository;
import com.hotelapp.mapper.news.NewsCreateEditMapper;
import com.hotelapp.mapper.news.NewsReadMapper;
import com.hotelapp.mapper.news.locales.NewsDescriptionReadMapper;
import com.hotelapp.mapper.news.locales.NewsNameReadMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class NewsService {
    private final NewsRepository newsRepository;
    private final NewsReadMapper newsReadMapper;
    private final NewsCreateEditMapper newsCreateEditMapper;
    private final NewsDescriptionRepository newsDescriptionRepository;
    private final NewsDescriptionReadMapper newsDescriptionReadMapper;
    private final NewsNameReadMapper newsNameReadMapper;
    private final NewsNameRepository newsNameRepository;

    @Transactional
    public NewsReadDto createNews(NewsCreateEditDto newsCreateEditDto) {
        return Optional.of(newsCreateEditDto)
                .map(newsCreateEditMapper::map)
                .map(newsRepository::save)
                .map(newsReadMapper::map)
                .orElseThrow();
    }

    public List<NewsReadDto> findAllNews() {
        return newsRepository.findAll()
              .stream()
              .map(newsReadMapper::map)
              .toList();
    }

    public List<NewsReadDto> findAllNewsByOrderByIdDesc() {
        return newsRepository.findAllByOrderByIdDesc()
                .stream()
                .map(newsReadMapper::map)
                .toList();
    }

    @Transactional
    public boolean deleteNewsById(Long id) {
        try {
            newsRepository.deleteById(id);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public NewsReadDto findNewsById(Long id) {
        return newsRepository.findById(id)
                .map(newsReadMapper::map)
                .orElseThrow();
    }

    @Transactional
    public NewsDescriptionDto updateNewsDescription(Long newsDescriptionId, String newsRuDescription, String newsEnDescription) {
        return Optional.of(newsDescriptionRepository.updateNewsDescription(newsDescriptionId, newsRuDescription, newsEnDescription))
                .map(newsDescriptionReadMapper::map)
                .orElseThrow();
    }


    @Transactional
    public NewsNameDto updateNewsName(Long newsNameId, String newsRuName, String newsEnName) {
        return Optional.of(newsNameRepository.updateNewsName(newsNameId, newsRuName, newsEnName))
                .map(newsNameReadMapper::map)
                .orElseThrow();
    }

    public Long findNewsNameIdByRuName(String newsName) {
        return Optional.of(newsNameRepository.findByRuName(newsName))
                .orElseThrow(() -> new IllegalArgumentException("Could not find"));
    }

    public Long findNewsDescriptionIdByRuName(String newsDescription) {
        return Optional.of(newsDescriptionRepository.findByRuName(newsDescription))
                .orElseThrow(() -> new IllegalArgumentException("Could not find"));
    }

}
