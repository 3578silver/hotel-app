package com.hotelapp.mapper.services;

import com.hotelapp.database.dto.services.CartCreateEditDto;
import com.hotelapp.database.entity.Cart;
import com.hotelapp.database.repository.UserRepository;
import com.hotelapp.mapper.Mapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class CartCreateEditMapper implements Mapper<CartCreateEditDto, Cart> {

    private final UserRepository userRepository;

    @Override
    public Cart map(CartCreateEditDto fromObject, Cart toObject) {
        copy(fromObject, toObject);
        return toObject;
    }

    @Override
    public Cart map(CartCreateEditDto cartCreateEditDto) {
        Cart cart = new Cart();
        copy(cartCreateEditDto, cart);
        return cart;
    }

    private void copy (CartCreateEditDto cartCreateEditDto, Cart cart) {
        cart.setSum(cartCreateEditDto.getSum());
    }
}
