package com.hotelapp.database.repository;

import com.hotelapp.database.entity.NewsName;
import com.hotelapp.database.entity.ServiceName;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface NewsNameRepository extends
        JpaRepository<NewsName, Long>,
        QuerydslPredicateExecutor<NewsName> {
    @Query(value = """
UPDATE news_name
SET ru_name = :newsRuName,
en_name = :newsEnName
WHERE id = :newsNameId
RETURNING *;
""", nativeQuery = true)
    NewsName updateNewsName(Long newsNameId, String newsRuName, String newsEnName);

    @Query(
            value = """
    SELECT id FROM news_name f WHERE f.ru_name ILIKE :ruName limit 1
""", nativeQuery = true
    )
    Long findByRuName(String ruName);
}
