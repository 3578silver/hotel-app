package com.hotelapp.database.repository;

import com.hotelapp.database.entity.RoomType;
import com.hotelapp.database.entity.RoomTypeDescription;
import com.hotelapp.database.entity.RoomTypeName;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface RoomTypeNameRepository extends
        JpaRepository<RoomTypeName, Integer>,
        QuerydslPredicateExecutor<RoomTypeName> {
    @Query(value = """
SELECT id
FROM room_type_name
WHERE en_name = :enName
""", nativeQuery = true)
    Integer findIdByEnName(String enName);
    @Query(value = """
UPDATE room_type_name
SET ru_name = :roomTypeRuName,
en_name = :roomTypeEnName
WHERE id = :roomTypeNameId
RETURNING *;
""", nativeQuery = true)
    RoomTypeName updateRoomTypeName(Integer roomTypeNameId, String roomTypeRuName, String roomTypeEnName);

}