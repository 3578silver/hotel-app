package com.hotelapp.mapper.services;

import com.hotelapp.database.dto.services.CartReadDto;
import com.hotelapp.database.entity.Cart;
import com.hotelapp.database.repository.UserRepository;
import com.hotelapp.mapper.Mapper;
import com.hotelapp.mapper.user.UserReadMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@RequiredArgsConstructor
public class CartReadMapper implements Mapper<Cart, CartReadDto> {

    private final UserReadMapper userReadMapper;

    private final UserRepository userRepository;

    @Override
    public CartReadDto map(Cart cart) {
        return new CartReadDto(
                cart.getId(),
                cart.getSum()
        );
    }
}
