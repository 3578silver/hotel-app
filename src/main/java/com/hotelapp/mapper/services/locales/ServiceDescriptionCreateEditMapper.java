package com.hotelapp.mapper.services.locales;

import com.hotelapp.database.dto.news.locales.NewsDescriptionDto;
import com.hotelapp.database.dto.services.locales.ServiceDescriptionDto;
import com.hotelapp.database.entity.NewsDescription;
import com.hotelapp.database.entity.ServiceDescription;
import com.hotelapp.mapper.Mapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class ServiceDescriptionCreateEditMapper implements Mapper<ServiceDescriptionDto, ServiceDescription> {
    @Override
    public ServiceDescription map(ServiceDescriptionDto serviceDescriptionDto, ServiceDescription serviceDescription) {
        copy(serviceDescriptionDto, serviceDescription);
        return serviceDescription;
    }

    @Override
    public ServiceDescription map(ServiceDescriptionDto serviceDescriptionDto) {
        ServiceDescription serviceDescription = new ServiceDescription();
        copy(serviceDescriptionDto, serviceDescription);
        return serviceDescription;
    }

    private void copy(ServiceDescriptionDto serviceDescriptionDto, ServiceDescription serviceDescription) {
        serviceDescription.setRuName(serviceDescriptionDto.getRuName());
        serviceDescription.setEnName(serviceDescriptionDto.getEnName());
    }
}
