package com.hotelapp.database.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "food_cart_food")
public class FoodCartFood implements BaseEntity<Long>{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "foodCart_id", nullable = false)
    private FoodCart foodCart;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "food_id", nullable = false)
    private Food food;

    public void setFoodCart(FoodCart foodCart) {
        this.foodCart = foodCart;
        this.foodCart.getFoodCartFoods().add(this);
    }

    public void setFood(Food food) {
        this.food = food;
        this.food.getFoodCartFoods().add(this);
    }
}
