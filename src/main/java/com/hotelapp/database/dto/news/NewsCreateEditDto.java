package com.hotelapp.database.dto.news;

import com.hotelapp.database.dto.news.locales.NewsDescriptionDto;
import com.hotelapp.database.dto.news.locales.NewsNameDto;
import jakarta.validation.constraints.NotEmpty;
import lombok.Setter;
import lombok.Value;
import lombok.experimental.NonFinal;

@Value
public class NewsCreateEditDto {

    @NonFinal
    @Setter
    NewsNameDto name;
    @NonFinal
    @Setter
    NewsDescriptionDto description;
    @NonFinal
    @Setter
    String imageLink;
}
