package com.hotelapp.mapper.news.locales;

import com.hotelapp.database.dto.news.NewsReadDto;
import com.hotelapp.database.dto.news.locales.NewsNameDto;
import com.hotelapp.database.entity.News;
import com.hotelapp.database.entity.NewsName;
import com.hotelapp.mapper.Mapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class NewsNameReadMapper implements Mapper<NewsName, NewsNameDto> {
    @Override
    public NewsNameDto map(NewsName newsName) {
        return new NewsNameDto(
                newsName.getRuName(),
                newsName.getEnName()
        );
    }
}
