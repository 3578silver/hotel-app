package com.hotelapp.database.dto.validate;

import com.hotelapp.database.dto.AvailabilityDto;
import com.hotelapp.database.dto.validate.annotation.CheckInBeforeCheckOut;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

import java.time.LocalDate;

public class CheckInBeforeCheckOutValidator implements ConstraintValidator<CheckInBeforeCheckOut, AvailabilityDto> {

    @Override
    public void initialize(CheckInBeforeCheckOut constraintAnnotation) {
    }

    @Override
    public boolean isValid(AvailabilityDto value, ConstraintValidatorContext context) {
        if (value == null || value.getCheckIn() == null || value.getCheckOut() == null) {
            // Поля не заполнены, валидация пропускается
            return true;
        }

        LocalDate checkIn = value.getCheckIn();
        LocalDate checkOut = value.getCheckOut();

        if (checkIn.isBefore(checkOut)) {
            // Дата заезда раньше даты выезда, валидация пройдена
            return true;
        }

        // Дата заезда больше или равна дате выезда, валидация не пройдена
        context.buildConstraintViolationWithTemplate("{checkInBeforeCheckOut.message}")
                .addPropertyNode("checkIn")
                .addConstraintViolation()
                .disableDefaultConstraintViolation();
        return false;
    }
}
