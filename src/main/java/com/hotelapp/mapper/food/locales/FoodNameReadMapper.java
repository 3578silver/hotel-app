package com.hotelapp.mapper.food.locales;

import com.hotelapp.database.dto.food.locales.FoodNameDto;
import com.hotelapp.database.dto.food.locales.FoodTypeDto;
import com.hotelapp.database.entity.FoodName;
import com.hotelapp.database.entity.FoodType;
import com.hotelapp.mapper.Mapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class FoodNameReadMapper implements Mapper<FoodName, FoodNameDto> {
    @Override
    public FoodNameDto map(FoodName foodName) {
        return new FoodNameDto(
                foodName.getRuName(),
                foodName.getEnName()
        );
    }
}