package com.hotelapp.mapper.booking.locales;

import com.hotelapp.database.dto.booking.locales.RoomTypeDescriptionDto;
import com.hotelapp.database.entity.RoomTypeDescription;
import com.hotelapp.mapper.Mapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class RoomTypeDescriptionReadMapper implements Mapper<RoomTypeDescription, RoomTypeDescriptionDto> {
    @Override
    public RoomTypeDescriptionDto map(RoomTypeDescription roomTypeDescription) {
        return new RoomTypeDescriptionDto(
                roomTypeDescription.getRuName(),
                roomTypeDescription.getEnName()
        );
    }
}
