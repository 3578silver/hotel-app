package com.hotelapp.database.dto.user;

import lombok.Value;

@Value
public class UserChatReadDto {
    Long id;
    UserReadDto userReadDto;
    ChatReadDto chatReadDto;
}
