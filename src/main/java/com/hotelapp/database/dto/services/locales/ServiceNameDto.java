package com.hotelapp.database.dto.services.locales;

import jakarta.validation.constraints.NotEmpty;
import lombok.Value;

@Value
public class ServiceNameDto {
    @NotEmpty
    String ruName;

    @NotEmpty
    String enName;
}
