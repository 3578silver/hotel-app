package com.hotelapp.database.dto.validate.annotation;

import com.hotelapp.database.dto.validate.CheckInBeforeCheckOutValidator;
import jakarta.validation.Constraint;
import jakarta.validation.Payload;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = CheckInBeforeCheckOutValidator.class)
public @interface CheckInBeforeCheckOut {

    String message() default "Дата заезда должна быть раньше даты выезда";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
