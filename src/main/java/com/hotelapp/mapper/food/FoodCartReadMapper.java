package com.hotelapp.mapper.food;

import com.hotelapp.database.dto.food.FoodCartReadDto;
import com.hotelapp.database.dto.user.LoyaltyReadDto;
import com.hotelapp.database.entity.FoodCart;
import com.hotelapp.database.entity.Loyalty;
import com.hotelapp.mapper.Mapper;
import com.hotelapp.mapper.booking.RoomReadMapper;
import com.hotelapp.mapper.user.UserReadMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@RequiredArgsConstructor
public class FoodCartReadMapper implements Mapper<FoodCart, FoodCartReadDto> {
    private final UserReadMapper userReadMapper;

    @Override
    public FoodCartReadDto map(FoodCart foodCart) {
        return new FoodCartReadDto(
                foodCart.getId(),
                foodCart.getSum(),
                foodCart.getRoom(),
                foodCart.getComment()
        );
    }
}