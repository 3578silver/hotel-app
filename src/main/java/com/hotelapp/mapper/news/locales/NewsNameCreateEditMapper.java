package com.hotelapp.mapper.news.locales;

import com.hotelapp.database.dto.news.locales.NewsNameDto;
import com.hotelapp.database.entity.NewsName;
import com.hotelapp.mapper.Mapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.Optional;

@Component
@RequiredArgsConstructor
public class NewsNameCreateEditMapper implements Mapper<NewsNameDto, NewsName> {
    @Override
    public NewsName map(NewsNameDto newsNameDto, NewsName newsName) {
        copy(newsNameDto, newsName);
        return newsName;
    }

    @Override
    public NewsName map(NewsNameDto newsNameDto) {
        NewsName newsName = new NewsName();
        copy(newsNameDto, newsName);
        return newsName;
    }

    private void copy(NewsNameDto newsNameDto, NewsName newsName) {
        newsName.setRuName(newsNameDto.getRuName());
        newsName.setEnName(newsNameDto.getEnName());
    }
}
