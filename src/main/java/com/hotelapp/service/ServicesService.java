package com.hotelapp.service;

import com.hotelapp.database.dto.food.FoodReadDto;
import com.hotelapp.database.dto.food.locales.FoodDescriptionDto;
import com.hotelapp.database.dto.food.locales.FoodNameDto;
import com.hotelapp.database.dto.food.locales.FoodTypeDto;
import com.hotelapp.database.dto.services.*;
import com.hotelapp.database.dto.services.locales.ServiceDescriptionDto;
import com.hotelapp.database.dto.services.locales.ServiceNameDto;
import com.hotelapp.database.dto.services.locales.ServiceTypeDto;
import com.hotelapp.database.entity.ServiceDescription;
import com.hotelapp.database.entity.ServiceName;
import com.hotelapp.database.entity.ServiceType;
import com.hotelapp.database.repository.*;
import com.hotelapp.mapper.services.*;
import com.hotelapp.mapper.services.locales.*;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class ServicesService {
    private final ServiceRepository serviceRepository;
    private final ServiceCartRepository serviceCartRepository;
    private final ServiceCreateEditMapper serviceCreateEditMapper;
    private final ServiceReadMapper serviceReadMapper;
    private final ServiceCartCreateEditMapper serviceCartCreateEditMapper;
    private final ServiceCartReadMapper serviceCartReadMapper;
    private final ServiceNameRepository serviceNameRepository;
    private final ServiceDescriptionRepository serviceDescriptionRepository;
    private final ServiceTypeRepository serviceTypeRepository;
    private final ServiceNameCreateEditMapper serviceNameCreateEditMapper;
    private final ServiceDescriptionCreateEditMapper serviceDescriptionCreateEditMapper;
    private final ServiceTypeCreateEditMapper serviceTypeCreateEditMapper;
    private final ServiceTypeReadMapper serviceTypeReadMapper;
    private final CartRepository cartRepository;
    private final CartReadMapper cartReadMapper;
    private final ServiceDescriptionReadMapper serviceDescriptionReadMapper;
    private final ServiceNameReadMapper serviceNameReadMapper;

    @Transactional
    public ServiceReadDto createService(ServiceCreateEditDto serviceCreateEditDto) {
        return Optional.of(serviceCreateEditDto)
                .map(serviceCreateEditMapper::map)
                .map(serviceRepository::save)
                .map(serviceReadMapper::map)
                .orElseThrow();
    }

    @Transactional
    public ServiceTypeDto createServiceType(ServiceTypeDto serviceTypeDto){
        return Optional.of(serviceTypeDto)
                .map(serviceTypeCreateEditMapper::map)
                .map(serviceTypeRepository::save)
                .map(serviceTypeReadMapper::map)
                .orElseThrow();
    }

    @Transactional
    public ServiceCartReadDto createServiceCart(ServiceCartCreateEditDto serviceCartCreateEditDto) {
        return Optional.of(serviceCartCreateEditDto)
                .map(serviceCartCreateEditMapper::map)
                .map(serviceCartRepository::save)
                .map(serviceCartReadMapper::map)
                .orElseThrow();
    }

    public List<ServiceReadDto> findAllService() {
        return serviceRepository.findAll()
                .stream()
                .map(serviceReadMapper::map)
                .toList();
    }

    public List<ServiceReadDto> findAllServicesDesc() {
        return serviceRepository.findAllDesc()
                .stream()
                .map(serviceReadMapper::map)
                .toList();
    }

    public List<ServiceTypeDto> findAllServiceTypes() {
        return serviceTypeRepository.findAll()
                .stream()
                .map(serviceTypeReadMapper::map)
                .toList();
    }

    public List<ServiceCartReadDto> findAllServiceCart() {
        return serviceCartRepository.findAll()
                .stream()
                .map(serviceCartReadMapper::map)
                .toList();
    }

    public List<ServiceReadDto> findAllByServiceTypeId(String type) {
        return serviceRepository.findAllByServiceTypeId(serviceTypeRepository.findByEnName(type).getId())
                .stream()
                .map(serviceReadMapper::map)
                .toList();
    }

    @Transactional
    public boolean deleteServicesCartById(Long id) {
        try {
            serviceCartRepository.deleteById(id);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public ServiceTypeDto findServiceTypeByEnName(String type) {
        return Optional.of(serviceTypeRepository.findByEnName(type))
                .map(serviceTypeReadMapper::map)
                .orElseThrow(() -> new IllegalArgumentException("Service type not found"));
    }

    public Optional<CartReadDto> findCartById(Long cartId) {
        return cartRepository.findById(cartId)
                .map(cartReadMapper::map);
    }



    public List<ServiceCartReadDto> findAllServiceCartByCartId(Long cartId) {
        return serviceCartRepository.findAllByCartId(cartId)
                .stream()
                .map(serviceCartReadMapper::map)
                .toList();
    }


    public Map<Integer, ServiceReadDto> findAllIdWithServices() {
        return serviceRepository.findAllIdWithService().entrySet().stream()
                .collect(Collectors.toMap(obj -> obj.getKey(), obj -> Optional.of(obj.getValue())
                        .map(serviceReadMapper::map).get()));
    }

    public Optional<ServiceReadDto> findServiceById(Integer serviceId) {
        return serviceRepository.findById(serviceId)
                .map(serviceReadMapper::map);
    }



    @Transactional
    public CartReadDto addSumByCartId(Long cartId, Integer cost) {
        CartReadDto cartReadDto = cartRepository.findById(cartId)
                .map(cartReadMapper::map)
                .orElseThrow(() -> new IllegalArgumentException("Could not find"));

        Integer newSum = cartReadDto.getSum() + cost;

        return cartRepository.updateSumById(cartId, newSum)
                .map(cartReadMapper::map)
                .orElse(null);
    }

    @Transactional
    public CartReadDto minusSumByCartId(Long cartId, Integer cost) {
        CartReadDto cartReadDto = cartRepository.findById(cartId)
                .map(cartReadMapper::map)
                .orElseThrow(() -> new IllegalArgumentException("Could not find"));

        Integer newSum = cartReadDto.getSum() - cost;

        return cartRepository.updateSumById(cartId, newSum)
                .map(cartReadMapper::map)
                .orElse(null);
    }

    public ServiceCartReadDto findServiceCartById(Long id) {
        return serviceCartRepository.findById(id)
                .map(serviceCartReadMapper::map)
                .orElse(null);
    }

    @Transactional
    public CartReadDto resetSumByCartId(Long cartId) {
        return cartRepository.updateSumById(cartId, 0)
                .map(cartReadMapper::map)
                .orElse(null);
    }

    @Transactional
    public boolean cleanCart(Long cartId) {
        serviceCartRepository.deleteAllByCartId(cartId);
        serviceCartRepository.flush();
        return true;
    }


    @Transactional
    public boolean deleteServiceById(Integer id) {
        try {
            serviceRepository.deleteById(id);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public ServiceTypeDto findServiceTypeByRuName(String type) {
        return Optional.of(serviceTypeRepository.findByRuName(type))
                .map(serviceTypeReadMapper::map)
                .orElseThrow(() -> new IllegalArgumentException("Could not find"));
    }

    public Integer findServiceTypeIdByRuName(String type) {
        return Optional.of(serviceTypeRepository.findIdByRuName(type))
                .orElseThrow(() -> new IllegalArgumentException("Could not find"));
    }


    public Integer findServiceNameIdByRuName(String serviceName) {
        return Optional.of(serviceNameRepository.findByRuName(serviceName))
                .orElseThrow(() -> new IllegalArgumentException("Could not find"));
    }

    public Integer findServiceDescriptionIdByRuName(String serviceDescription) {
        return Optional.of(serviceDescriptionRepository.findByRuName(serviceDescription))
                .orElseThrow(() -> new IllegalArgumentException("Could not find"));
    }

    @Transactional
    public ServiceReadDto updatePrice(Integer id, Integer price) {
        return Optional.of(serviceRepository.updatePrice(id, price))
                .map(serviceReadMapper::map)
                .orElseThrow();
    }

    @Transactional
    public ServiceDescriptionDto updateServiceDescription(Integer serviceDescriptionId, String serviceRuDescription, String serviceEnDescription) {
        return Optional.of(serviceDescriptionRepository.updateServiceDescription(serviceDescriptionId, serviceRuDescription, serviceEnDescription))
                .map(serviceDescriptionReadMapper::map)
                .orElseThrow();
    }


    @Transactional
    public ServiceNameDto updateServiceName(Integer serviceNameId, String serviceRuName, String serviceEnName) {
        return Optional.of(serviceNameRepository.updateServiceName(serviceNameId, serviceRuName, serviceEnName))
                .map(serviceNameReadMapper::map)
                .orElseThrow();
    }


    @Transactional
    public ServiceReadDto updateServiceTypeInService(Integer id, Integer serviceTypeId) {
        return Optional.of(serviceRepository.updateServiceTypeId(id, serviceTypeId))
                .map(serviceReadMapper::map)
                .orElseThrow();
    }
}
