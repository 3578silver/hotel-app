package com.hotelapp.http.handler;

import com.hotelapp.service.UserService;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@Slf4j
@ControllerAdvice(basePackages = "com.hotelapp.http.controller")
@RequiredArgsConstructor
public class ControllerExceptionHandler {

    private final UserService userService;

    @ExceptionHandler(Exception.class)
    public String handleExceptions(Exception exception, HttpServletRequest request, Model model,
                                   @AuthenticationPrincipal UserDetails userDetails) {
        model.addAttribute("currentUser", userService.findByUsername(userDetails.getUsername()).get());
        log.error("Failed to return response", exception);
        return "app/404.html";
    }
}
