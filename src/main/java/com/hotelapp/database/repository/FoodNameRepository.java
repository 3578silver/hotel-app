package com.hotelapp.database.repository;

import com.hotelapp.database.entity.Food;
import com.hotelapp.database.entity.FoodDescription;
import com.hotelapp.database.entity.FoodName;
import com.hotelapp.database.entity.FoodType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface FoodNameRepository extends
        JpaRepository<FoodName, Integer>,
        QuerydslPredicateExecutor<FoodName> {

    @Query(
            value = """
    SELECT id FROM food_name f WHERE f.ru_name ILIKE :ruName limit 1
""", nativeQuery = true
    )
    Integer findByRuName(String ruName);

    @Query(value = """
UPDATE food_name
SET ru_name = :foodRuName,
en_name = :foodEnName
WHERE id = :foodNameId
RETURNING *;
""", nativeQuery = true)
    FoodName updateFoodName(Integer foodNameId, String foodRuName, String foodEnName);

}