package com.hotelapp.mapper.user;

import com.hotelapp.database.dto.services.locales.CleaningOptionCreateEditDto;
import com.hotelapp.database.entity.CleaningOption;
import com.hotelapp.mapper.Mapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class CleaningOptionCreateEditMapper implements Mapper<CleaningOptionCreateEditDto, CleaningOption> {


    @Override
    public CleaningOption map(CleaningOptionCreateEditDto cleaningOptionCreateEditDto) {
        CleaningOption cleaningOption = new CleaningOption();
        copy(cleaningOptionCreateEditDto, cleaningOption);
        return cleaningOption;
    }

    @Override
    public CleaningOption map(CleaningOptionCreateEditDto fromObject, CleaningOption toObject) {
        copy(fromObject, toObject);
        return toObject;
    }

    private void copy(CleaningOptionCreateEditDto cleaningOptionCreateEditDto, CleaningOption cleaningOption) {
        cleaningOption.setEnName(cleaningOptionCreateEditDto.getEnName());
        cleaningOption.setRuName(cleaningOptionCreateEditDto.getRuName());
    }
}
