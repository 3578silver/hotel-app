package com.hotelapp.mapper.booking;

import com.hotelapp.database.dto.booking.TypeConveniencesReadDto;
import com.hotelapp.database.entity.TypeConveniences;
import com.hotelapp.mapper.Mapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@RequiredArgsConstructor
public class TypeConveniencesReadMapper implements Mapper<TypeConveniences, TypeConveniencesReadDto> {
    private final RoomTypeReadMapper roomTypeReadMapper;
    private final ConveniencyReadMapper conveniencyReadMapper;

    @Override
    public TypeConveniencesReadDto map(TypeConveniences typeConveniences) {
        return new TypeConveniencesReadDto(
                typeConveniences.getId(),
                Optional.of(typeConveniences.getRoomType()).map(roomTypeReadMapper::map).orElse(null),
                Optional.of(typeConveniences.getConveniency()).map(conveniencyReadMapper::map).orElse(null)
        );
    }
}
