package com.hotelapp.database.dto.booking;

import lombok.Value;

@Value
public class TypeConveniencesReadDto {
    Integer id;

    RoomTypeReadDto roomTypeReadDto;

    ConveniencyReadDto conveniencyReadDto;
}
