package com.hotelapp.database.dto.food.locales;

import jakarta.validation.constraints.NotEmpty;
import lombok.Value;

@Value
public class FoodTypeDto {
    @NotEmpty
    String ruName;
    @NotEmpty
    String enName;
}
