package com.hotelapp.mapper.user;

import com.hotelapp.database.dto.services.CleaningCreateEditDto;
import com.hotelapp.database.entity.Cleaning;
import com.hotelapp.database.repository.CleaningOptionRepository;
import com.hotelapp.database.repository.UserRepository;
import com.hotelapp.mapper.Mapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Optional;

@Component
@RequiredArgsConstructor
public class CleaningCreateEditMapper implements Mapper<CleaningCreateEditDto, Cleaning> {

    private final UserRepository userRepository;
    private final CleaningOptionCreateEditMapper cleaningOptionCreateEditMapper;
    private final CleaningOptionRepository cleaningOptionRepository;

    @Override
    public Cleaning map(CleaningCreateEditDto fromObject, Cleaning toObject) {
        copy(fromObject, toObject);
        return toObject;
    }

    @Override
    public Cleaning map(CleaningCreateEditDto cleaningCreateEditDto) {
        Cleaning cleaning = new Cleaning();
        copy(cleaningCreateEditDto, cleaning);
        return cleaning;
    }

    private void copy(CleaningCreateEditDto cleaningCreateEditDto, Cleaning cleaning) {
        cleaning.setComment(cleaningCreateEditDto.getComment());
        cleaning.setRoom(cleaningCreateEditDto.getRoom());
        cleaning.setCleaningTime(cleaningCreateEditDto.getTime());

    }
}
