package com.hotelapp.database.dto.services;

import com.hotelapp.database.dto.services.locales.CleaningOptionReadDto;
import lombok.Setter;
import lombok.Value;
import lombok.experimental.NonFinal;

@Value
public class CleaningOptionCleaningReadDto {
    Long id;
    CleaningOptionReadDto cleaningOptionReadDto;
    CleaningReadDto cleaningReadDto;
}
