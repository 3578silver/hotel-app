package com.hotelapp.mapper.user;

import com.hotelapp.database.dto.user.MessageCreateEditDto;
import com.hotelapp.database.entity.Message;
import com.hotelapp.database.repository.UserChatRepository;
import com.hotelapp.mapper.Mapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class MessageCreateEditMapper implements Mapper<MessageCreateEditDto, Message> {

    private final UserChatRepository userChatRepository;

    @Override
    public Message map(MessageCreateEditDto messageCreateEditDto, Message message) {
        copy(messageCreateEditDto, message);
        return message;
    }

    @Override
    public Message map(MessageCreateEditDto messageCreateEditDto) {
        Message message = new Message();
        copy(messageCreateEditDto, message);
        return message;
    }

    private void copy(MessageCreateEditDto messageCreateEditDto, Message message) {
        message.setText(messageCreateEditDto.getText());
        message.setImageLink(message.getImageLink());
        message.setUserChat(userChatRepository.findById(messageCreateEditDto.getUserChatId()).orElseThrow());
        message.setFromWhom(messageCreateEditDto.getFromWhom());
    }
}