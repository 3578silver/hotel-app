package com.hotelapp.mapper.food.locales;

import com.hotelapp.database.dto.food.locales.FoodTypeDto;
import com.hotelapp.database.dto.user.LoyaltyCreateEditDto;
import com.hotelapp.database.entity.FoodType;
import com.hotelapp.database.entity.Loyalty;
import com.hotelapp.mapper.Mapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class FoodTypeCreateEditMapper implements Mapper<FoodTypeDto, FoodType> {
    @Override
    public FoodType map(FoodTypeDto foodTypeDto) {
        FoodType foodType = new FoodType();
        copy(foodTypeDto, foodType);
        return foodType;
    }

    @Override
    public FoodType map(FoodTypeDto foodTypeDto, FoodType foodType) {
        copy(foodTypeDto, foodType);
        return foodType;
    }


    private void copy(FoodTypeDto foodTypeDto, FoodType foodType) {
        foodType.setRuName(foodTypeDto.getRuName());
        foodType.setEnName(foodTypeDto.getEnName());
    }
}