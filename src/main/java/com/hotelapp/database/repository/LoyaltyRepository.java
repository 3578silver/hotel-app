package com.hotelapp.database.repository;

import com.hotelapp.database.entity.Food;
import com.hotelapp.database.entity.Loyalty;
import com.hotelapp.database.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import java.util.List;
import java.util.Optional;

public interface LoyaltyRepository extends
        JpaRepository<Loyalty, Short>,
        QuerydslPredicateExecutor<Loyalty> {

    @Query(value = "SELECT min(l.id) FROM loyalty l",
            nativeQuery = true)
    Short findLoyaltyWithMinId();

}