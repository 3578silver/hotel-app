package com.hotelapp.database.repository;

import com.hotelapp.database.entity.Chat;
import com.hotelapp.database.entity.UserChat;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public interface UserChatRepository extends
        JpaRepository<UserChat, Long>,
        QuerydslPredicateExecutor<UserChat> {


    UserChat findByChatIdAndUserId(Short chatId, Long userId);

    default Map<Long, String> findAllWithNamesByChatId(Short chatId){
        return findAll().stream()
                .filter(userChat -> userChat.getChat().getId() == chatId)
                .collect(Collectors.toMap(o -> o.getId(), o -> o.getUser().getFirstname() + " " + o.getUser().getLastname()));
    }

    @Query(value = """
SELECT uc.*
FROM (SELECT m.userchat_id, max(m.sent_date) date
          FROM message m
          GROUP BY m.userchat_id) mc
LEFT JOIN users_chat uc on uc.id = mc.userchat_id
WHERE uc.chat_id = :chatId
ORDER BY mc.date desc;
""", nativeQuery = true)
    List<UserChat> findAllNotEmptyOrderByDateDescByChatId(Short chatId);


    default Map<Short, Long> findCountUsersInChats(){
        return findAll().stream()
                .collect(Collectors.groupingBy(o -> o.getChat().getId(), Collectors.counting()));
    }




}