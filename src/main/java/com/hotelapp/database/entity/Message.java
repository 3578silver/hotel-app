package com.hotelapp.database.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.*;
import org.hibernate.type.YesNoConverter;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "message")
public class Message implements BaseEntity<Long>{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "from_whom", length = 256)
    private String fromWhom;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "userchat_id", nullable = false)
    private UserChat userChat;

    @Column(name = "text", length = 6142)
    private String text;

    @Column(name = "image_link")
    private String imageLink;


    @Column(name = "sent_date")
    private LocalDateTime sentDate = LocalDateTime.now();

    @Convert(converter = YesNoConverter.class)
    private Boolean read = false;


    public void setUserChat(UserChat userChat) {
        this.userChat = userChat;
        this.userChat.getMessages().add(this);
    }
}