package com.hotelapp.service;

import com.hotelapp.database.dto.booking.RoomTypeReadDto;
import com.hotelapp.database.dto.user.*;
import com.hotelapp.database.dto.user.locales.ChatNameDto;
import com.hotelapp.database.entity.Chat;
import com.hotelapp.database.entity.Message;
import com.hotelapp.database.entity.UserChat;
import com.hotelapp.database.repository.MessageRepository;
import com.hotelapp.database.repository.ChatNameRepository;
import com.hotelapp.database.repository.ChatRepository;
import com.hotelapp.database.repository.UserChatRepository;
import com.hotelapp.database.repository.UserRepository;
import com.hotelapp.mapper.user.*;
import com.hotelapp.mapper.user.locales.ChatNameCreateEditMapper;
import com.hotelapp.mapper.user.locales.ChatNameReadMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class ChatService {

    private final ChatCreateEditMapper chatCreateEditMapper;
    private final ChatReadMapper chatReadMapper;
    private final ChatRepository chatRepository;

    private final UserChatCreateEditMapper userChatCreateEditMapper;
    private final UserChatReadMapper userChatReadMapper;
    private final UserChatRepository userChatRepository;
    private final ChatNameRepository chatNameRepository;

    private final ChatNameCreateEditMapper chatNameCreateEditMapper;

    private final ChatNameReadMapper chatNameReadMapper;
    private final UserRepository userRepository;
    private final MessageRepository messageRepository;
    private final MessageCreateEditMapper messageCreateEditMapper;
    private final MessageReadMapper messageReadMapper;


    @Transactional
    public ChatReadDto createChat(ChatNameDto chatNameDto) {
        ChatCreateEditDto chatCreateEditDto = new ChatCreateEditDto(chatNameDto);
        return Optional.of(chatCreateEditDto)
                .map(chatCreateEditMapper::map)
                .map(chatRepository::save)
                .map(chatReadMapper::map)
                .orElseThrow();
    }

    @Transactional
    public ChatReadDto createChatForAllUsers(ChatCreateEditDto chatCreateEditDto) {
        ChatReadDto chatReadDto = Optional.of(chatCreateEditDto)
                .map(chatCreateEditMapper::map)
                .map(chatRepository::save)
                .map(chatReadMapper::map)
                .orElseThrow();
        userRepository.findAll().forEach(user ->
                userChatRepository.saveAndFlush(
                        Optional.of(new UserChatCreateEditDto(user.getId(),
                                chatReadDto.getId()))
                                .map(userChatCreateEditMapper::map).orElse(null)));
        return chatReadDto;

    }

    @Transactional
    public UserChatReadDto createUserChat(UserChatCreateEditDto userChatCreateEditDto) {
        return Optional.of(userChatCreateEditDto)
                .map(userChatCreateEditMapper::map)
                .map(userChatRepository::save)
                .map(userChatReadMapper::map)
                .orElseThrow();
    }

    @Transactional
    public List<UserChatReadDto> createAllUserChatsForUser(Long userId) {
        return findAllChats().stream().map(chatReadDto ->
            userChatRepository.save(
                    Optional.of(new UserChatCreateEditDto(userId, chatReadDto.getId()))
                            .map(userChatCreateEditMapper::map).orElseThrow())
        ).map(userChatReadMapper::map).toList();
    }


    @Transactional
    public void deleteAllChats() {
        chatRepository.deleteAll();
    }

    public List<ChatReadDto> findAllChats() {
        return chatRepository.findAll()
                .stream()
                .sorted(Comparator.comparing(Chat::getId))
                .map(chatReadMapper::map)
                .toList();
    }

    public ChatReadDto findChatById(Short id) {
        return chatRepository.findById(id)
                .map(chatReadMapper::map)
                .orElseThrow();
    }

    public List<UserChatReadDto> findAllUserChats() {
        return userChatRepository.findAll()
                .stream()
                .map(userChatReadMapper::map)
                .toList();
    }

    @Transactional
    public void deleteAllUserChats() {
        userChatRepository.deleteAll();
    }


    public UserChatReadDto findUserChatByChatIdAndUserId(Short chatId, Long user){
        return Optional.of(userChatRepository.findByChatIdAndUserId(chatId, user))
                .map(userChatReadMapper::map)
                .orElseThrow();
    }

    @Transactional
    public MessageReadDto createMessage(MessageCreateEditDto messageCreateEditDto){
        return Optional.of(messageCreateEditDto)
                .map(messageCreateEditMapper::map)
                .map(messageRepository::save)
                .map(messageReadMapper::map)
                .orElseThrow();
    }

    public List<MessageReadDto> findAllMessagesByUserChatId(Long userChatId) {
        return messageRepository.findAllMessagesByUserChatIdOrderById(userChatId)
                .stream()
                .map(messageReadMapper::map)
                .toList();
    }

    @Transactional
    public MessageReadDto updateImageLinkById(Long id, String imageLink) {
        return Optional.of(messageRepository.updateImageLinkById(id, imageLink))
                .map(messageReadMapper::map)
                .orElseThrow();
    }

    public Map<Short, Long> findCountMessagesInChats(Long userId, String role) {
        return Optional.of(messageRepository.findCountMessagesInChats(userId, role))
                .orElseThrow();
    }

    public Map<Short, Long> findCountMessagesInChatsForAdmin() {
        return Optional.of(messageRepository.findCountMessagesInChatsForAdmin())
                .orElseThrow();
    }

    public Long findCountMessagesInAllChats(Long userId, String role) {
        if (role.equals("ADMIN")) {
            return findCountMessagesInAllChatsForAdmin();
        }
        return Optional.of(messageRepository.findCountMessagesInAllChats(userId, role))
                .orElse(0L);
    }

    public Map<Long, String> findAllUserChatsWithNamesByChatId(Short chatId){
        return Optional.of(userChatRepository.findAllWithNamesByChatId(chatId))
                .orElseThrow();
    }

    public Long findCountMessagesInAllChatsForAdmin() {
        return Optional.of(messageRepository.findCountMessagesInAllChatsForAdmin())
                .orElse(0L);
    }

    public Map<Long, Long> findCountMessagesInUserChats(Short chatId){
        return Optional.of(messageRepository.findCountMessagesInUserChats(chatId))
                .orElseThrow();
    }

    public UserChatReadDto findUserChatById(Long id) {
        return userChatRepository.findById(id)
                .map(userChatReadMapper::map)
                .orElseThrow();
    }

    @Transactional
    public List<MessageReadDto> makeReadableMessages(Long userChatId, String role){
        return messageRepository.makeReadableMessages(userChatId, role)
                .stream()
                .map(messageReadMapper::map)
                .toList();
    }

    public List<UserChatReadDto> findAllUserChatsNotEmptyOrderByDateDesc(Short chatId) {
        return userChatRepository.findAllNotEmptyOrderByDateDescByChatId(chatId)
                .stream()
                .map(userChatReadMapper::map)
                .toList();
    }

    public Map<Short, Long> findCountUsersInChats() {
        return Optional.of(userChatRepository.findCountUsersInChats())
                .orElseThrow();
    }

    @Transactional
    public boolean delete(Short id) {
        return chatRepository.findById(id)
                .map(entity -> {
                    chatRepository.delete(entity);
                    chatRepository.flush();
                    return true;
                })
                .orElse(false);
    }

    @Transactional
    public ChatNameDto updateChatName(Short id, String ruName, String enName) {
        return Optional.of(chatNameRepository.updateChatName(id, ruName, enName))
                .map(chatNameReadMapper::map)
                .orElseThrow();
    }
}
