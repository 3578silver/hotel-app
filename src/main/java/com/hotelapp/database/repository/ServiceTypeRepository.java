package com.hotelapp.database.repository;

import com.hotelapp.database.entity.FoodType;
import com.hotelapp.database.entity.ServiceType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface ServiceTypeRepository extends
        JpaRepository<ServiceType, Integer>,
        QuerydslPredicateExecutor<ServiceType> {

    @Query(
            value = """
    SELECT * FROM service_type f WHERE f.en_name ILIKE :enName limit 1
""", nativeQuery = true
    )
    ServiceType findByEnName(String enName);

    @Query(
            value = """
    SELECT id FROM service_type f WHERE f.ru_name ILIKE :ruName limit 1
""", nativeQuery = true
    )
    Integer findIdByRuName(String ruName);

    @Query(
            value = """
    SELECT * FROM service_type f WHERE f.ru_name ILIKE :ruName limit 1
""", nativeQuery = true
    )
    ServiceType findByRuName(String ruName);
}
