package com.hotelapp.database.repository;

import com.hotelapp.database.entity.Cleaning;
import com.hotelapp.database.entity.CleaningTime;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface CleaningRepository extends
        JpaRepository<Cleaning, Long>,
        QuerydslPredicateExecutor<Cleaning> {

    @Query(value = """
        UPDATE cleaning
        SET cleaning_time = :time,
        room = :room,
        comment = :comment
        WHERE id = :cleaningId
        RETURNING *;
""", nativeQuery = true)
    Cleaning updateTimeRoomComment(String time,
                    String room,
                    String comment, Long cleaningId);
}
