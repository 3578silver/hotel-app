package com.hotelapp.database.dto.booking;

import com.hotelapp.database.dto.booking.locales.RoomTypeDescriptionDto;
import com.hotelapp.database.dto.booking.locales.RoomTypeNameDto;
import lombok.Value;

@Value
public class RoomTypeReadDto {

    Integer id;

    RoomTypeNameDto roomTypeNameDto;

    RoomTypeDescriptionDto roomTypeDescriptionDto;

    String imagePath;
    Integer price;
}
