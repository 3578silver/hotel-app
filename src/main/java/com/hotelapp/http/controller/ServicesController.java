package com.hotelapp.http.controller;

import com.hotelapp.database.dto.PayDto;
import com.hotelapp.database.dto.services.CartReadDto;
import com.hotelapp.database.dto.services.ServiceCartCreateEditDto;
import com.hotelapp.database.dto.services.ServiceCartReadDto;
import com.hotelapp.database.dto.services.ServiceReadDto;
import com.hotelapp.database.dto.user.UserReadDto;
import com.hotelapp.service.ChatService;
import com.hotelapp.service.ServicesService;
import com.hotelapp.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

@Controller
@RequiredArgsConstructor
public class ServicesController {
    private final ServicesService servicesService;

    private final UserService userService;

    private final ChatService chatService;

    @GetMapping("/services")
    public String getAllService(Model model,
                             @AuthenticationPrincipal UserDetails userDetails) {
        model.addAttribute("service", servicesService.findAllService());
        UserReadDto currentUser = userService.findByUsername(userDetails.getUsername()).get();
        model.addAttribute("countNotReadMessages", chatService.findCountMessagesInAllChats(currentUser.getId(), currentUser.getRole().getAuthority()));
        model.addAttribute("currentUser", currentUser);
        model.addAttribute("serviceTypes", servicesService.findAllServiceTypes());
        model.addAttribute("url", "services");
        return "app/_services";
    }

    @GetMapping("/services/{type}")
    public String getAllServiceByType(@PathVariable("type") String type,
                                   Model model,
                                   @AuthenticationPrincipal UserDetails userDetails) {
        model.addAttribute("service", servicesService.findAllByServiceTypeId(type));
        model.addAttribute("serviceTypes", servicesService.findAllServiceTypes());
        UserReadDto currentUser = userService.findByUsername(userDetails.getUsername()).get();
        model.addAttribute("countNotReadMessages", chatService.findCountMessagesInAllChats(currentUser.getId(), currentUser.getRole().getAuthority()));
        model.addAttribute("currentUser", currentUser);
        model.addAttribute("currentServiceType", type);
        return "app/_services";
    }

    @GetMapping("/cart")
    public String cartPage(Model model,
                               @AuthenticationPrincipal UserDetails userDetails) {
        UserReadDto userReadDto = userService.findByUsername(userDetails.getUsername()).get();
        model.addAttribute("currentUser", userReadDto);
        model.addAttribute("countNotReadMessages", chatService.findCountMessagesInAllChats(userReadDto.getId(), userReadDto.getRole().getAuthority()));
        model.addAttribute("cart", servicesService.findCartById(userReadDto.getCartId()).get());
        List<ServiceCartReadDto> userServices = servicesService.findAllServiceCartByCartId(userReadDto.getCartId());
        if (!userServices.isEmpty()) {
            model.addAttribute("userServices", userServices);
        }
        model.addAttribute("allServices", servicesService.findAllIdWithServices());
        model.addAttribute("url", "cart");
        return "app/_cart";
    }

    @PostMapping("/services/add/{id}")
    public String addServiceToCart(@PathVariable("id") Integer id,
                                @AuthenticationPrincipal UserDetails userDetails) {
        UserReadDto userReadDto = userService.findByUsername(userDetails.getUsername()).get();
        ServiceReadDto serviceReadDto = servicesService.findServiceById(id).orElse(null);
        servicesService.createServiceCart(new ServiceCartCreateEditDto(id, userReadDto.getCartId()));
        servicesService.addSumByCartId(userReadDto.getCartId(), serviceReadDto.getPrice());
        return "redirect:/services";
    }

    @PostMapping("/services/delete/{id}")
    public String deleteServiceFromCart(@PathVariable("id") Long id,
                                     @AuthenticationPrincipal UserDetails userDetails) {
        UserReadDto userReadDto = userService.findByUsername(userDetails.getUsername()).get();
        ServiceCartReadDto serviceCartReadDto = servicesService.findServiceCartById(id);
        ServiceReadDto serviceReadDto = servicesService.findServiceById(serviceCartReadDto.getServiceReadDto().getId()).orElse(null);
        servicesService.deleteServicesCartById(id);
        servicesService.minusSumByCartId(userReadDto.getCartId(), serviceReadDto.getPrice());
        return "redirect:/cart";
    }



    @GetMapping("/payServices")
    public String payServices(Model model,
                          @AuthenticationPrincipal UserDetails userDetails) {
        UserReadDto currentUser = userService.findByUsername(userDetails.getUsername()).get();
        model.addAttribute("countNotReadMessages", chatService.findCountMessagesInAllChats(currentUser.getId(), currentUser.getRole().getAuthority()));
        model.addAttribute("currentUser", currentUser);
        CartReadDto cartReadDto = servicesService.findCartById(currentUser.getCartId()).orElse(null);
        model.addAttribute("cart", cartReadDto);
        model.addAttribute("url", "cart");
        return "app/_payservices";
    }

    @PostMapping("/payServicesProcess")
    public String payServicesProcess(@ModelAttribute @Validated PayDto payDto,
                                 BindingResult bindingResult,
                                 RedirectAttributes redirectAttributes,
                                 @AuthenticationPrincipal UserDetails userDetails) {
        if (bindingResult.hasErrors()) {
            redirectAttributes.addFlashAttribute("errors", "error");
            return "redirect:/payServices";
        }
        UserReadDto userReadDto = userService.findByUsername(userDetails.getUsername()).get();
        servicesService.cleanCart(userReadDto.getCartId());
        servicesService.resetSumByCartId(userReadDto.getCartId());

        redirectAttributes.addFlashAttribute("createdOrder", payDto);


        return "redirect:/services";
    }
}
