package com.hotelapp.database.dto.services;

import com.hotelapp.database.dto.services.locales.CleaningOptionReadDto;
import com.hotelapp.database.dto.user.UserReadDto;
import com.hotelapp.database.entity.CleaningOption;
import com.hotelapp.database.entity.CleaningTime;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import lombok.Setter;
import lombok.Value;
import lombok.experimental.NonFinal;

import java.util.List;

@Value
public class CleaningCreateEditDto {
    @NotNull
    CleaningTime time;
    @NotEmpty
    @Pattern(regexp = "^\\d+$")
    String room;
    String comment;
}
