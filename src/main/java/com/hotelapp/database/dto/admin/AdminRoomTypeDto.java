package com.hotelapp.database.dto.admin;

import com.hotelapp.database.dto.booking.locales.RoomTypeDescriptionDto;
import com.hotelapp.database.dto.booking.locales.RoomTypeNameDto;
import jakarta.validation.constraints.NotNull;
import lombok.Value;
import org.hibernate.validator.constraints.Length;

@Value
public class AdminRoomTypeDto {

    @Length(max = 255)
    @NotNull
    String roomTypeRuName;
    @Length(max = 255)
    @NotNull
    String roomTypeEnName;
    @Length(max = 3150)
    @NotNull
    String roomTypeRuDescription;
    @Length(max = 3150)
    @NotNull
    String roomTypeEnDescription;
    @NotNull
    Integer price;

}
