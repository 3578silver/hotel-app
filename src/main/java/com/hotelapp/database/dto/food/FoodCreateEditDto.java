package com.hotelapp.database.dto.food;

import com.hotelapp.database.dto.food.locales.FoodDescriptionDto;
import com.hotelapp.database.dto.food.locales.FoodNameDto;
import com.hotelapp.database.dto.food.locales.FoodTypeDto;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Value;

@Value
public class FoodCreateEditDto {
    @NotNull
    FoodNameDto foodName;
    @NotNull
    Integer cost;
    @NotNull
    FoodDescriptionDto foodDescription;
    @NotNull
    FoodTypeDto foodType;
    @NotNull
    String imageLink;
}
