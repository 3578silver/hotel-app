package com.hotelapp.database.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "room_type")
public class RoomType implements BaseEntity<Integer> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "type_description_id", nullable = false)
    private RoomTypeDescription roomTypeDescription;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "type_name_id", nullable = false)
    private RoomTypeName roomTypeName;

    @NotEmpty
    @Column(name = "image_path", nullable = false)
    private String imagePath;

    @NotNull
    @Column(name = "price", nullable = false)
    private Integer price;

    @Builder.Default
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "roomType", orphanRemoval = true)
    private List<TypeConveniences> typeConveniences = new ArrayList<>();

    @Builder.Default
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "roomType", orphanRemoval = true)
    private List<Room> rooms = new ArrayList<>();

}
