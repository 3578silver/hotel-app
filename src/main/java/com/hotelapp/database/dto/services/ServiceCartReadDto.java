package com.hotelapp.database.dto.services;

import com.hotelapp.database.dto.food.FoodCartReadDto;
import com.hotelapp.database.dto.food.FoodReadDto;
import lombok.Value;

@Value
public class ServiceCartReadDto {
    Long id;

    ServiceReadDto serviceReadDto;

    CartReadDto cartReadDto;
}
