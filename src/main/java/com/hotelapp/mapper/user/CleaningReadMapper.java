package com.hotelapp.mapper.user;

import com.hotelapp.database.dto.services.CleaningReadDto;
import com.hotelapp.database.entity.Cleaning;
import com.hotelapp.mapper.Mapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@RequiredArgsConstructor
public class CleaningReadMapper implements Mapper<Cleaning, CleaningReadDto> {

    private final UserReadMapper userReadMapper;

    @Override
    public CleaningReadDto map(Cleaning cleaning) {
        return new CleaningReadDto(
                cleaning.getId(),
                cleaning.getCleaningTime(),
                cleaning.getRoom(),
                cleaning.getComment()
        );
    }
}
