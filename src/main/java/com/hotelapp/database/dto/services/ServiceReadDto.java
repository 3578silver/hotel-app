package com.hotelapp.database.dto.services;

import com.hotelapp.database.dto.services.locales.ServiceDescriptionDto;
import com.hotelapp.database.dto.services.locales.ServiceNameDto;
import com.hotelapp.database.dto.services.locales.ServiceTypeDto;
import lombok.Value;

@Value
public class ServiceReadDto {
    Integer id;

    ServiceNameDto serviceName;

    ServiceDescriptionDto serviceDescription;

    ServiceTypeDto serviceType;

    String imageLink;
    Integer price;
}
