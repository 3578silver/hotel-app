package com.hotelapp.mapper.booking;

import com.hotelapp.database.dto.booking.RoomCreateEditDto;
import com.hotelapp.database.entity.Room;
import com.hotelapp.database.entity.RoomType;
import com.hotelapp.database.repository.RoomTypeRepository;
import com.hotelapp.mapper.Mapper;
import com.hotelapp.mapper.booking.locales.RoomTypeDescriptionCreateEditMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class RoomCreateEditMapper implements Mapper<RoomCreateEditDto, Room> {

    private final RoomTypeRepository roomTypeRepository;


    @Override
    public Room map(RoomCreateEditDto fromObject, Room toObject) {
        copy(fromObject, toObject);
        return toObject;
    }

    @Override
    public Room map(RoomCreateEditDto roomCreateEditDto) {
        Room room = new Room();
        copy(roomCreateEditDto, room);
        return room;
    }

    private void copy(RoomCreateEditDto roomCreateEditDto, Room room) {
        RoomType roomType = roomTypeRepository.findById(roomCreateEditDto.getRoomTypeId()).orElse(null);
        room.setRoomType(roomType);
    }

}
