package com.hotelapp.database.repository;

import com.hotelapp.database.entity.FoodDescription;
import com.hotelapp.database.entity.ServiceDescription;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface ServiceDescriptionRepository extends
        JpaRepository<ServiceDescription, Integer>,
        QuerydslPredicateExecutor<ServiceDescription> {

    @Query(
            value = """
    SELECT id FROM service_description f WHERE f.ru_name ILIKE :ruName limit 1
""", nativeQuery = true
    )
    Integer findByRuName(String ruName);

    @Query(value = """
UPDATE service_description
SET ru_name = :serviceRuDescription,
en_name = :serviceEnDescription
WHERE id = :serviceDescriptionId
RETURNING *;
""", nativeQuery = true)
    ServiceDescription updateServiceDescription(Integer serviceDescriptionId, String serviceRuDescription, String serviceEnDescription);
}
