package com.hotelapp.http.rest;

import com.hotelapp.database.dto.services.CartCreateEditDto;
import com.hotelapp.database.dto.services.CartReadDto;
import com.hotelapp.database.dto.services.CleaningCreateEditDto;
import com.hotelapp.database.dto.services.CleaningReadDto;
import com.hotelapp.service.CartService;
import com.hotelapp.service.CleaningService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/cart")
@RequiredArgsConstructor
public class CartRestController {

    private final CartService cartService;

    @PostMapping(path = "/createCart", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<CartReadDto> createCart(@Validated @RequestBody CartCreateEditDto cartCreateEditDto){
        return new ResponseEntity<>(cartService.createCart(cartCreateEditDto), HttpStatus.CREATED);
    }

}