package com.hotelapp.config;

import com.hotelapp.service.AuthenticationService;
import com.hotelapp.service.UserService;
import jakarta.servlet.http.HttpSession;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.HeadersConfigurer;
import org.springframework.security.config.annotation.web.configurers.oauth2.server.resource.OAuth2ResourceServerConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.security.web.header.Header;
import org.springframework.security.web.savedrequest.NullRequestCache;

import java.io.IOException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Set;

@Configuration
@EnableWebSecurity
@EnableMethodSecurity
@RequiredArgsConstructor
public class SecurityConfiguration {


    private final UserService userService;
    private final JwtAuthenticationFilter jwtAuthFilter;
    private final AuthenticationProvider authenticationProvider;
    private final LogoutHandler logoutHandler;

    private final AuthenticationService authenticationService;





    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http, HttpSession httpSession) throws Exception {

        http
                .csrf()
                .disable()
                .authorizeHttpRequests()
                .requestMatchers("/swagger-ui/**", "/login", "/registration", "/v3/api-docs/**",
                        "/process_login", "/process_registration", "/app/**", "/templates/**", "/api/**", "/images/**", "/image/**")
                .permitAll()
                .requestMatchers("/adminpanel", "/adminrooms", "/adminroomsprocess", "/adminroomslist", "/adminuserslist/**", "/adminusers/delete/**",
                        "/adminchangerole/**", "/adminroomtypes", "/adminroomtypesprocess", "/adminroomtype/delete/**", "/adminconvlist", "/adminconv/delete/**",
                        "/adminconvnew", "/adminconvnewprocess", "/adminconvexist", "/adminconvexistprocess", "/adminroomtypes/updatepage/**", "/adminroomtypes/update/**",
                        "/adminusercreate", "/adminusercreateprocess", "/adminfoodlist", "/adminfood/delete/**", "/adminfoodcreate", "/adminfoodcreateprocess",
                        "/adminfood/updatepage/**", "/adminfood/update/**", "/adminserviceslist", "/adminservice/delete/**", "/adminservicecreate", "/adminservicecreateprocess",
                        "/adminservice/updatepage/**", "/adminservice/update/**", "/adminnewslist", "/adminnews/delete/**", "/adminnewscreate", "/adminnewscreateprocess",
                        "/adminnews/updatepage/**", "/adminnews/update/**", "/adminchatslist", "/adminchat/delete/**", "/adminchat/updatepage/**", "/adminchat/update/**",
                        "/adminchatcreate", "/adminchatcreateprocess", "/adminmessages/**", "/userchat/**", "/sendmessageadmin/**", "/sendpictureadmin/**")
                .hasAuthority("ADMIN")
                .anyRequest()
                .authenticated()
                .and()
                .requestCache()
                .requestCache(new NullRequestCache())
                .and()
                .formLogin()
                .loginPage("/login")

                .and()
                .logout(logout -> logout
                        .logoutUrl("/logout")
                        .logoutSuccessUrl("/login")
                        .deleteCookies("JSESSIONID")
                        .addLogoutHandler(logoutHandler)
                        .logoutSuccessHandler((request, response, authentication) -> {
                            SecurityContextHolder.clearContext();
                            httpSession.removeAttribute("Authorization");
                            response.sendRedirect("http://localhost:8080/login");
                        }))
                .sessionManagement()
                .sessionFixation().none()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authenticationProvider(authenticationProvider)
                .addFilterBefore(jwtAuthFilter, UsernamePasswordAuthenticationFilter.class)
                .headers()
                .frameOptions().sameOrigin()
                .cacheControl();

        return http.build();
    }


    @Bean
    public AuthenticationManager authenticationManager(AuthenticationConfiguration config) throws Exception {
        return config.getAuthenticationManager();
    }
}

