package com.hotelapp.http.rest;

import com.hotelapp.database.dto.services.ServiceCartCreateEditDto;
import com.hotelapp.database.dto.services.ServiceCartReadDto;
import com.hotelapp.database.dto.services.ServiceCreateEditDto;
import com.hotelapp.database.dto.services.ServiceReadDto;
import com.hotelapp.service.ServicesService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/services")
@RequiredArgsConstructor
public class ServicesRestController {
    private final ServicesService servicesService;

    @GetMapping(path = "/findAllServices", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<ServiceReadDto>> findAllService() {
        return new ResponseEntity<>(servicesService.findAllService(), HttpStatus.OK);
    }

    @GetMapping(path = "/findAllServiceCart", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<ServiceCartReadDto>> findAllServiceCart() {
        return new ResponseEntity<>(servicesService.findAllServiceCart(), HttpStatus.OK);
    }

    @PostMapping(path = "/createService", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<ServiceReadDto> createService(@Validated @RequestBody ServiceCreateEditDto serviceCreateEditDto){
        return new ResponseEntity<>(servicesService.createService(serviceCreateEditDto), HttpStatus.CREATED);
    }

    @PostMapping(path = "/createServiceCart", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<ServiceCartReadDto> createServiceCart(@Validated @RequestBody ServiceCartCreateEditDto serviceCartCreateEditDto) {
        return new ResponseEntity<>(servicesService.createServiceCart(serviceCartCreateEditDto), HttpStatus.OK);
    }

    @DeleteMapping("/deleteService/{id}")
    public ResponseEntity<Boolean> deleteService(@PathVariable("id") Integer id) {
        return new ResponseEntity<>(servicesService.deleteServiceById(id), HttpStatus.OK);
    }

}
