package com.hotelapp.database.dto.user;

import com.hotelapp.database.dto.food.FoodCartCreateEditDto;
import com.hotelapp.database.dto.food.FoodCartReadDto;
import com.hotelapp.database.dto.services.CartCreateEditDto;
import com.hotelapp.database.dto.services.CartReadDto;
import com.hotelapp.database.dto.services.CleaningCreateEditDto;
import com.hotelapp.database.dto.services.CleaningReadDto;
import com.hotelapp.database.entity.Cart;
import com.hotelapp.database.entity.Cleaning;
import com.hotelapp.database.entity.FoodCart;
import com.hotelapp.database.entity.Role;
import com.hotelapp.mapper.services.CartCreateEditMapper;
import jakarta.persistence.CascadeType;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToOne;
import jakarta.validation.constraints.*;
import jakarta.validation.groups.Default;
import lombok.Setter;
import lombok.ToString;
import lombok.Value;
import lombok.experimental.FieldNameConstants;
import lombok.experimental.NonFinal;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.NumberFormat;

import java.time.LocalDate;

@Value
@FieldNameConstants
public class UserCreateEditDto {

    @Email
    @NotEmpty
    String username;

    @NonFinal
    @Setter
    @NotBlank(groups = {Default.class, UpdatePasswordGroup.class})
    @Size(min = 6, max = 64, groups = {Default.class, UpdatePasswordGroup.class})
    String password;


    @NotEmpty(groups = {Default.class, UpdateUserGroup.class})
    @Pattern(regexp = "^[a-zA-Zа-яА-Я]+$", groups = {Default.class, UpdateUserGroup.class})
    @Size(min = 2, max = 64, groups = {Default.class, UpdateUserGroup.class})
    String firstname;

    @NotEmpty(groups = {Default.class, UpdateUserGroup.class})
    @Pattern(regexp = "^[a-zA-Zа-яА-Я]+$", groups = {Default.class, UpdateUserGroup.class})
    @Size(min = 2, max = 64, groups = {Default.class, UpdateUserGroup.class})
    String lastname;

    @Pattern(regexp = "^[0-9+-]+$", groups = {Default.class, UpdateUserGroup.class})
    @NotEmpty(groups = {Default.class, UpdateUserGroup.class})
    @Size(min = 5, max = 15, groups = {Default.class, UpdateUserGroup.class})
    String phoneNumber;

    @NonFinal
    @Setter
    Short loyaltyId;

    @NotNull(groups = {Default.class, UpdateUserGroup.class})
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    LocalDate birthDate;

    @NonFinal
    @Setter
    CartCreateEditDto cartCreateEditDto;

    @NonFinal
    @Setter
    FoodCartCreateEditDto foodCartCreateEditDto;

    @NonFinal
    @Setter
    CleaningCreateEditDto cleaningCreateEditDto;

    Role role;

}
