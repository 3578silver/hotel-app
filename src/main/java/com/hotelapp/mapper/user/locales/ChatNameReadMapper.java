package com.hotelapp.mapper.user.locales;

import com.hotelapp.database.dto.food.locales.FoodTypeDto;
import com.hotelapp.database.dto.user.locales.ChatNameDto;
import com.hotelapp.database.entity.ChatName;
import com.hotelapp.database.entity.FoodType;
import com.hotelapp.mapper.Mapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class ChatNameReadMapper implements Mapper<ChatName, ChatNameDto> {
    @Override
    public ChatNameDto map(ChatName chatName) {
        return new ChatNameDto(
                chatName.getRuName(),
                chatName.getEnName()
        );
    }
}