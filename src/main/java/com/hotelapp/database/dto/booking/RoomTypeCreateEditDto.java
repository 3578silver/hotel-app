package com.hotelapp.database.dto.booking;

import com.hotelapp.database.dto.booking.locales.RoomTypeDescriptionDto;
import com.hotelapp.database.dto.booking.locales.RoomTypeNameDto;
import jakarta.validation.constraints.NotNull;
import lombok.Value;

@Value
public class RoomTypeCreateEditDto {
    @NotNull
    RoomTypeNameDto roomTypeNameDto;
    @NotNull
    RoomTypeDescriptionDto roomTypeDescriptionDto;
    @NotNull
    String imagePath;
    @NotNull
    Integer price;
}