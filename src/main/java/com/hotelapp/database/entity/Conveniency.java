package com.hotelapp.database.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "conveniency")
public class Conveniency implements BaseEntity<Integer>{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "conveniency_description_id")
    private ConveniencyDescription conveniencyDescription;

    @NotEmpty
    @Column(name = "svg_link", nullable = false)
    private String svgLink;

    @Builder.Default
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "conveniency", orphanRemoval = true)
    private List<TypeConveniences> typeConveniences = new ArrayList<>();
}
