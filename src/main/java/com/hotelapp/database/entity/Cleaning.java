package com.hotelapp.database.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.ColumnDefault;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "cleaning")
public class Cleaning implements BaseEntity<Long>{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Enumerated(EnumType.STRING)
    private CleaningTime cleaningTime = CleaningTime.NOW;

    @NotNull
    @Column(nullable = false)
    private String room;

    @Builder.Default
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "cleaning", orphanRemoval = true)
    private List<CleaningOptionCleaning> cleaningOptionCleanings = new ArrayList<>();


    @Column
    private String comment;
}