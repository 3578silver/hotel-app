package com.hotelapp.http.controller;

import com.hotelapp.database.dto.AvailabilityDto;
import com.hotelapp.database.dto.PayDto;
import com.hotelapp.database.dto.booking.BookingCreateEditDto;
import com.hotelapp.database.dto.services.CleaningCreateEditDto;
import com.hotelapp.database.dto.services.CleaningForm;
import com.hotelapp.database.dto.user.UserReadDto;
import com.hotelapp.database.entity.BookingStatus;
import com.hotelapp.database.entity.CleaningTime;
import com.hotelapp.service.ChatService;
import com.hotelapp.service.CleaningService;
import com.hotelapp.service.RoomService;
import com.hotelapp.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;
import java.util.Optional;

@Controller
@RequiredArgsConstructor
public class CleaningController {
    private final CleaningService cleaningService;
    private final UserService userService;
    private final RoomService roomService;
    private final ChatService chatService;

    @GetMapping("/cleaning")
    public String cleaningPage(Model model,
                               @AuthenticationPrincipal UserDetails userDetails) {
        UserReadDto currentUser = userService.findByUsername(userDetails.getUsername()).get();
        model.addAttribute("countNotReadMessages", chatService.findCountMessagesInAllChats(currentUser.getId(), currentUser.getRole().getAuthority()));
        model.addAttribute("currentUser", currentUser);
        model.addAttribute("options", cleaningService.findAllCleaningOptions());
        model.addAttribute("url", "cleaning");
        model.addAttribute("time", CleaningTime.values());
        return "app/_cleaning";
    }

    @PostMapping("/cleaningProcess")
    public String cleaning(@ModelAttribute @Validated CleaningCreateEditDto cleaningCreateEditDto,
                           BindingResult bindingResult,
                           RedirectAttributes redirectAttributes,
                           @ModelAttribute CleaningForm cleaningForm,
                           @AuthenticationPrincipal UserDetails userDetails) {
        if (bindingResult.hasErrors() || roomService.findRoomById(Integer.parseInt(cleaningCreateEditDto.getRoom())) == null) {
            redirectAttributes.addFlashAttribute("errors", "error");
            return "redirect:/cleaning";
        }


        cleaningService.updateCleaning(cleaningCreateEditDto,
                userService.findByUsername(userDetails.getUsername()).get().getCleaningId());

        redirectAttributes.addFlashAttribute("createdCleaning", cleaningCreateEditDto);


        return "redirect:/rooms";
    }

}
