package com.hotelapp.database.dto.user;

import com.hotelapp.database.entity.Role;
import lombok.Value;

import java.time.LocalDate;

@Value
public class UserReadDto {
    Long id;
    String username;
    String firstname;
    String lastname;
    String phoneNumber;
    LocalDate birthDate;
    Long spent;
    LoyaltyReadDto loyalty;
    Long cleaningId;
    Long foodCartId;
    Long cartId;
    Role role;
}



