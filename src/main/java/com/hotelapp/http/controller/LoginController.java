package com.hotelapp.http.controller;

import com.hotelapp.database.dto.AuthenticationRequest;
import com.hotelapp.database.dto.AuthenticationResponse;
import com.hotelapp.database.dto.user.UserCreateEditDto;
import com.hotelapp.http.rest.AuthenticationRestController;
import jakarta.servlet.http.HttpSession;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

@Controller
@RequiredArgsConstructor
public class LoginController {

    private final AuthenticationRestController authenticationController;


    @GetMapping("/login")
    public String loginPage(Model model, AuthenticationRequest request){
        if (!model.containsAttribute("user")) {
            model.addAttribute("user", request);
        }
        if (!model.containsAttribute("errors")) {
            model.addAttribute("errors", "false");
        }
        return "app/_signin";
    }


    @PostMapping("/process_login")
    public String login(@ModelAttribute @Validated AuthenticationRequest request,
                        BindingResult bindingResult,
                        RedirectAttributes redirectAttributes,
                        HttpSession httpSession){
        redirectAttributes.addFlashAttribute("user", request);
        if (bindingResult.hasErrors()) {
            redirectAttributes.addFlashAttribute("errors", "true");
            return "redirect:/login";
        }
        try {
            ResponseEntity<AuthenticationResponse> responseEntity = authenticationController.authenticate(request, httpSession);
        } catch (AuthenticationException e) {
            redirectAttributes.addFlashAttribute("errors", "true");
            return "redirect:/login";
        }
        return "redirect:/rooms";


    }

    @GetMapping("/registration")
    public String registerPage(Model model, UserCreateEditDto user){
        if (!model.containsAttribute("user")) {
            model.addAttribute("user", user);
        }
        if (!model.containsAttribute("errors")) {
            model.addAttribute("errors", new HashMap<>());
        }
        return "app/_signup";
    }

    @PostMapping("/process_registration")
    public String register(@ModelAttribute @Validated UserCreateEditDto user,
                           BindingResult bindingResult,
                           RedirectAttributes redirectAttributes,
                           HttpSession httpSession){
        redirectAttributes.addFlashAttribute("user", user);
        Map<String, String> errors = new HashMap<>(){{
            put("username", "correct");
            put("firstname", "correct");
            put("password", "correct");
            put("lastname", "correct");
            put("phoneNumber", "correct");
            put("birthDate", "correct");
        }
        };
        if (bindingResult.hasErrors()) {
            bindingResult.getFieldErrors().stream().map(FieldError::getField).forEach(field -> errors.put(field, "incorrect"));
            redirectAttributes.addFlashAttribute("errors", errors);
            return "redirect:/registration";
        }
        try {
            authenticationController.register(user);
        } catch (IllegalArgumentException e) {
            errors.put("username", "incorrect");
            redirectAttributes.addFlashAttribute("errors", errors);
            return "redirect:/registration";
        }

        return "redirect:/login";
    }
}
