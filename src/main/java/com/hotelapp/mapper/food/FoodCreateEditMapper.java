package com.hotelapp.mapper.food;

import com.hotelapp.database.dto.food.FoodCreateEditDto;
import com.hotelapp.database.dto.user.LoyaltyCreateEditDto;
import com.hotelapp.database.entity.Food;
import com.hotelapp.database.entity.Loyalty;
import com.hotelapp.database.repository.FoodTypeRepository;
import com.hotelapp.mapper.Mapper;
import com.hotelapp.mapper.food.locales.FoodDescriptionCreateEditMapper;
import com.hotelapp.mapper.food.locales.FoodNameCreateEditMapper;
import com.hotelapp.mapper.food.locales.FoodTypeCreateEditMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@RequiredArgsConstructor
public class FoodCreateEditMapper implements Mapper<FoodCreateEditDto, Food> {

    private final FoodNameCreateEditMapper foodNameCreateEditMapper;
    private final FoodDescriptionCreateEditMapper foodDescriptionCreateEditMapper;
    private final FoodTypeCreateEditMapper foodTypeCreateEditMapper;
    private final FoodTypeRepository foodTypeRepository;

    @Override
    public Food map(FoodCreateEditDto foodCreateEditDto) {
        Food food = new Food();
        copy(foodCreateEditDto, food);
        return food;
    }

    @Override
    public Food map(FoodCreateEditDto foodCreateEditDto, Food food) {
        copy(foodCreateEditDto, food);
        return food;
    }


    private void copy(FoodCreateEditDto foodCreateEditDto, Food food) {
        food.setCost(foodCreateEditDto.getCost());
        food.setName(Optional.of(foodCreateEditDto.getFoodName())
                .map(foodNameCreateEditMapper::map).orElse(null));
        food.setDescription(Optional.of(foodCreateEditDto.getFoodDescription())
                .map(foodDescriptionCreateEditMapper::map).orElse(null));
        food.setType(Optional.of(foodTypeRepository.findByEnName(foodCreateEditDto.getFoodType().getEnName())).orElse(null));
//        food.setType(Optional.of(foodCreateEditDto.getFoodType())
//                .map(foodTypeCreateEditMapper::map).orElse(null));
        food.setImageLink(foodCreateEditDto.getImageLink());
    }
}