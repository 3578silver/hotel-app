package com.hotelapp.database.repository;

import com.hotelapp.database.entity.CleaningOption;
import com.hotelapp.database.entity.CleaningOptionCleaning;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface CleaningOptionCleaningRepository extends JpaRepository<CleaningOptionCleaning, Long>,
        QuerydslPredicateExecutor<CleaningOptionCleaning>{
}