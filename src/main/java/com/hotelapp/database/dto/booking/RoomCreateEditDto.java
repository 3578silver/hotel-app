package com.hotelapp.database.dto.booking;

import jakarta.validation.constraints.NotNull;
import lombok.Value;

@Value
public class RoomCreateEditDto {
    @NotNull
    Integer roomTypeId;
}
