package com.hotelapp.http.controller;


import com.hotelapp.database.dto.user.MessageCreateEditDto;
import com.hotelapp.database.dto.user.MessageReadDto;
import com.hotelapp.database.dto.user.UserChatReadDto;
import com.hotelapp.database.dto.user.UserReadDto;
import com.hotelapp.http.rest.ChatRestController;
import com.hotelapp.service.ChatService;
import com.hotelapp.service.UserService;
import io.swagger.v3.core.util.Json;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Controller
@RequiredArgsConstructor
public class ChatController {

    private final UserService userService;
    private final ChatService chatService;
    @GetMapping("/messages")
    public String messages(Model model, @AuthenticationPrincipal UserDetails userDetails){
        UserReadDto currentUser = userService.findByUsername(userDetails.getUsername()).get();
        model.addAttribute("currentUser", currentUser);
        model.addAttribute("url", "messages");
        model.addAttribute("chats", chatService.findAllChats());
        model.addAttribute("countNotReadMessages", chatService.findCountMessagesInAllChats(currentUser.getId(), currentUser.getRole().getAuthority()));

        if (currentUser.getRole().getAuthority().equals("USER")) {
            model.addAttribute("chatReadMessagesMap", chatService.findCountMessagesInChats(currentUser.getId(), currentUser.getRole().getAuthority()));
        } else {
            model.addAttribute("chatReadMessagesMap", chatService.findCountMessagesInChatsForAdmin());
        }

        return "app/_messages";
    }

    //for admin
    @GetMapping("/adminmessages/{id}")
    public String messagesChat(@PathVariable("id") Short id, Model model, @AuthenticationPrincipal UserDetails userDetails){
        UserReadDto currentUser = userService.findByUsername(userDetails.getUsername()).get();
        model.addAttribute("currentUser", currentUser);
        model.addAttribute("url", "messages");
        model.addAttribute("countNotReadMessages", chatService.findCountMessagesInAllChats(currentUser.getId(), currentUser.getRole().getAuthority()));


        model.addAttribute("chats", chatService.findAllUserChatsNotEmptyOrderByDateDesc(id));
        model.addAttribute("chatNames", chatService.findAllUserChatsWithNamesByChatId(id));

        model.addAttribute("chatReadMessagesMap", chatService.findCountMessagesInUserChats(id));

        model.addAttribute("countNotReadMessages", chatService.findCountMessagesInAllChats(currentUser.getId(), currentUser.getRole().getAuthority()));

        return "app/_messages_admin";
    }

    //for admin
    @GetMapping("/userchat/{id}")
    public String userchat(@PathVariable("id") Long id,
                       Model model,
                       @AuthenticationPrincipal UserDetails userDetails) {
        UserReadDto currentUser = userService.findByUsername(userDetails.getUsername()).get();
        chatService.makeReadableMessages(id, currentUser.getRole().getAuthority());

        List<MessageReadDto> messageReadDtos = chatService.findAllMessagesByUserChatId(id);
        UserChatReadDto userChatReadDto = chatService.findUserChatById(id);
        Short chatId = userChatReadDto.getChatReadDto().getId();

        Map<Long, List<String>> messagesMap = textLining(messageReadDtos);


        model.addAttribute("currentUser", currentUser);
        model.addAttribute("userChatId", id);
        model.addAttribute("chatId", chatId);

        model.addAttribute("chats", chatService.findAllUserChatsNotEmptyOrderByDateDesc(chatId));
        model.addAttribute("chatNames", chatService.findAllUserChatsWithNamesByChatId(chatId));
        model.addAttribute("chatReadMessagesMap", chatService.findCountMessagesInUserChats(chatId));
        model.addAttribute("countNotReadMessages", chatService.findCountMessagesInAllChats(currentUser.getId(), currentUser.getRole().getAuthority()));

        model.addAttribute("messages", messageReadDtos);
        model.addAttribute("messagesMap", messagesMap);
        model.addAttribute("url", id);



        return "app/_messages_chat_admin";
    }


    @GetMapping("/chat/{id}")
    public String chat(@PathVariable("id") Short id,
                       Model model,
                       @AuthenticationPrincipal UserDetails userDetails) {
        UserReadDto currentUser = userService.findByUsername(userDetails.getUsername()).get();
        UserChatReadDto userChatReadDto = chatService.findUserChatByChatIdAndUserId(id, currentUser.getId());
        chatService.makeReadableMessages(userChatReadDto.getId(), currentUser.getRole().getAuthority());
        List<MessageReadDto> messageReadDtos = chatService.findAllMessagesByUserChatId(userChatReadDto.getId());

        Map<Long, List<String>> messagesMap = textLining(messageReadDtos);


        model.addAttribute("countNotReadMessages", chatService.findCountMessagesInAllChats(currentUser.getId(), currentUser.getRole().getAuthority()));
        model.addAttribute("chatReadMessagesMap", chatService.findCountMessagesInChats(currentUser.getId(), currentUser.getRole().getAuthority()));
        model.addAttribute("currentUser", currentUser);
        model.addAttribute("chatId", id);
        model.addAttribute("chats", chatService.findAllChats());
        model.addAttribute("messages", messageReadDtos);
        model.addAttribute("messagesMap", messagesMap);
        model.addAttribute("url", chatService.findChatById(id).getName().getEnName());



        return "app/_messages_chat";
    }

    @PostMapping("/sendmessage/{id}")
    public String sendMessage(@ModelAttribute MessageCreateEditDto messageCreateEditDto,
            @PathVariable("id") Short id,
                              RedirectAttributes redirectAttributes,
                              @AuthenticationPrincipal UserDetails userDetails) {

        UserReadDto currentUser = userService.findByUsername(userDetails.getUsername()).get();
        UserChatReadDto userChatReadDto = chatService.findUserChatByChatIdAndUserId(id, currentUser.getId());

        chatService.createMessage(new MessageCreateEditDto(
                currentUser.getRole().getAuthority(),
                userChatReadDto.getId(),
                messageCreateEditDto.getText(),
                messageCreateEditDto.getImageLink()
        ));

        return "redirect:/chat/" + id;

    }

    // for admin
    @PostMapping("/sendmessageadmin/{id}")
    public String sendMessageAdmin(@ModelAttribute MessageCreateEditDto messageCreateEditDto,
                              @PathVariable("id") Long id,
                              RedirectAttributes redirectAttributes,
                              @AuthenticationPrincipal UserDetails userDetails) {

        UserReadDto currentUser = userService.findByUsername(userDetails.getUsername()).get();

        chatService.createMessage(new MessageCreateEditDto(
                currentUser.getRole().getAuthority(),
                id,
                messageCreateEditDto.getText(),
                messageCreateEditDto.getImageLink()
        ));

        return "redirect:/userchat/" + id;

    }

    @PostMapping("/sendpicture/{id}")
    public String sendPicture(@RequestParam("image") MultipartFile image,
                              @PathVariable("id") Short id,
                              RedirectAttributes redirectAttributes,
                              @AuthenticationPrincipal UserDetails userDetails) throws IOException {
        if (image.isEmpty()) {
            return "redirect:/chat/" + id;
        }

        UserReadDto currentUser = userService.findByUsername(userDetails.getUsername()).get();
        UserChatReadDto userChatReadDto = chatService.findUserChatByChatIdAndUserId(id, currentUser.getId());


        MessageReadDto messageReadDto = chatService.createMessage(new MessageCreateEditDto(
                currentUser.getRole().getAuthority(),
                userChatReadDto.getId(),
                null,
                ""
        ));

        String fullFileName = messageReadDto.getId() + ".png";

        chatService.updateImageLinkById(messageReadDto.getId(), fullFileName);

        Path path = Paths.get("src", "main", "resources", "templates" , "images", "chat", fullFileName);

        Files.write(path, image.getBytes());


        return "redirect:/chat/" + id;
    }

    @PostMapping("/sendpictureadmin/{id}")
    public String sendPictureAdmin(@RequestParam("image") MultipartFile image,
                              @PathVariable("id") Long id,
                              RedirectAttributes redirectAttributes,
                              @AuthenticationPrincipal UserDetails userDetails) throws IOException {
        if (image.isEmpty()) {
            return "redirect:/userchat/" + id;
        }

        UserReadDto currentUser = userService.findByUsername(userDetails.getUsername()).get();

        MessageReadDto messageReadDto = chatService.createMessage(new MessageCreateEditDto(
                currentUser.getRole().getAuthority(),
                id,
                null,
                ""
        ));

        String fullFileName = messageReadDto.getId() + ".png";

        chatService.updateImageLinkById(messageReadDto.getId(), fullFileName);

        Path path = Paths.get("src", "main", "resources", "templates" , "images", "chat", fullFileName);

        Files.write(path, image.getBytes());

        return "redirect:/userchat/" + id;
    }

    private Map<Long, List<String>> textLining(List<MessageReadDto> messages){
        return messages.stream()
                .collect(Collectors.toMap(MessageReadDto::getId, o -> {
                    List<String> text = new ArrayList<>();
                    String sb = o.getText();
                    if (sb == null) {
                        return text;
                    }
                    StringBuilder buffer = new StringBuilder();

                    while (true) {
                        if (sb.length() <= 35) {
                            if (!buffer.isEmpty()) {
                                text.add(buffer.toString());
                            }
                            text.add(sb);
                            sb = "";
                            break;
                        }

                        String line = sb.substring(0, 35);

                        if (!line.contains(" ")){
                            if (!buffer.isEmpty()) {
                                text.add(buffer.toString());
                                buffer = new StringBuilder();
                            }
                            text.add(line);
                            sb = sb.substring(36);
                        } else {
                            if (line.lastIndexOf(" ") == 0) {
                                buffer.append(" ");
                                sb = sb.substring(1);
                                continue;
                            }
                            buffer.append(line.substring(0, line.lastIndexOf(" ")));
                            sb = sb.substring(line.lastIndexOf(" "));
                        }
                    }
                    return text;
                }));
    }
}
